#include "HF.h"


double runHF(int nact,int nels,double &Ecore,double &Enuc,char *logName,char *datName,arma::mat &K1,arma::mat &V2aa,arma::mat &V2ab,arma::mat &S,arma::mat &C,arma::mat &Fock)
{
  initializeMats(K1,V2aa,V2ab,C,S,nact);
  buildK2ao(logName,K1,V2aa,V2ab,Ecore,Enuc,C);
  buildS(datName,S);

  return Ecore+Enuc+solveHF(K1,V2aa,V2ab,S,C,nels);
}

double solveHF(const arma::mat &K1,const arma::mat &V2aa,const arma::mat &V2ab,const arma::mat &S,arma::mat &C,int nels)
{
  int nAO=K1.n_rows;
  int nMO=C.n_cols;
  int nStep;
  double Enew,Eold,dE,dConv;
  arma::mat D1,D1old,dD1,F,G;
  arma::mat s,U,X;
  arma::vec eigval;

  eig_sym(eigval,U,S);
  X.resize(nAO,nMO);
  for(int i=0;i<nMO;i++)
  {
    X.col(nMO-i-1)=U.col(nAO-i-1)/sqrt(eigval(nAO-i-1));
  }

  G.resize(nAO,nAO);
  D1.resize(nAO,nAO);
  buildD1(nels,C,D1);
  buildG(G,D1,V2ab);

  dConv=1.0; dE=1.0; nStep=0;
  while(fabs(dE) > pow(10,-12) || dConv > 0.000001)
  {
    nStep++;
    F=K1+G;
    F=X.t()*F*X;
    eig_sym(eigval,C,F);
    C=X*C;
    D1old = D1;
    buildD1(nels,C,D1);
    buildG(G,D1,V2ab);
    Eold=Enew;
    Enew = 2.0*trace(K1*D1)+trace(G*D1);

    while(Enew > Eold)
    {
      D1 = 0.5*D1+0.5*D1old;
      buildG(G,D1,V2ab);
      Enew = 2.0*trace(K1*D1)+trace(G*D1);
    }
//lines used for printing iterations
    dE = Enew-Eold;
    dConv = arma::norm(D1-D1old);
    std::cout << nStep << '\t' << Enew << '\t' << dE << '\t' << dConv << std::endl;
  }

  return Enew;
}

void buildD1(int nels,const arma::mat &C,arma::mat &D1)
{
  D1.zeros();
  for(int i=0;i<nels/2;i++)
  {
    D1+=kron(C.col(i),C.col(i).t());
  }

  return;
}

void buildG(arma::mat &G,const arma::mat &D1,const arma::mat &V2ab)
{
  int nact=D1.n_rows;
  double x;

  for(int k=0;k<nact;k++)
  {
    for(int i=0;i<nact;i++)
    {
      x=0.0;
      for(int l=0;l<nact;l++)
      {
	for(int j=0;j<nact;j++)
	{
	  x += D1(j,l)*(2.0*V2ab(i*nact+j,k*nact+l)-V2ab(j*nact+i,k*nact+l));
	}
      }
      G(i,k) = x;
    }
  }

  return;
}

void rotateInts(arma::mat &K1,arma::mat &V2aa,arma::mat &V2ab,const arma::mat &C)
{

  K1=C.t()*K1*C;
  int nact=K1.n_rows;
  V2ab=kron(C.t(),C.t())*V2ab*kron(C,C);
  V2aa.resize(nact*(nact-1)/2,nact*(nact-1)/2);
  for(int i=0;i<nact;i++)
  {
    for(int j=i+1;j<nact;j++)
    {
      for(int k=0;k<nact;k++)
      {
	for(int l=k+1;l<nact;l++)
	{
	  V2aa(indexD(i,j,nact),indexD(k,l,nact))=2.0*(V2ab(i*nact+j,k*nact+l)-V2ab(i*nact+j,l*nact+k));
	}
      }
    }
  }

  return;
}

double solveHFdiis(const arma::mat &K1,const arma::mat &V2aa,const arma::mat &V2ab,const arma::mat &S,arma::mat &C,int nels,int nF)
{
  int nAO=K1.n_rows;
  int nMO=C.n_cols;
  int nStep;
  double Enew,Eold,dE,dConv;
  arma::mat D1,D1old,dD1,F,G;
  arma::mat s,U,X;
  arma::vec eigval;

  arma::mat B;
  arma::cube Fs,Es;
  arma::mat Fock;

  eig_sym(eigval,U,S);
  nMO -=2;
  X.resize(nAO,nMO);
  C.resize(nAO,nMO);
  for(int i=0;i<nMO;i++)
  {
    X.col(nMO-i-1)=U.col(nAO-i-1)/sqrt(eigval(nAO-i-1));
    std::cout << i << '\t' << eigval(i) << std::endl;
  }
  std::cout << "\n\n";
  eig_sym(eigval,U,K1);
  for(int i=0;i<eigval.n_rows;i++)
  {
    std::cout << i << '\t' << eigval(i) <<std::endl;
  }
  std::cout << "\n\n";
  s=X.t()*K1*X;
  eig_sym(eigval,U,s);
  for(int i=0;i<eigval.n_rows;i++)
  {
    std::cout << i << '\t' << eigval(i) <<std::endl;
  }
  std::cout << "\n\n";
  

  G.resize(nAO,nAO);
  D1.resize(nAO,nAO);
  buildD1(nels,C,D1);
  buildG(G,D1,V2ab);

  dConv=1.0; dE=1.0; nStep=0;
  while(fabs(dE) > pow(10,-12) || dConv > 0.00000001)
  {
    nStep++;
    F=K1+G;
    DIIS(F,D1,S,B,Fs,Es,nF);    

    Fock=X.t()*F*X;
    eig_sym(eigval,C,Fock);
    C=X*C;
    for(int i=0;i<C.n_rows;i++)
    {
      Fock=C.col(i).t()*K1*C.col(i);
      std::cout << i << '\t' << Fock(0) << std::endl;
    }
    return 0.0;
    D1old = D1;
    buildD1(nels,C,D1);
    buildG(G,D1,V2ab);
    Eold=Enew;
    Enew = 2.0*trace(K1*D1)+trace(G*D1);

/*    while(Enew > Eold)
    {
      std::cout << "\tdropping step size" << std::endl;
      D1 = 0.5*D1+0.5*D1old;
      buildG(G,D1,V2ab);
      Enew = 2.0*trace(K1*D1)+trace(G*D1);
    }*/
//lines used for printing iterations
    dE = Enew-Eold;
    dConv = arma::norm(D1-D1old);
    std::cout << nStep << '\t' << 2.0*trace(K1*D1) << '\t' << trace(G*D1) << '\t' << trace(K1) << '\t' << trace(D1*S) << std::endl;
    //std::cout << nStep << '\t' << Enew << '\t' << dE << '\t' << dConv << std::endl;
  }

  return Enew;
}


void DIIS(arma::mat &Fn,const arma::mat &D1,const arma::mat &S,arma::mat &B,arma::cube &Fs,arma::cube &Es,int nF)
{
  arma::mat E;
  arma::mat Bnew;
  if(arma::norm(D1) < 0.000001)
  {
    return;
  }
  else if(Es.n_slices==0)
  {
    Es.resize(Fn.n_rows,Fn.n_cols,1);
    Fs.resize(Fn.n_rows,Fn.n_cols,1);
    Fs.slice(0) = Fn;
    Es.slice(0) = Fn*D1*S-S*D1*Fn;
    B.resize(2,2);
    B(0,0) = arma::dot(Es.slice(0),Es.slice(0));
    return;
  }
  else if(Es.n_slices < nF)
  {
    E=Fn*D1*S-S*D1*Fn;
    Es.insert_slices(Es.n_slices,1);
    Es.slice(Es.n_slices-1)=E;
    Fs.insert_slices(Fs.n_slices,1);
    Fs.slice(Fs.n_slices-1)=Fn;
    B.resize(B.n_rows+1,B.n_rows+1);
    for(int i=0;i<B.n_rows-2;i++)
    {
      B(i,Es.n_slices-1)=B(Es.n_slices-1,i)=arma::dot(Es.slice(i),Es.slice(Es.n_slices-1));
      B(i,Es.n_slices)=B(Es.n_slices,i)=-1.0;
    }
    B(Es.n_slices,Es.n_slices)=0.0;
    B(B.n_rows-2,B.n_cols-2)=arma::dot(E,E);
    B(B.n_rows-1,B.n_rows-2)=B(B.n_rows-2,B.n_cols-1)=-1;
//    B.print();
  }
  else
  {
    Es.slices(0,nF-2)=Es.slices(1,nF-1);
    Fs.slices(0,nF-2)=Fs.slices(1,nF-1);
    B.submat(0,0,nF-2,nF-2)=B.submat(1,1,nF-1,nF-1);
    E=Fn*D1*S-S*D1*Fn;
    Es.slice(Es.n_slices-1)=E;
    Fs.slice(Fs.n_slices-1)=Fn;
    for(int i=0;i<B.n_rows-2;i++)
    {
      B(i,Es.n_slices-1)=B(Es.n_slices-1,i)=arma::dot(Es.slice(i),Es.slice(Es.n_slices-1));
      B(i,Es.n_slices)=B(Es.n_slices,i)=-1.0;
    }
    B(Es.n_slices,Es.n_slices)=0.0;
    B(B.n_rows-2,B.n_cols-2)=arma::dot(E,E);
    B(B.n_rows-1,B.n_rows-2)=B(B.n_rows-2,B.n_cols-1)=-1;
//    B.print();
  }

  arma::vec RHS(Es.n_slices+1,arma::fill::zeros);
  RHS(Es.n_slices)=-1.0;

  for(int i=0;i<Es.n_slices;i++)
  {
    B(i,B.n_cols-1)=-1;
    B(B.n_rows-1,i)=-1;
    for(int j=0;j<Es.n_slices;j++)
    {
      B(i,j) = dot(Es.slice(i),Es.slice(j));
    }
  }

    //B.print();
    //std::cout << std::endl;
  
  arma::vec C = solve(B,RHS);
  Fn.zeros();
  for(int i=0;i<Es.n_slices;i++)
  {
    Fn += C(i)*Fs.slice(i);
  }
  return;
}
