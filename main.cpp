#include "stdafx.h"
#include <armadillo>
#include <iomanip>
#include "readK2.h"
#include "HF.h"
#include "D2.h"
#include "math.h"
#include "time.h"
#include "optimization.h"
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

// Libint Gaussian integrals library
#include <libint2.hpp>

using namespace alglib;

double F1(int i,int a,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab);
double F2ab(int m,int n,int e,int f,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab);
double F2aa(int m,int n,int e,int f,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab);
void calcFci(arma::mat &F1m,arma::mat &F2aaM,arma::mat &F2abM,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,time_t &tim);
void calcF(arma::mat &F1m,arma::mat &F2aaM,arma::mat &F2abM,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,time_t &tim);
void genInts(double &Enuc,arma::mat &S,arma::mat &K1ao,arma::mat &V2aaAO,arma::mat &V2abAO,std::vector<libint2::Atom> atoms,std::string basis);
void evalCI(const real_1d_array &T,double &E,real_1d_array &grad,void *ptr);
void evalNum(const real_1d_array &T,double &E,real_1d_array &grad,void *ptr);
void gradEval(const real_1d_array &T,double &E,real_1d_array &grad,void *ptr);
void printIts(const real_1d_array &x,double func,void *ptr);
void makeD2p(arma::mat &D1,arma::mat &D2aa,arma::mat &D2ab,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,arma::mat &F1m,arma::mat &F2aaM,arma::mat &F2abM,time_t &tim,void *ptr);
void makeDnew(arma::mat &D1,arma::mat &D2aa,arma::mat &D2ab,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,arma::mat &F1m,arma::mat &F2aaM,arma::mat &F2abM,time_t &tim,void *ptr,real_1d_array &grad);

double dij(int i,int j,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab);
double dab(int a,int b,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab);
double dia(int i,int a,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m);

struct intBundle
{
  double E;
  double Enuc;
  arma::mat K1;
  arma::mat V2aa;
  arma::mat V2ab;
  int nels;
  int virt;
  int numIt;
  bool print;
  time_t timE,timG,timF;
  arma::mat K1i_j,K1a_b,K1ia;
  arma::mat V1i_j,V1ia,V1a_b,V1i_a;
  arma::mat V2abi_jka,V2abia_jb,V2abh_fgn;
  arma::mat V2aai_jka,V2aaia_jb,V2aah_fgn;
  arma::mat V1aia,V1ai_j,V1aa_b;
  arma::mat V2aaij_kl,V2aaab_cd;
  arma::mat V2abij_kl,V2abab_cd;
  arma::mat V2abia_jb2;
};

void setMats(intBundle &i);

struct bundleNum
{
  std::vector<libint2::Atom> atoms;
  std::string basis;
  int numIt;
};


int main(int argc, char *argv[])
{
  using arma::mat;
  using arma::vec;

  double x;
  int nact,nels,nCore,nstate;
  double Ecore,Enuc;

  std::cout << std::setprecision(15);

//  char xyz[] = "h2o.xyz";
  std::ifstream input_file(argv[1]);
  std::vector<libint2::Atom> atoms = libint2::read_dotxyz(input_file);

//  std::string basis="sto-3g";  
  //std::string basis="6-31g*";  
  std::string basis=std::string(argv[2]);  

  libint2::BasisSet obs(basis,atoms);

  int nMO=0;
  int nAO=0;
  for(int i=0;i<obs.size();i++)
  {
    nMO += obs[i].size();
    nAO += obs[i].cartesian_size();
  }
  std::cout << "nMO: " << nMO << std::endl;
  double r;
  Enuc=0.0;
  nels=0;
  for(int i=0;i<atoms.size();i++)
  {
    nels += atoms[i].atomic_number;
    for(int j=i+1;j<atoms.size();j++)
    {
      r=(atoms[i].x-atoms[j].x)*(atoms[i].x-atoms[j].x);
      r+=(atoms[i].y-atoms[j].y)*(atoms[i].y-atoms[j].y);
      r+=(atoms[i].z-atoms[j].z)*(atoms[i].z-atoms[j].z);
      Enuc += (double) atoms[i].atomic_number*atoms[j].atomic_number/sqrt(r);
    }
  }
  std::cout << "Enuc: " << Enuc << std::endl;
  std::cout << "nels: " << nels << std::endl;
  arma::mat S(nAO,nAO,arma::fill::zeros);
  arma::mat K1ao(nAO,nAO,arma::fill::zeros);
  arma::mat V2aaAO(nAO*(nAO-1)/2,nAO*(nAO-1)/2,arma::fill::zeros);
  arma::mat V2abAO(nAO*nAO,nAO*nAO,arma::fill::zeros);
  arma::mat C(nAO,nMO,arma::fill::zeros);
  std::cout << "Generating integrals" << std::endl;
  genInts(Enuc,S,K1ao,V2aaAO,V2abAO,atoms,basis);
  std::cout << "integrals generated" << std::endl;

  /*std::cout << std::setprecision(6);
  for(int i=0;i<K1ao.n_rows;i++)
  {
    for(int j=0;j<K1ao.n_cols;j++)
    {
      std::cout << i << '\t' << j << '\t' << K1ao(i,j) << std::endl;
    }
  }
*/
  double Ehf=solveHF(K1ao,V2aaAO,V2abAO,S,C,nels);
  //double Ehf=solveHFdiis(K1ao,V2aaAO,V2abAO,S,C,nels,10);
  std::cout << "Ehf: " << Enuc+Ehf << std::endl;
  return 0;

  arma::mat K1,V2aa,V2ab;
  K1=K1ao; V2aa=V2aaAO; V2ab=V2abAO;

  rotateInts(K1,V2aa,V2ab,C);
//  buildK2("co_ei.log",K1,V2aa,V2ab,Ecore,Enuc,C);
//  K1=K1ao; V2aa=V2aaAO; V2ab=V2abAO;
//  rotateInts(K1,V2aa,V2ab,C);

  nels=nels/2;
  int virt=nMO-nels;
  std::cout << nels << '\t' << virt << '\t' << nels+virt << std::endl;

  intBundle integrals={0.0,Enuc,K1,V2aa,V2ab,nels,virt,0,true,0,0,0};
  setMats(integrals);

  int numTot=nels*virt;
  numTot += nels*(nels-1)/2*virt*(virt-1)/2;
  numTot += nels*nels*virt*virt;

  double* Ts;
  Ts = new double[numTot];
  for(int i=0;i<numTot;i++)
  {
    Ts[i]=0.0;
  }

  real_1d_array T;
  T.setcontent(numTot,Ts);

  double epsg = 0.0005;
  double epsf = 0;
  double epsx = 0;
  ae_int_t maxits = 0;
  minlbfgsstate state;
  minlbfgsreport rep;

  minlbfgscreate(3,T,state);
  minlbfgssetcond(state,epsg,epsf,epsx,maxits);
  minlbfgssetxrep(state,true);

  minlbfgsoptimize(state,gradEval,printIts,&integrals);
  //minlbfgsoptimize(state,evalNum,printIts,&integrals);
  minlbfgsresults(state,T,rep);

  real_1d_array grad;
  grad.setcontent(numTot,Ts);
  double E;
  //evalNum(T,E,grad,&integrals);
  gradEval(T,E,grad,&integrals);

  std::cout << "\nTime F: " << integrals.timF << std::endl;
  std::cout << "Time E: " << integrals.timE << std::endl;
  std::cout << "Time G: " << integrals.timG << std::endl;
  std::cout << integrals.timF+integrals.timE+integrals.timG << std::endl;
/*
  arma::mat T1(nels,virt,arma::fill::zeros);
  arma::mat T2aa(nels*(nels-1)/2,virt*(virt-1)/2,arma::fill::zeros);
  arma::mat T2ab(nels*nels,virt*virt,arma::fill::zeros);  

  arma::mat F1m(nels,virt,arma::fill::zeros);
  arma::mat F2aaM(nels*(nels-1)/2,virt*(virt-1)/2,arma::fill::zeros);
  arma::mat F2abM(nels*nels,virt*virt,arma::fill::zeros);  

  nact=nels+virt;

  arma::mat D1(nact,nact,arma::fill::zeros);
  arma::mat D2aa(nact*(nact-1)/2,nact*(nact-1)/2,arma::fill::zeros);
  arma::mat D2ab(nact*nact,nact*nact,arma::fill::zeros);  

  int n1=nels*virt;
  int n2aa=T2aa.n_rows*T2aa.n_cols;

  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      T1(i,a)=T[i*virt+a];
      for(int j=i+1;j<nels;j++)
      {
	for(int b=a+1;b<virt;b++)
	{
	  T2aa(indexD(i,j,nels),indexD(a,b,virt))=T[n1+indexD(i,j,nels)*T2aa.n_cols+indexD(a,b,virt)];
	}
      }
      for(int j=0;j<nels;j++)
      {
	for(int b=0;b<virt;b++)
	{
	  T2ab(i*nels+j,a*virt+b)=T[n1+n2aa+(i*nels+j)*T2ab.n_cols+(a*virt+b)];
	}
      }	
    }
  }

  time_t tim;
//  calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,tim);
  makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,tim,&integrals);

  std::cout << Enuc << '\t' << 2.0*trace(K1*D1) << '\t' << trace(V2aa*D2aa) << '\t' << trace(V2ab*D2ab) << std::endl;
  std::cout << Enuc+2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab) << std::endl;

  std::cout << "Time F: " << integrals.timF << std::endl;
  std::cout << "Time E: " << integrals.timE << std::endl;
  std::cout << integrals.timF+integrals.timE << std::endl;
*/
  return EXIT_SUCCESS;
}

double dE1dT2ab(int m,int n,int e,int f,const arma::mat &K1,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m)
{
  int nact = K1.n_rows;
  int nels = T1.n_rows;
  int virt = T1.n_cols;
  double x=0.0;
  double dE = 0.0;

/*  x=0.0;
  for(int i=0;i<nels;i++)
  {
    x += K1(m,i)*T2ab(i*nels+n,e*virt+f);
  }
  dE -= 4.0*x;

  x = 0.0;
  for(int a=0;a<virt;a++)
  {
    x += K1(a+nels,e+nels)*T2ab(m*nels+n,a*virt+f);
  }
  dE += 4.0*x;
*/
//  dE += 4.0*K1(m,e+nels)*T1(n,f);
//  dE += 4.0*K1(m,e+nels)*T1(m,e)*T2ab(m*nels+n,e*virt+f)*pow(F1m(m,e),-0.5);
/*  x=0.0;
  for(int i=0;i<nels;i++)
  {
    x += K1(i,e+nels)*T1(i,e)*pow(F1m(i,e),-0.5);
  }
  dE -= 4.0*x*T2ab(m*nels+n,e*virt+f);
  x=0.0;
  for(int a=0;a<virt;a++)
  {
    x += K1(m,a+nels)*T1(m,a)*pow(F1m(m,a),-0.5);
  }
  dE -= 4.0*x*T2ab(m*nels+n,e*virt+f);
*/
  return dE;
}

double dE1dT1(int m,int e,const arma::mat &K1,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m)
{
  int nact = K1.n_rows;
  int nels = T1.n_rows;
  int virt = T1.n_cols;
  double x=0.0;
  double dE = 0.0;

/*  x = 0.0;
  for(int i=0;i<nels;i++)
  {
    x += K1(m,i)*T1(i,e);
  }
  dE -= 4.0*x;
  x = 0.0;
  for(int a=0;a<virt;a++)
  {
    x += K1(a+nels,e+nels)*T1(m,a);
  }
  dE += 4.0*x;

  x=0.0;
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      x += K1(i,a+nels)*T2ab(i*nels+m,a*virt+e);
    }
  }
  dE += 4.0*x;
  x = 0.0;
  for(int i=0;i<m;i++)
  {
    for(int a=0;a<e;a++)
    {
      x += K1(i,a+nels)*T2aa(indexD(i,m,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      x -= K1(i,a+nels)*T2aa(indexD(i,m,nels),indexD(e,a,virt));
    }
  }
  dE += 4.0*x;
  x = 0.0;
  for(int i=m+1;i<nels;i++)
  {
    for(int a=0;a<e;a++)
    {
      x -= K1(i,a+nels)*T2aa(indexD(m,i,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      x += K1(i,a+nels)*T2aa(indexD(m,i,nels),indexD(e,a,virt));
    }
  }
  dE += 4.0*x;
  dE += 4.0*K1(m,e+nels)*pow(F1m(m,e),0.5);
  dE += 4.0*K1(m,e+nels)*T1(m,e)*T1(m,e)*pow(F1m(m,e),-0.5);
  x=0.0;
  for(int i=0;i<nels;i++)
  {
    x += K1(i,e+nels)*T1(i,e)*T1(m,e)*pow(F1m(i,e),-0.5);
  }
  dE -= 4.0*x;
  x=0.0;
  for(int a=0;a<virt;a++)
  {
    x += K1(m,a+nels)*T1(m,a)*T1(m,e)*pow(F1m(m,a),-0.5);
  }
  dE -= 4.0*x;
*/
  return dE;
}

double dE1dT2aa(int m,int n,int e,int f,const arma::mat &K1,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m)
{
  int nact = K1.n_rows;
  int nels = T1.n_rows;
  int virt = T1.n_cols;
  double x=0.0;
  double dE = 0.0;
  double y;

  /*x=0.0;
  for(int i=0;i<n;i++)
  {
    x += K1(i,m)*T2aa(indexD(i,n,nels),indexD(e,f,virt));
  }
  dE -= 4.0*x;
  x=0.0;
  for(int i=0;i<m;i++)
  {
    x += K1(i,n)*T2aa(indexD(i,m,nels),indexD(e,f,virt));
  }
  dE += 4.0*x;
  x=0.0;
  for(int i=m+1;i<nels;i++)
  {
    x += K1(i,n)*T2aa(indexD(m,i,nels),indexD(e,f,virt));
  }
  dE -= 4.0*x;
  x=0.0;
  for(int i=n+1;i<nels;i++)
  {
    x += K1(i,m)*T2aa(indexD(n,i,nels),indexD(e,f,virt));
  }
  dE += 4.0*x;

  x=0.0;
  for(int a=0;a<f;a++)
  {
    x += K1(a+nels,e+nels)*T2aa(indexD(m,n,nels),indexD(a,f,virt));
  }
  dE += 4.0*x;
  x=0.0;
  for(int a=0;a<e;a++)
  {
    x += K1(a+nels,f+nels)*T2aa(indexD(m,n,nels),indexD(a,e,virt));
  }
  dE -= 4.0*x;
  x=0.0;
  for(int a=f+1;a<virt;a++)
  {
    x += K1(a+nels,e+nels)*T2aa(indexD(m,n,nels),indexD(f,a,virt));
  }
  dE -= 4.0*x;
  x=0.0;
  for(int a=e+1;a<virt;a++)
  {
    x += K1(a+nels,f+nels)*T2aa(indexD(m,n,nels),indexD(e,a,virt));
  }
  dE += 4.0*x;

  dE += 4.0*K1(m,e+nels)*T1(n,f);
  dE -= 4.0*K1(n,e+nels)*T1(m,f);
  dE -= 4.0*K1(m,f+nels)*T1(n,e);
  dE += 4.0*K1(n,f+nels)*T1(m,e);

  x=K1(n,f+nels)*T1(n,f)*pow(F1m(n,f),-0.5);
  x+=K1(n,e+nels)*T1(n,e)*pow(F1m(n,e),-0.5);
  x+=K1(m,f+nels)*T1(m,f)*pow(F1m(m,f),-0.5);
  x+=K1(m,e+nels)*T1(m,e)*pow(F1m(m,e),-0.5);
  dE += 4.0*x*T2aa(indexD(m,n,nels),indexD(e,f,virt));

  x=0.0;
  for(int i=0;i<nels;i++)
  {
    x += K1(i,f+nels)*T1(i,f)*pow(F1m(i,f),-0.5);
    x += K1(i,e+nels)*T1(i,e)*pow(F1m(i,e),-0.5);
  }
  dE -= 4.0*x*T2aa(indexD(m,n,nels),indexD(e,f,virt));
  x=0.0;
  for(int a=0;a<virt;a++)
  {
    x += K1(m,a+nels)*T1(m,a)*pow(F1m(m,a),-0.5);
    x += K1(n,a+nels)*T1(n,a)*pow(F1m(n,a),-0.5);
  }
  dE -= 4.0*x*T2aa(indexD(m,n,nels),indexD(e,f,virt));
*/
  return dE;
}

double dE2ab_dT1(int q,int s,const arma::mat &V2ab,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m,const arma::mat &F2abM)
{
  int nels = T1.n_rows;
  int virt = T1.n_cols;
  int nact = nels+virt;
  double x=0.0;
  double dE = 0.0;
  double y;

  /*x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      x += V2ab(m*nact+n,m*nact+q)*T1(n,s);
    }
  }
  dE -= 4.0*x;


  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      for(int e=0;e<virt;e++)
      {
	x += V2ab(m*nact+n,m*nact+e+nels)*T2ab(q*nels+n,s*virt+e);
      }
    }
  }
  dE += 4.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=s+1;e<virt;e++)
    {
      for(int n=q+1;n<nels;n++)
      {
	x += V2ab(m*nact+n,m*nact+e+nels)*T2aa(indexD(q,n,nels),indexD(s,e,virt));
      }
      for(int n=0;n<q;n++)
      {
	x -= V2ab(m*nact+n,m*nact+e+nels)*T2aa(indexD(n,q,nels),indexD(s,e,virt));
      }
    }
    for(int e=0;e<s;e++)
    {
      for(int n=q+1;n<nels;n++)
      {
	x -= V2ab(m*nact+n,m*nact+e+nels)*T2aa(indexD(q,n,nels),indexD(e,s,virt));
      }
      for(int n=0;n<q;n++)
      {
	x += V2ab(m*nact+n,m*nact+e+nels)*T2aa(indexD(n,q,nels),indexD(e,s,virt));
      }
    }
  }
  dE += 4.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      for(int e=0;e<virt;e++)
      {
	x += V2ab(m*nact+n,q*nact+e+nels)*T2ab(m*nels+n,s*virt+e);
      }
    }
  }
  dE -= 4.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    x += V2ab(m*nact+q,m*nact+s+nels)*pow(F1m(q,s),0.5);
  }
  dE += 4.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    x += V2ab(m*nact+q,m*nact+s+nels)*T1(q,s)*T1(q,s)*pow(F1m(q,s),-0.5);
  }
  dE += 4.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      x += V2ab(m*nact+n,m*nact+s+nels)*T1(n,s)*T1(q,s)*pow(F1m(n,s),-0.5);
    }
  }
  dE -= 4.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab(m*nact+q,m*nact+e+nels)*T1(q,e)*T1(q,s)*pow(F1m(q,e),-0.5);
    }
  }
  dE -= 4.0*x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab(m*nact+e+nels,m*nact+s+nels)*T1(q,e);
    }
  }
  dE += 4.0*x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab(m*nact+s+nels,(e+nels)*nact+q)*T1(m,e);
    }
  }
  dE += 4.0*x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int f=0;f<virt;f++)
    {
      for(int g=0;g<virt;g++)
      {
	x += V2ab(m*nact+s+nels,(f+nels)*nact+g+nels)*T2ab(m*nels+q,f*virt+g);
      }
    }
  }
  dE += 4.0*x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab((e+nels)*nact+s+nels,m*nact+q)*T2ab(m*nels+q,e*virt+s)*pow(F2abM(m*nels+q,e*virt+s),-0.5);
    }
  }
  dE += 4.0*x*T1(q,s);
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      for(int n=0;n<nels;n++)
      {
	x += V2ab((e+nels)*nact+s+nels,m*nact+n)*T2ab(m*nels+n,e*virt+s)*pow(F2abM(m*nels+n,e*virt+s),-0.5);
      }
    }
  }
  dE -= 4.0*x*T1(q,s);
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      for(int f=0;f<virt;f++)
      {
        x += V2ab((e+nels)*nact+f+nels,m*nact+q)*T2ab(m*nels+q,e*virt+f)*pow(F2abM(m*nels+q,e*virt+f),-0.5);
      }
    }
  }
  dE -= 4.0*x*T1(q,s);
*/
  return dE;
}

double dE2aa_dT1(int q,int s,const arma::mat &V2aa,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m,const arma::mat &F2aaM)
{
  int nels = T1.n_rows;
  int virt = T1.n_cols;
  int nact = nels+virt;
  double x=0.0;
  double dE = 0.0;
  double y;

  /*x=0.0;
  for(int n=0;n<nels;n++)
  {
    y=0.0;
    for(int m=0;m<std::min(q,n);m++)
    {
      y += V2aa(indexD(m,n,nact),indexD(m,q,nact))*T1(n,s);
    }
    x += y;
    y=0.0;
    for(int m=q+1;m<n;m++)
    {
      y += V2aa(indexD(m,n,nact),indexD(q,m,nact))*T1(n,s);
    }
    x -= y;
    y=0.0;
    for(int m=n+1;m<q;m++)
    {
      y += V2aa(indexD(n,m,nact),indexD(m,q,nact))*T1(n,s);
    }
    x -= y;
    y = 0.0;
    for(int m=std::max(n,q)+1;m<nels;m++)
    {
      y += V2aa(indexD(n,m,nact),indexD(q,m,nact))*T1(n,s);
    }
    x += y;
  }
  dE -= 2.0*x;


  x=0.0;
  for(int m=0;m<q;m++)
  {
    x += V2aa(indexD(m,q,nact),indexD(m,s+nels,nact))*pow(F1m(q,s),0.5);
  }
  dE += 2.0*x;
  x=0.0;
  for(int m=q+1;m<nels;m++)
  {
    x += V2aa(indexD(q,m,nact),indexD(m,s+nels,nact))*pow(F1m(q,s),0.5);
  }
  dE -= 2.0*x;
  x=0.0;
  for(int n=0;n<nels;n++)
  {
    for(int e=0;e<virt;e++)
    {
      for(int m=0;m<n;m++)
      {
	x += V2aa(indexD(m,n,nact),indexD(m,e+nels,nact))*T2ab(q*nels+n,s*virt+e);
      }
      for(int m=n+1;m<nels;m++)
      {
	x -= V2aa(indexD(n,m,nact),indexD(m,e+nels,nact))*T2ab(q*nels+n,s*virt+e);
      }
    }
  }
  dE += 2.0*x;
  x=0.0;
  for(int e=0;e<s;e++)
  {
    for(int n=0;n<q;n++)
    {
      for(int m=0;m<n;m++)
      {
	x += V2aa(indexD(m,n,nact),indexD(m,e+nels,nact))*T2aa(indexD(n,q,nels),indexD(e,s,virt));
      }
      for(int m=n+1;m<nels;m++)
      {
	x -= V2aa(indexD(n,m,nact),indexD(m,e+nels,nact))*T2aa(indexD(n,q,nels),indexD(e,s,virt));
      }
    }
    for(int n=q+1;n<nels;n++)
    {
      for(int m=0;m<n;m++)
      {
	x -= V2aa(indexD(m,n,nact),indexD(m,e+nels,nact))*T2aa(indexD(q,n,nels),indexD(e,s,virt));
      }
      for(int m=n+1;m<nels;m++)
      {
	x += V2aa(indexD(n,m,nact),indexD(m,e+nels,nact))*T2aa(indexD(q,n,nels),indexD(e,s,virt));
      }
    }
  }
  dE += 2.0*x;
  x=0.0;
  for(int e=s+1;e<virt;e++)
  {
    for(int n=0;n<q;n++)
    {
      for(int m=0;m<n;m++)
      {
	x -= V2aa(indexD(m,n,nact),indexD(m,e+nels,nact))*T2aa(indexD(n,q,nels),indexD(s,e,virt));
      }
      for(int m=n+1;m<nels;m++)
      {
	x += V2aa(indexD(n,m,nact),indexD(m,e+nels,nact))*T2aa(indexD(n,q,nels),indexD(s,e,virt));
      }
    }
    for(int n=q+1;n<nels;n++)
    {
      for(int m=0;m<n;m++)
      {
	x += V2aa(indexD(m,n,nact),indexD(m,e+nels,nact))*T2aa(indexD(q,n,nels),indexD(s,e,virt));
      }
      for(int m=n+1;m<nels;m++)
      {
	x -= V2aa(indexD(n,m,nact),indexD(m,e+nels,nact))*T2aa(indexD(q,n,nels),indexD(s,e,virt));
      }
    }
  }
  dE += 2.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      for(int e=0;e<s;e++)
      {
	x += V2aa(indexD(m,n,nact),indexD(q,e+nels,nact))*T2aa(indexD(m,n,nels),indexD(e,s,virt));
      }
      for(int e=s+1;e<virt;e++)
      {
	x -= V2aa(indexD(m,n,nact),indexD(q,e+nels,nact))*T2aa(indexD(m,n,nels),indexD(s,e,virt));
      }
    }
  }
  dE += 2.0*x;
  x=0.0;
  for(int m=0;m<q;m++)
  {
    x += V2aa(indexD(m,q,nact),indexD(m,s+nels,nact));
  }
  for(int m=q+1;m<nels;m++)
  {
    x -= V2aa(indexD(q,m,nact),indexD(m,s+nels,nact));
  }
  dE += 2.0*x*T1(q,s)*T1(q,s)*pow(F1m(q,s),-0.5);
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      x += V2aa(indexD(m,n,nact),indexD(m,s+nels,nact))*T1(n,s)*pow(F1m(n,s),-0.5);
    }
    for(int n=0;n<m;n++)
    {
      x -= V2aa(indexD(n,m,nact),indexD(m,s+nels,nact))*T1(n,s)*pow(F1m(n,s),-0.5);
    }
  }
  dE -= 2.0*x*T1(q,s);
  x=0.0;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<q;m++)
    {
      x += V2aa(indexD(m,q,nact),indexD(m,e+nels,nact))*T1(q,e)*pow(F1m(q,e),-0.5);
    }
    for(int m=q+1;m<nels;m++)
    {
      x -= V2aa(indexD(q,m,nact),indexD(m,e+nels,nact))*T1(q,e)*pow(F1m(q,e),-0.5);
    }
  }
  dE -= 2.0*x*T1(q,s);

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2aa(indexD(m,e+nels,nact),indexD(m,s+nels,nact))*T1(q,e);
    }
  }
  dE += 2.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2aa(indexD(m,s+nels,nact),indexD(q,e+nels,nact))*T1(m,e);
    }
  }
  dE -= 2.0*x;

  x=0.0;
  for(int f=0;f<virt;f++)
  {
    for(int g=f+1;g<virt;g++)
    {
      for(int m=0;m<q;m++)
      {
	x += V2aa(indexD(m,s+nels,nact),indexD(f+nels,g+nels,nact))*T2aa(indexD(m,q,nels),indexD(f,g,virt));
      }
      for(int m=q+1;m<nels;m++)
      {
	x -= V2aa(indexD(m,s+nels,nact),indexD(f+nels,g+nels,nact))*T2aa(indexD(q,m,nels),indexD(f,g,virt));
      }
    }
  }
  dE += 2.0*x;

  x=0.0;
  for(int m=0;m<q;m++)
  {
    for(int e=0;e<s;e++)
    {
      x += V2aa(indexD(m,q,nact),indexD(e+nels,s+nels,nact))*T2aa(indexD(m,q,nels),indexD(e,s,virt))*pow(F2aaM(indexD(m,q,nels),indexD(e,s,virt)),-0.5);
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2aa(indexD(m,q,nact),indexD(s+nels,e+nels,nact))*T2aa(indexD(m,q,nels),indexD(s,e,virt))*pow(F2aaM(indexD(m,q,nels),indexD(s,e,virt)),-0.5);
    }
  }
  for(int m=q+1;m<nels;m++)
  {
    for(int e=0;e<s;e++)
    {
      x += V2aa(indexD(q,m,nact),indexD(e+nels,s+nels,nact))*T2aa(indexD(q,m,nels),indexD(e,s,virt))*pow(F2aaM(indexD(q,m,nels),indexD(e,s,virt)),-0.5);
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2aa(indexD(q,m,nact),indexD(s+nels,e+nels,nact))*T2aa(indexD(q,m,nels),indexD(s,e,virt))*pow(F2aaM(indexD(q,m,nels),indexD(s,e,virt)),-0.5);
    }
  }
  dE += 2.0*x*T1(q,s);
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      for(int e=0;e<s;e++)
      {
	x += V2aa(indexD(m,n,nact),indexD(e+nels,s+nels,nact))*T2aa(indexD(m,n,nels),indexD(e,s,virt))*pow(F2aaM(indexD(m,n,nels),indexD(e,s,virt)),-0.5);
      }
      for(int e=s+1;e<virt;e++)
      {
	x += V2aa(indexD(m,n,nact),indexD(s+nels,e+nels,nact))*T2aa(indexD(m,n,nels),indexD(s,e,virt))*pow(F2aaM(indexD(m,n,nels),indexD(s,e,virt)),-0.5);
      }
    }
  }
  dE -= 2.0*x*T1(q,s);
  x=0.0;
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      for(int m=0;m<q;m++)
      {
	x += V2aa(indexD(m,q,nact),indexD(e+nels,f+nels,nact))*T2aa(indexD(m,q,nels),indexD(e,f,virt))*pow(F2aaM(indexD(m,q,nels),indexD(e,f,virt)),-0.5);
      }
      for(int m=q+1;m<nels;m++)
      {
	x += V2aa(indexD(q,m,nact),indexD(e+nels,f+nels,nact))*T2aa(indexD(q,m,nels),indexD(e,f,virt))*pow(F2aaM(indexD(q,m,nels),indexD(e,f,virt)),-0.5);;
      }
    }
  }
  dE -= 2.0*x*T1(q,s);
*/
  return dE;
}

double dE2ab_dT2ab(int q,int r,int s,int t,const arma::mat &V2ab,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m,const arma::mat &F2abM)
{
  int virt = T1.n_cols;
  int nels = T1.n_rows;
  int nact = nels+virt;
  double x,y;
  double dE = 0.0;

/*  x=0.0;
  y=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      x += V2ab(m*nact+n,q*nact+n)*T2ab(m*nels+r,s*virt+t);
      y += V2ab(m*nact+n,q*nact+r)*T2ab(m*nels+n,s*virt+t);
    }
  }
  dE -= 4.0*x;
  dE += 2.0*y;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    x += V2ab(m*nact+r,m*nact+t+nels)*T1(q,s);
  }
  dE += 4.0*x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    x += V2ab(q*nact+r,m*nact+t+nels)*T1(m,s);
  }
  dE -= 4.0*x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    x += V2ab(m*nact+q,m*nact+s+nels)*T1(q,s)*T2ab(q*nels+r,s*virt+t)*pow(F1m(q,s),-0.5);
  }
  dE += 4.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      x += V2ab(m*nact+n,m*nact+s+nels)*T1(n,s)*T2ab(q*nels+r,s*virt+t)*pow(F1m(n,s),-0.5);
    }
  }
  dE -= 4.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab(m*nact+q,m*nact+e+nels)*T1(q,e)*T2ab(q*nels+r,s*virt+t)*pow(F1m(q,e),-0.5);
    }
  }
  dE -= 4.0*x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab(m*nact+e+nels,m*nact+s+nels)*T2ab(q*nels+r,e*virt+t);
    }
  }
  dE += 4.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab(m*nact+t+nels,q*nact+e+nels)*T2ab(m*nels+r,s*virt+e);
    }
  }
  dE -= 4.0*x;

  x=0.0;
  for(int m=0;m<q;m++)
  {
    for(int e=0;e<s;e++)
    {
      x += V2ab(m*nact+t+nels,(e+nels)*nact+r)*T2aa(indexD(m,q,nels),indexD(e,s,virt));
    }
    for(int e=s+1;e<virt;e++)
    {
      x -= V2ab(m*nact+t+nels,(e+nels)*nact+r)*T2aa(indexD(m,q,nels),indexD(s,e,virt));
    }
  }
  dE += 2.0*x;
  x=0.0;
  for(int m=q+1;m<nels;m++)
  {
    for(int e=0;e<s;e++)
    {
      x -= V2ab(m*nact+t+nels,(e+nels)*nact+r)*T2aa(indexD(q,m,nels),indexD(e,s,virt));
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2ab(m*nact+t+nels,(e+nels)*nact+r)*T2aa(indexD(q,m,nels),indexD(s,e,virt));
    }
  }
  dE += 2.0*x;
  x=0.0;
  for(int m=0;m<r;m++)
  {
    for(int e=0;e<t;e++)
    {
      x += V2ab(m*nact+s+nels,(e+nels)*nact+q)*T2aa(indexD(m,r,nels),indexD(e,t,virt));
    }
    for(int e=t+1;e<virt;e++)
    {
      x -= V2ab(m*nact+s+nels,(e+nels)*nact+q)*T2aa(indexD(m,r,nels),indexD(t,e,virt));
    }
  }
  dE += 2.0*x;
  x=0.0;
  for(int m=r+1;m<nels;m++)
  {
    for(int e=0;e<t;e++)
    {
      x -= V2ab(m*nact+s+nels,(e+nels)*nact+q)*T2aa(indexD(r,m,nels),indexD(e,t,virt));
    }
    for(int e=t+1;e<virt;e++)
    {
      x += V2ab(m*nact+s+nels,(e+nels)*nact+q)*T2aa(indexD(r,m,nels),indexD(t,e,virt));
    }
  }
  dE += 2.0*x;

  x=0.0;
  for(int e=0;e<virt;e++)
  {
    x += V2ab(q*nact+e+nels,(s+nels)*nact+t+nels)*T1(r,e);
  }
  dE += 4.0*x;

  x=0.0;
  for(int e=0;e<virt;e++)
  {
    for(int f=0;f<virt;f++)
    {
      x += V2ab((e+nels)*nact+f+nels,(s+nels)*nact+t+nels)*T2ab(q*nels+r,e*virt+f);
    }
  }
  dE += 2.0*x;

  dE += 2.0*V2ab((s+nels)*nact+t+nels,q*nact+r)*pow(F2abM(q*nels+r,s*virt+t),0.5);
  dE -= 6.0*V2ab((s+nels)*nact+t+nels,q*nact+r)*T2ab(q*nels+r,s*virt+t)*T2ab(q*nels+r,s*virt+t)*pow(F2abM(q*nels+r,s*virt+t),-0.5);
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    x += V2ab((s+nels)*nact+t+nels,q*nact+m)*T2ab(q*nels+m,s*virt+t)*pow(F2abM(q*nels+m,s*virt+t),-0.5);
    x += V2ab((s+nels)*nact+t+nels,m*nact+r)*T2ab(m*nels+r,s*virt+t)*pow(F2abM(m*nels+r,s*virt+t),-0.5);
  }
  dE += 4.0*x*T2ab(q*nels+r,s*virt+t);
  x=0.0;
  for(int e=0;e<virt;e++)
  {
    x += V2ab((e+nels)*nact+t+nels,q*nact+r)*T2ab(q*nels+r,e*virt+t)*pow(F2abM(q*nels+r,e*virt+t),-0.5);
    x += V2ab((s+nels)*nact+e+nels,q*nact+r)*T2ab(q*nels+r,s*virt+e)*pow(F2abM(q*nels+r,s*virt+e),-0.5);
  }
  dE += 4.0*x*T2ab(q*nels+r,s*virt+t);
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      x += V2ab((s+nels)*nact+t+nels,m*nact+n)*T2ab(m*nels+n,s*virt+t)*pow(F2abM(m*nels+n,s*virt+t),-0.5);
    }
  }
  dE -= 2.0*x*T2ab(q*nels+r,s*virt+t);
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab((e+nels)*nact+t+nels,m*nact+r)*T2ab(m*nels+r,e*virt+t)*pow(F2abM(m*nels+r,e*virt+t),-0.5);
      x += V2ab((s+nels)*nact+e+nels,m*nact+r)*T2ab(m*nels+r,s*virt+e)*pow(F2abM(m*nels+r,s*virt+e),-0.5);
      x += V2ab((e+nels)*nact+t+nels,q*nact+m)*T2ab(q*nels+m,e*virt+t)*pow(F2abM(q*nels+m,e*virt+t),-0.5);
      x += V2ab((s+nels)*nact+e+nels,q*nact+m)*T2ab(q*nels+m,s*virt+e)*pow(F2abM(q*nels+m,s*virt+e),-0.5);;
    }
  }
  dE -= 2.0*x*T2ab(q*nels+r,s*virt+t);
  x=0.0;
  for(int e=0;e<virt;e++)
  {
    for(int f=0;f<virt;f++)
    {
      x += V2ab((e+nels)*nact+f+nels,q*nact+r)*T2ab(q*nels+r,e*virt+f)*pow(F2abM(q*nels+r,e*virt+f),-0.5);
    }
  }
  dE -= 2.0*x*T2ab(q*nels+r,s*virt+t);
*/
  return dE;  
}

double dE2aa_dT2ab(int q,int r,int s,int t,const arma::mat &V2aa,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m,const arma::mat &F2aaM)
{
  int virt = T1.n_cols;
  int nels = T1.n_rows;
  int nact = nels+virt;
  double x,y;
  double dE = 0.0;

//VOILA
/*  x=0.0;
  for(int n=0;n<nels;n++)
  {
    y=0.0;
    for(int m=0;m<std::min(n,q);m++)
    {
      y += V2aa(indexD(m,n,nact),indexD(m,q,nact))*T2ab(n*nels+r,s*virt+t);
    }
    x += y;
    y=0.0;
    for(int m=n+1;m<q;m++)
    {
      y += V2aa(indexD(n,m,nact),indexD(m,q,nact))*T2ab(n*nels+r,s*virt+t);
    }
    x -= y;
    y=0.0;
    for(int m=q+1;m<n;m++)
    {
      y += V2aa(indexD(m,n,nact),indexD(q,m,nact))*T2ab(n*nels+r,s*virt+t);
    }
    x -= y;
    y=0.0;
    for(int m=std::max(n,q)+1;m<nels;m++)
    {
      y += V2aa(indexD(n,m,nact),indexD(q,m,nact))*T2ab(n*nels+r,s*virt+t);
    }
    x += y;
  }
  dE -= 2.0*x;

  x=0.0;
  for(int m=0;m<r;m++)
  {
    x += V2aa(indexD(m,r,nact),indexD(m,t+nels,nact));
  }
  for(int m=r+1;m<nels;m++)
  {
    x -= V2aa(indexD(r,m,nact),indexD(m,t+nels,nact));
  }
  dE += 2.0*x*T1(q,s);

  x=0.0;
  for(int m=0;m<q;m++)
  {
    x += V2aa(indexD(m,q,nact),indexD(m,s+nels,nact));
  }
  for(int m=q+1;m<nels;m++)
  {
    x -= V2aa(indexD(q,m,nact),indexD(m,s+nels,nact));
  }
  dE += 2.0*x*T1(q,s)*T2ab(q*nels+r,s*virt+t)*pow(F1m(q,s),-0.5);
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      x += V2aa(indexD(m,n,nact),indexD(m,s+nels,nact))*T1(n,s)*pow(F1m(n,s),-0.5);
    }
    for(int n=0;n<m;n++)
    {
      x -= V2aa(indexD(n,m,nact),indexD(m,s+nels,nact))*T1(n,s)*pow(F1m(n,s),-0.5);
    }
  }
  dE -= 2.0*x*T2ab(q*nels+r,s*virt+t);
  x=0.0;
  for(int e=0;e<virt;e++)
  {
    for(int m=q+1;m<nels;m++)
    {
      x -= V2aa(indexD(q,m,nact),indexD(m,e+nels,nact))*T1(q,e)*pow(F1m(q,e),-0.5);
    }
    for(int m=0;m<q;m++)
    {
      x += V2aa(indexD(m,q,nact),indexD(m,e+nels,nact))*T1(q,e)*pow(F1m(q,e),-0.5);
    }
  }
  dE -= 2.0*x*T2ab(q*nels+r,s*virt+t);

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2aa(indexD(m,e+nels,nact),indexD(m,s+nels,nact))*T2ab(q*nels+r,e*virt+t);
    }
  }  
  dE += 2.0*x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2aa(indexD(m,s+nels,nact),indexD(q,e+nels,nact))*T2ab(m*nels+r,e*virt+t);
    }
  }  
  dE -= 2.0*x;
  
  x=0.0;
  for(int m=0;m<q;m++)
  {
    for(int e=0;e<s;e++)
    {
      x += V2aa(indexD(m,q,nact),indexD(e+nels,s+nels,nact))*T2aa(indexD(m,q,nels),indexD(e,s,virt))*pow(F2aaM(indexD(m,q,nels),indexD(e,s,virt)),-0.5);
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2aa(indexD(m,q,nact),indexD(s+nels,e+nels,nact))*T2aa(indexD(m,q,nels),indexD(s,e,virt))*pow(F2aaM(indexD(m,q,nels),indexD(s,e,virt)),-0.5);
    }
  }
  for(int m=q+1;m<nels;m++)
  {
    for(int e=0;e<s;e++)
    {
      x += V2aa(indexD(q,m,nact),indexD(e+nels,s+nels,nact))*T2aa(indexD(q,m,nels),indexD(e,s,virt))*pow(F2aaM(indexD(q,m,nels),indexD(e,s,virt)),-0.5);
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2aa(indexD(q,m,nact),indexD(s+nels,e+nels,nact))*T2aa(indexD(q,m,nels),indexD(s,e,virt))*pow(F2aaM(indexD(q,m,nels),indexD(s,e,virt)),-0.5);
    }
  }
  dE -= 2.0*x*T2ab(q*nels+r,s*virt+t);
*/
  return dE;  
}

double dE2ab_dT2aa(int q,int r,int s,int t,const arma::mat &V2ab,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m,const arma::mat &F2abM)
{
  int nels = T1.n_rows;
  int virt = T1.n_cols;
  int nact = nels+virt;
  double x=0.0;
  double dE = 0.0;
  double y;

/*  x=0.0;
  for(int n=0;n<nels;n++)
  {
    y=0.0;
    for(int m=q+1;m<nels;m++)
    {
      y += V2ab(m*nact+n,r*nact+n)*T2aa(indexD(q,m,nels),indexD(s,t,virt));
    }
    x -= 4.0*y;
    y=0.0;
    for(int m=0;m<r;m++)
    {
      y += V2ab(m*nact+n,q*nact+n)*T2aa(indexD(m,r,nels),indexD(s,t,virt));
    }
    x -= 4.0*y;
    y=0.0;
    for(int m=r+1;m<nels;m++)
    {
      y += V2ab(m*nact+n,q*nact+n)*T2aa(indexD(r,m,nels),indexD(s,t,virt));
    }
    x += 4.0*y;
    y=0.0;
    for(int m=0;m<q;m++)
    {
      y += V2ab(m*nact+n,r*nact+n)*T2aa(indexD(m,q,nels),indexD(s,t,virt));
    }
    x += 4.0*y;
  }
  dE += x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    x += V2ab(m*nact+r,m*nact+t+nels)*T1(q,s);
    x -= V2ab(m*nact+r,m*nact+s+nels)*T1(q,t);
    x -= V2ab(m*nact+q,m*nact+t+nels)*T1(r,s);
    x += V2ab(m*nact+q,m*nact+s+nels)*T1(r,t);
  }
  dE += 4.0*x;
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    x += V2ab(m*nact+r,m*nact+t+nels)*T1(r,t)*pow(F1m(r,t),-0.5);
    x += V2ab(m*nact+q,m*nact+t+nels)*T1(q,t)*pow(F1m(q,t),-0.5);
    x += V2ab(m*nact+r,m*nact+s+nels)*T1(r,s)*pow(F1m(r,s),-0.5);
    x += V2ab(m*nact+q,m*nact+s+nels)*T1(q,s)*pow(F1m(q,s),-0.5);
  }
  dE += 4.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      x += V2ab(m*nact+n,m*nact+t+nels)*T1(n,t)*pow(F1m(n,t),-0.5);
      x += V2ab(m*nact+n,m*nact+s+nels)*T1(n,s)*pow(F1m(n,s),-0.5);
    }
  }
  dE -= 4.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab(m*nact+q,m*nact+e+nels)*T1(q,e)*pow(F1m(q,e),-0.5);
      x += V2ab(m*nact+r,m*nact+e+nels)*T1(r,e)*pow(F1m(r,e),-0.5);
    }
  }
  dE -= 4.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<t;e++)
    {
      x += V2ab(m*nact+e+nels,m*nact+s+nels)*T2aa(indexD(q,r,nels),indexD(e,t,virt));
    }
    for(int e=t+1;e<virt;e++)
    {
      x -= V2ab(m*nact+e+nels,m*nact+s+nels)*T2aa(indexD(q,r,nels),indexD(t,e,virt));
    }
    for(int e=0;e<s;e++)
    {
      x -= V2ab(m*nact+e+nels,m*nact+t+nels)*T2aa(indexD(q,r,nels),indexD(e,s,virt));
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2ab(m*nact+e+nels,m*nact+t+nels)*T2aa(indexD(q,r,nels),indexD(s,e,virt));
    }
  }
  dE += 4.0*x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab(q*nact+e+nels,(s+nels)*nact+m)*T2ab(r*nels+m,t*virt+e);
      x -= V2ab(r*nact+e+nels,(s+nels)*nact+m)*T2ab(q*nels+m,t*virt+e);
      x -= V2ab(q*nact+e+nels,(t+nels)*nact+m)*T2ab(r*nels+m,s*virt+e);
      x += V2ab(r*nact+e+nels,(t+nels)*nact+m)*T2ab(q*nels+m,s*virt+e);
    }
  }
  dE += 4.0*x;

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      x += V2ab(m*nact+r,(e+nels)*nact+t+nels)*T2ab(m*nels+r,e*virt+t)*pow(F2abM(m*nels+r,e*virt+t),-0.5);
      x += V2ab(m*nact+q,(e+nels)*nact+t+nels)*T2ab(m*nels+q,e*virt+t)*pow(F2abM(m*nels+q,e*virt+t),-0.5);
      x += V2ab(m*nact+r,(e+nels)*nact+s+nels)*T2ab(m*nels+r,e*virt+s)*pow(F2abM(m*nels+r,e*virt+s),-0.5);
      x += V2ab(m*nact+q,(e+nels)*nact+s+nels)*T2ab(m*nels+q,e*virt+s)*pow(F2abM(m*nels+q,e*virt+s),-0.5);
    }
  }
  dE -= 4.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
*/
  return dE;
}

double dE2aa_dT2aa(int q,int r,int s,int t,const arma::mat &V2aa,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m,const arma::mat &F2aaM)
{
  int nels = T1.n_rows;
  int virt = T1.n_cols;
  int nact = nels+virt;
  double x=0.0;
  double dE = 0.0;
  double y;


/*  x=0.0;
  for(int m=0;m<q;m++)
  {
    y=0.0;
    for(int n=m+1;n<r;n++)
    {
      y += V2aa(indexD(m,n,nact),indexD(m,q,nact))*T2aa(indexD(n,r,nels),indexD(s,t,virt));
    }
    x += y;
    y=0.0;
    for(int n=0;n<std::min(m,r);n++)
    {
      y += V2aa(indexD(n,m,nact),indexD(m,q,nact))*T2aa(indexD(n,r,nels),indexD(s,t,virt));
    }
    x -= y;
    y=0.0;
    for(int n=std::max(m,r)+1;n<nels;n++)
    {
      y += V2aa(indexD(m,n,nact),indexD(m,q,nact))*T2aa(indexD(r,n,nels),indexD(s,t,virt));
    }
    x -= y;
    y=0.0;
    for(int n=r+1;n<m;n++)
    {
      y += V2aa(indexD(n,m,nact),indexD(m,q,nact))*T2aa(indexD(r,n,nels),indexD(s,t,virt));
    }
    x += y;
  }
  dE -= 2.0*x;

  x=0.0;
  for(int m=0;m<r;m++)
  {
    y=0.0;
    for(int n=m+1;n<q;n++)
    {
      y += V2aa(indexD(m,n,nact),indexD(m,r,nact))*T2aa(indexD(n,q,nels),indexD(s,t,virt));
    }
    x -= y;
    y=0.0;
    for(int n=0;n<std::min(m,q);n++)
    {
      y += V2aa(indexD(n,m,nact),indexD(m,r,nact))*T2aa(indexD(n,q,nels),indexD(s,t,virt));
    }
    x += y;
    y=0.0;
    for(int n=std::max(m,q)+1;n<nels;n++)
    {
      y += V2aa(indexD(m,n,nact),indexD(m,r,nact))*T2aa(indexD(q,n,nels),indexD(s,t,virt));
    }
    x += y;
    y=0.0;
    for(int n=q+1;n<m;n++)
    {
      y += V2aa(indexD(n,m,nact),indexD(m,r,nact))*T2aa(indexD(q,n,nels),indexD(s,t,virt));
    }
    x -= y;
  }
  dE -= 2.0*x;
  x=0.0;
  for(int m=q+1;m<nels;m++)
  {
    y=0.0;
    for(int n=m+1;n<r;n++)
    {
      y += V2aa(indexD(m,n,nact),indexD(q,m,nact))*T2aa(indexD(n,r,nels),indexD(s,t,virt));
    }
    x -= y;
    y=0.0;
    for(int n=0;n<std::min(r,m);n++)
    {
      y += V2aa(indexD(n,m,nact),indexD(q,m,nact))*T2aa(indexD(n,r,nels),indexD(s,t,virt));
    }
    x += y;
    y=0.0;
    for(int n=std::max(r,m)+1;n<nels;n++)
    {
      y += V2aa(indexD(m,n,nact),indexD(q,m,nact))*T2aa(indexD(r,n,nels),indexD(s,t,virt));
    }
    x += y;
    y=0.0;
    for(int n=r+1;n<m;n++)
    {
      y += V2aa(indexD(n,m,nact),indexD(q,m,nact))*T2aa(indexD(r,n,nels),indexD(s,t,virt));
    }
    x -= y;
  }
  dE -= 2.0*x;
  x = 0.0;
  for(int m=r+1;m<nels;m++)
  {
    y=0.0;
    for(int n=m+1;n<q;n++)
    {
      y += V2aa(indexD(m,n,nact),indexD(r,m,nact))*T2aa(indexD(n,q,nels),indexD(s,t,virt));
    }
    x += y;
    y=0.0;
    for(int n=0;n<std::min(m,q);n++)
    {
      y += V2aa(indexD(n,m,nact),indexD(r,m,nact))*T2aa(indexD(n,q,nels),indexD(s,t,virt));
    }
    x -= y;
    y=0.0;
    for(int n=std::max(m,q)+1;n<nels;n++)
    {
      y += V2aa(indexD(m,n,nact),indexD(r,m,nact))*T2aa(indexD(q,n,nels),indexD(s,t,virt));
    }
    x -= y;
    y=0.0;
    for(int n=q+1;n<m;n++)
    {
      y += V2aa(indexD(n,m,nact),indexD(r,m,nact))*T2aa(indexD(q,n,nels),indexD(s,t,virt));
    }
    x += y;
  }
  dE -= 2.0*x;

  x = 0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      x += V2aa(indexD(m,n,nact),indexD(q,r,nact))*T2aa(indexD(m,n,nels),indexD(s,t,virt));
    }
  }
  dE += 2.0*x;

  x=0.0;
  for(int m=0;m<r;m++)
  {
    x += V2aa(indexD(m,r,nact),indexD(m,t+nels,nact));
  }
  for(int m=r+1;m<nels;m++)
  {
    x -= V2aa(indexD(r,m,nact),indexD(m,t+nels,nact));
  }
  dE += 2.0*x*T1(q,s);
  x=0.0;
  for(int m=0;m<q;m++)
  {
    x += V2aa(indexD(m,q,nact),indexD(m,t+nels,nact));
  }
  for(int m=q+1;m<nels;m++)
  {
    x -= V2aa(indexD(q,m,nact),indexD(m,t+nels,nact));
  }
  dE -= 2.0*x*T1(r,s);
  x=0.0;
  for(int m=0;m<r;m++)
  {
    x += V2aa(indexD(m,r,nact),indexD(m,s+nels,nact));
  }
  for(int m=r+1;m<nels;m++)
  {
    x -= V2aa(indexD(r,m,nact),indexD(m,s+nels,nact));
  }
  dE -= 2.0*x*T1(q,t);
  x=0.0;
  for(int m=0;m<q;m++)
  {
    x += V2aa(indexD(m,q,nact),indexD(m,s+nels,nact));
  }
  for(int m=q+1;m<nels;m++)
  {
    x -= V2aa(indexD(q,m,nact),indexD(m,s+nels,nact));
  }
  dE += 2.0*x*T1(r,t);
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    x += V2aa(indexD(q,r,nact),indexD(m,t+nels,nact))*T1(m,s);
    x -= V2aa(indexD(q,r,nact),indexD(m,s+nels,nact))*T1(m,t);
  }
  dE -= 2.0*x;
  x=0.0;
  for(int m=0;m<r;m++)
  {
    x += V2aa(indexD(m,r,nact),indexD(m,t+nels,nact));
  }
  for(int m=r+1;m<nels;m++)
  {
    x -= V2aa(indexD(r,m,nact),indexD(m,t+nels,nact));
  }
  dE += 2.0*x*T1(r,t)*pow(F1m(r,t),-0.5)*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  for(int m=0;m<r;m++)
  {
    x += V2aa(indexD(m,r,nact),indexD(m,s+nels,nact));
  }
  for(int m=r+1;m<nels;m++)
  {
    x -= V2aa(indexD(r,m,nact),indexD(m,s+nels,nact));
  }
  dE += 2.0*x*T1(r,s)*pow(F1m(r,s),-0.5)*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  for(int m=0;m<q;m++)
  {
    x += V2aa(indexD(m,q,nact),indexD(m,s+nels,nact));
  }
  for(int m=q+1;m<nels;m++)
  {
    x -= V2aa(indexD(q,m,nact),indexD(m,s+nels,nact));
  }
  dE += 2.0*x*T1(q,s)*pow(F1m(q,s),-0.5)*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  for(int m=0;m<q;m++)
  {
    x += V2aa(indexD(m,q,nact),indexD(m,t+nels,nact));
  }
  for(int m=q+1;m<nels;m++)
  {
    x -= V2aa(indexD(q,m,nact),indexD(m,t+nels,nact));
  }
  dE += 2.0*x*T1(q,t)*pow(F1m(q,t),-0.5)*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  y=0.0;
  for(int n=0;n<nels;n++)
  {
    for(int m=0;m<n;m++)
    {
      x += V2aa(indexD(m,n,nact),indexD(m,t+nels,nact))*T1(n,t)*pow(F1m(n,t),-0.5);
      y += V2aa(indexD(m,n,nact),indexD(m,s+nels,nact))*T1(n,s)*pow(F1m(n,s),-0.5);
    }
    for(int m=n+1;m<nels;m++)
    {
      x -= V2aa(indexD(n,m,nact),indexD(m,t+nels,nact))*T1(n,t)*pow(F1m(n,t),-0.5);
      y -= V2aa(indexD(n,m,nact),indexD(m,s+nels,nact))*T1(n,s)*pow(F1m(n,s),-0.5);
    }
  }
  dE -= 2.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  dE -= 2.0*y*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  y=0.0;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<q;m++)
    {
      x += V2aa(indexD(m,q,nact),indexD(m,e+nels,nact))*T1(q,e)*pow(F1m(q,e),-0.5);
    }
    for(int m=q+1;m<nels;m++)
    {
      x -= V2aa(indexD(q,m,nact),indexD(m,e+nels,nact))*T1(q,e)*pow(F1m(q,e),-0.5);
    }
    for(int m=0;m<r;m++)
    {
      x += V2aa(indexD(m,r,nact),indexD(m,e+nels,nact))*T1(r,e)*pow(F1m(r,e),-0.5);
    }
    for(int m=r+1;m<nels;m++)
    {
      x -= V2aa(indexD(r,m,nact),indexD(m,e+nels,nact))*T1(r,e)*pow(F1m(r,e),-0.5);
    }
  }
  dE -= 2.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  dE -= 2.0*y*T2aa(indexD(q,r,nels),indexD(s,t,virt));

  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<t;e++)
    {
      x += V2aa(indexD(m,e+nels,nact),indexD(m,s+nels,nact))*T2aa(indexD(q,r,nels),indexD(e,t,virt));
    }
    for(int e=t+1;e<virt;e++)
    {
      x -= V2aa(indexD(m,e+nels,nact),indexD(m,s+nels,nact))*T2aa(indexD(q,r,nels),indexD(t,e,virt));
    }
    for(int e=0;e<s;e++)
    {
      x -= V2aa(indexD(m,e+nels,nact),indexD(m,t+nels,nact))*T2aa(indexD(q,r,nels),indexD(e,s,virt));
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2aa(indexD(m,e+nels,nact),indexD(m,t+nels,nact))*T2aa(indexD(q,r,nels),indexD(s,e,virt));
    }
  }
  dE += 2.0*x;

  x =0.0;
  for(int m=0;m<q;m++)
  {
    for(int f=0;f<s;f++)
    {
      x += V2aa(indexD(m,t+nels,nact),indexD(r,f+nels,nact))*T2aa(indexD(m,q,nels),indexD(f,s,virt));
    }
    for(int f=s+1;f<virt;f++)
    {
      x -= V2aa(indexD(m,t+nels,nact),indexD(r,f+nels,nact))*T2aa(indexD(m,q,nels),indexD(s,f,virt));
    }
    for(int f=0;f<t;f++)
    {
      x -= V2aa(indexD(m,s+nels,nact),indexD(r,f+nels,nact))*T2aa(indexD(m,q,nels),indexD(f,t,virt));
    }
    for(int f=t+1;f<virt;f++)
    {
      x += V2aa(indexD(m,s+nels,nact),indexD(r,f+nels,nact))*T2aa(indexD(m,q,nels),indexD(t,f,virt));
    }
  }
  dE -= 2.0*x;
  x =0.0;
  for(int m=q+1;m<nels;m++)
  {
    for(int f=0;f<s;f++)
    {
      x += V2aa(indexD(m,t+nels,nact),indexD(r,f+nels,nact))*T2aa(indexD(q,m,nels),indexD(f,s,virt));
    }
    for(int f=s+1;f<virt;f++)
    {
      x -= V2aa(indexD(m,t+nels,nact),indexD(r,f+nels,nact))*T2aa(indexD(q,m,nels),indexD(s,f,virt));
    }
    for(int f=0;f<t;f++)
    {
      x -= V2aa(indexD(m,s+nels,nact),indexD(r,f+nels,nact))*T2aa(indexD(q,m,nels),indexD(f,t,virt));
    }
    for(int f=t+1;f<virt;f++)
    {
      x += V2aa(indexD(m,s+nels,nact),indexD(r,f+nels,nact))*T2aa(indexD(q,m,nels),indexD(t,f,virt));
    }
  }
  dE += 2.0*x;
  x =0.0;
  for(int m=0;m<r;m++)
  {
    for(int f=0;f<s;f++)
    {
      x += V2aa(indexD(m,t+nels,nact),indexD(q,f+nels,nact))*T2aa(indexD(m,r,nels),indexD(f,s,virt));
    }
    for(int f=s+1;f<virt;f++)
    {
      x -= V2aa(indexD(m,t+nels,nact),indexD(q,f+nels,nact))*T2aa(indexD(m,r,nels),indexD(s,f,virt));
    }
    for(int f=0;f<t;f++)
    {
      x -= V2aa(indexD(m,s+nels,nact),indexD(q,f+nels,nact))*T2aa(indexD(m,r,nels),indexD(f,t,virt));
    }
    for(int f=t+1;f<virt;f++)
    {
      x += V2aa(indexD(m,s+nels,nact),indexD(q,f+nels,nact))*T2aa(indexD(m,r,nels),indexD(t,f,virt));
    }
  }
  dE += 2.0*x;
  x =0.0;
  for(int m=r+1;m<nels;m++)
  {
    for(int f=0;f<s;f++)
    {
      x += V2aa(indexD(m,t+nels,nact),indexD(q,f+nels,nact))*T2aa(indexD(r,m,nels),indexD(f,s,virt));
    }
    for(int f=s+1;f<virt;f++)
    {
      x -= V2aa(indexD(m,t+nels,nact),indexD(q,f+nels,nact))*T2aa(indexD(r,m,nels),indexD(s,f,virt));
    }
    for(int f=0;f<t;f++)
    {
      x -= V2aa(indexD(m,s+nels,nact),indexD(q,f+nels,nact))*T2aa(indexD(r,m,nels),indexD(f,t,virt));
    }
    for(int f=t+1;f<virt;f++)
    {
      x += V2aa(indexD(m,s+nels,nact),indexD(q,f+nels,nact))*T2aa(indexD(r,m,nels),indexD(t,f,virt));
    }
  }
  dE -= 2.0*x;

  x=0.0;
  for(int e=0;e<virt;e++)
  {
    x += V2aa(indexD(q,e+nels,nact),indexD(s+nels,t+nels,nact))*T1(r,e);
    x -= V2aa(indexD(r,e+nels,nact),indexD(s+nels,t+nels,nact))*T1(q,e);
  }
  dE += 2.0*x;

  x=0.0;
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      x += V2aa(indexD(e+nels,f+nels,nact),indexD(s+nels,t+nels,nact))*T2aa(indexD(q,r,nels),indexD(e,f,virt));
    }
  }
  dE += 2.0*x;

  dE += 2.0*V2aa(indexD(q,r,nact),indexD(s+nels,t+nels,nact))*pow(F2aaM(indexD(q,r,nels),indexD(s,t,virt)),0.5);
  dE -= 6.0*V2aa(indexD(q,r,nact),indexD(s+nels,t+nels,nact))*pow(F2aaM(indexD(q,r,nels),indexD(s,t,virt)),-0.5)*T2aa(indexD(q,r,nels),indexD(s,t,virt))*T2aa(indexD(q,r,nels),indexD(s,t,virt));

  x=0.0;
  for(int m=0;m<r;m++)
  {
    x += V2aa(indexD(m,r,nact),indexD(s+nels,t+nels,nact))*T2aa(indexD(m,r,nels),indexD(s,t,virt))*pow(F2aaM(indexD(m,r,nels),indexD(s,t,virt)),-0.5);
  }
  for(int m=r+1;m<nels;m++)
  {
    x += V2aa(indexD(r,m,nact),indexD(s+nels,t+nels,nact))*T2aa(indexD(r,m,nels),indexD(s,t,virt))*pow(F2aaM(indexD(r,m,nels),indexD(s,t,virt)),-0.5);
  }
  for(int m=0;m<q;m++)
  {
    x += V2aa(indexD(m,q,nact),indexD(s+nels,t+nels,nact))*T2aa(indexD(m,q,nels),indexD(s,t,virt))*pow(F2aaM(indexD(m,q,nels),indexD(s,t,virt)),-0.5);
  }
  for(int m=q+1;m<nels;m++)
  {
    x += V2aa(indexD(q,m,nact),indexD(s+nels,t+nels,nact))*T2aa(indexD(q,m,nels),indexD(s,t,virt))*pow(F2aaM(indexD(q,m,nels),indexD(s,t,virt)),-0.5);
  }
  dE += 4.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));

  x=0.0;
  for(int e=0;e<s;e++)
  {
    x += V2aa(indexD(q,r,nact),indexD(e+nels,s+nels,nact))*T2aa(indexD(q,r,nels),indexD(e,s,virt))*pow(F2aaM(indexD(q,r,nels),indexD(e,s,virt)),-0.5);
  }
  for(int e=s+1;e<virt;e++)
  {
    x += V2aa(indexD(q,r,nact),indexD(s+nels,e+nels,nact))*T2aa(indexD(q,r,nels),indexD(s,e,virt))*pow(F2aaM(indexD(q,r,nels),indexD(s,e,virt)),-0.5);
  }
  for(int e=0;e<t;e++)
  {
    x += V2aa(indexD(q,r,nact),indexD(e+nels,t+nels,nact))*T2aa(indexD(q,r,nels),indexD(e,t,virt))*pow(F2aaM(indexD(q,r,nels),indexD(e,t,virt)),-0.5);
  }
  for(int e=t+1;e<virt;e++)
  {
    x += V2aa(indexD(q,r,nact),indexD(t+nels,e+nels,nact))*T2aa(indexD(q,r,nels),indexD(t,e,virt))*pow(F2aaM(indexD(q,r,nels),indexD(t,e,virt)),-0.5);
  }
  dE += 4.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      x += V2aa(indexD(m,n,nact),indexD(s+nels,t+nels,nact))*T2aa(indexD(m,n,nels),indexD(s,t,virt))*pow(F2aaM(indexD(m,n,nels),indexD(s,t,virt)),-0.5);
    }
  }
  dE -= 2.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      x += V2aa(indexD(q,r,nact),indexD(e+nels,f+nels,nact))*T2aa(indexD(q,r,nels),indexD(e,f,virt))*pow(F2aaM(indexD(q,r,nels),indexD(e,f,virt)),-0.5);
    }
  }
  dE -= 2.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
*/
/*  x=0.0;
  for(int m=0;m<q;m++)
  {
    for(int e=0;e<s;e++)
    {
      x += V2aa(indexD(m,q,nact),indexD(e+nels,s+nels,nact))*T2aa(indexD(m,q,nels),indexD(e,s,virt))*pow(F2aaM(indexD(m,q,nels),indexD(e,s,virt)),-0.5);
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2aa(indexD(m,q,nact),indexD(s+nels,e+nels,nact))*T2aa(indexD(m,q,nels),indexD(s,e,virt))*pow(F2aaM(indexD(m,q,nels),indexD(s,e,virt)),-0.5);
    }
    for(int e=0;e<t;e++)
    {
      x += V2aa(indexD(m,q,nact),indexD(e+nels,t+nels,nact))*T2aa(indexD(m,q,nels),indexD(e,t,virt))*pow(F2aaM(indexD(m,q,nels),indexD(e,t,virt)),-0.5);
    }
    for(int e=t+1;e<virt;e++)
    {
      x += V2aa(indexD(m,q,nact),indexD(t+nels,e+nels,nact))*T2aa(indexD(m,q,nels),indexD(t,e,virt))*pow(F2aaM(indexD(m,q,nels),indexD(t,e,virt)),-0.5);
    }
  }
  dE -= 2.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  for(int m=q+1;m<nels;m++)
  {
    for(int e=0;e<s;e++)
    {
      x += V2aa(indexD(q,m,nact),indexD(e+nels,s+nels,nact))*T2aa(indexD(q,m,nels),indexD(e,s,virt))*pow(F2aaM(indexD(q,m,nels),indexD(e,s,virt)),-0.5);
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2aa(indexD(q,m,nact),indexD(s+nels,e+nels,nact))*T2aa(indexD(q,m,nels),indexD(s,e,virt))*pow(F2aaM(indexD(q,m,nels),indexD(s,e,virt)),-0.5);
    }
    for(int e=0;e<t;e++)
    {
      x += V2aa(indexD(q,m,nact),indexD(e+nels,t+nels,nact))*T2aa(indexD(q,m,nels),indexD(e,t,virt))*pow(F2aaM(indexD(q,m,nels),indexD(e,t,virt)),-0.5);
    }
    for(int e=t+1;e<virt;e++)
    {
      x += V2aa(indexD(q,m,nact),indexD(t+nels,e+nels,nact))*T2aa(indexD(q,m,nels),indexD(t,e,virt))*pow(F2aaM(indexD(q,m,nels),indexD(t,e,virt)),-0.5);
    }
  }
  dE -= 2.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  for(int m=0;m<r;m++)
  {
    for(int e=0;e<s;e++)
    {
      x += V2aa(indexD(m,r,nact),indexD(e+nels,s+nels,nact))*T2aa(indexD(m,r,nels),indexD(e,s,virt))*pow(F2aaM(indexD(m,r,nels),indexD(e,s,virt)),-0.5);
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2aa(indexD(m,r,nact),indexD(s+nels,e+nels,nact))*T2aa(indexD(m,r,nels),indexD(s,e,virt))*pow(F2aaM(indexD(m,r,nels),indexD(s,e,virt)),-0.5);
    }
    for(int e=0;e<t;e++)
    {
      x += V2aa(indexD(m,r,nact),indexD(e+nels,t+nels,nact))*T2aa(indexD(m,r,nels),indexD(e,t,virt))*pow(F2aaM(indexD(m,r,nels),indexD(e,t,virt)),-0.5);
    }
    for(int e=t+1;e<virt;e++)
    {
      x += V2aa(indexD(m,r,nact),indexD(t+nels,e+nels,nact))*T2aa(indexD(m,r,nels),indexD(t,e,virt))*pow(F2aaM(indexD(m,r,nels),indexD(t,e,virt)),-0.5);
    }
  }
  dE -= 2.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
  x=0.0;
  for(int m=r+1;m<nels;m++)
  {
    for(int e=0;e<s;e++)
    {
      x += V2aa(indexD(r,m,nact),indexD(e+nels,s+nels,nact))*T2aa(indexD(r,m,nels),indexD(e,s,virt))*pow(F2aaM(indexD(r,m,nels),indexD(e,s,virt)),-0.5);
    }
    for(int e=s+1;e<virt;e++)
    {
      x += V2aa(indexD(r,m,nact),indexD(s+nels,e+nels,nact))*T2aa(indexD(r,m,nels),indexD(s,e,virt))*pow(F2aaM(indexD(r,m,nels),indexD(s,e,virt)),-0.5);
    }
    for(int e=0;e<t;e++)
    {
      x += V2aa(indexD(r,m,nact),indexD(e+nels,t+nels,nact))*T2aa(indexD(r,m,nels),indexD(e,t,virt))*pow(F2aaM(indexD(r,m,nels),indexD(e,t,virt)),-0.5);
    }
    for(int e=t+1;e<virt;e++)
    {
      x += V2aa(indexD(r,m,nact),indexD(t+nels,e+nels,nact))*T2aa(indexD(r,m,nels),indexD(t,e,virt))*pow(F2aaM(indexD(r,m,nels),indexD(t,e,virt)),-0.5);
    }
  }
  dE -= 2.0*x*T2aa(indexD(q,r,nels),indexD(s,t,virt));
*/
  return dE;
}

double F1(int m,int e,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab)
{
  int nels=T1.n_rows;
  int virt=T1.n_cols;

  double x=1.0;
  double y=0.0;
 
  x += T1(m,e)*T1(m,e);
  y=0.0;
  for(int i=0;i<nels;i++)
  {
    y += T1(i,e)*T1(i,e);
  }
  x -= y;
  y=0.0;
  for(int a=0;a<virt;a++)
  {
    y += T1(m,a)*T1(m,a);
  }
  x -= y;

  y=0.0;
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      y += T2ab(m*nels+i,e*virt+a)*T2ab(m*nels+i,e*virt+a);
      for(int j=0;j<nels;j++)
      {
	y -= T2ab(i*nels+j,e*virt+a)*T2ab(i*nels+j,e*virt+a);
      }
      for(int b=0;b<virt;b++)
      {
	y -= T2ab(m*nels+i,a*virt+b)*T2ab(m*nels+i,a*virt+b);
      }
    }
  }
  x += y;

  y = 0.0;
  for(int i=0;i<m;i++)
  {
    for(int a=0;a<e;a++)
    {
      y += T2aa(indexD(i,m,nels),indexD(a,e,virt))*T2aa(indexD(i,m,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      y += T2aa(indexD(i,m,nels),indexD(e,a,virt))*T2aa(indexD(i,m,nels),indexD(e,a,virt));
    }
  }
  x += y;
  y = 0.0;
  for(int i=m+1;i<nels;i++)
  {
    for(int a=0;a<e;a++)
    {
      y += T2aa(indexD(m,i,nels),indexD(a,e,virt))*T2aa(indexD(m,i,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      y += T2aa(indexD(m,i,nels),indexD(e,a,virt))*T2aa(indexD(m,i,nels),indexD(e,a,virt));
    }
  }
  x += y;
  y = 0.0;
  for(int i=0;i<nels;i++)
  {
    for(int j=i+1;j<nels;j++)
    {
      for(int a=0;a<e;a++)
      {
	y += T2aa(indexD(i,j,nels),indexD(a,e,virt))*T2aa(indexD(i,j,nels),indexD(a,e,virt));
      }
      for(int a=e+1;a<virt;a++)
      {
	y += T2aa(indexD(i,j,nels),indexD(e,a,virt))*T2aa(indexD(i,j,nels),indexD(e,a,virt));
      }
    }
  }
  x -= y;
  y = 0.0;
  for(int a=0;a<virt;a++)
  {
    for(int b=a+1;b<virt;b++)
    {
      for(int i=0;i<m;i++)
      {
	y += T2aa(indexD(i,m,nels),indexD(a,b,virt))*T2aa(indexD(i,m,nels),indexD(a,b,virt));
      }
      for(int i=m+1;i<nels;i++)
      {
	y += T2aa(indexD(m,i,nels),indexD(a,b,virt))*T2aa(indexD(m,i,nels),indexD(a,b,virt));
      }
    }
  }
  x -= y;

  return x;
}

double F2ab(int m,int n,int e,int f,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab)
{
  int nels=T1.n_rows;
  int virt=T1.n_cols;

  double x=1.0;
  double y=0.0;

  x -= 3.0*T2ab(m*nels+n,e*virt+f)*T2ab(m*nels+n,e*virt+f);
  y=0.0;
  for(int i=0;i<nels;i++)
  {
    y += T2ab(m*nels+i,e*virt+f)*T2ab(m*nels+i,e*virt+f);
    y += T2ab(i*nels+n,e*virt+f)*T2ab(i*nels+n,e*virt+f);
  }
  x += 2.0*y;
  y = 0.0;
  for(int a=0;a<virt;a++)
  {
    y += T2ab(m*nels+n,e*virt+a)*T2ab(m*nels+n,e*virt+a);
    y += T2ab(m*nels+n,a*virt+f)*T2ab(m*nels+n,a*virt+f);
  }
  x += 2.0*y;
  y = 0.0;
  for(int i=0;i<nels;i++)
  {
    for(int j=0;j<nels;j++)
    {
      y += T2ab(i*nels+j,e*virt+f)*T2ab(i*nels+j,e*virt+f);
    }
  }
  x -= y;
  y = 0.0;
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      y += T2ab(i*nels+n,a*virt+f)*T2ab(i*nels+n,a*virt+f);
      y += T2ab(i*nels+n,e*virt+a)*T2ab(i*nels+n,e*virt+a);
      y += T2ab(m*nels+i,a*virt+f)*T2ab(m*nels+i,a*virt+f);
      y += T2ab(m*nels+i,e*virt+a)*T2ab(m*nels+i,e*virt+a);
    }
  }
  x -= y;
  y = 0.0;
  for(int a=0;a<virt;a++)
  {
    for(int b=0;b<virt;b++)
    {
      y += T2ab(m*nels+n,a*virt+b)*T2ab(m*nels+n,a*virt+b);
    }
  }
  x -= y;

  y = 0.0;
  for(int i=0;i<n;i++)
  {
    for(int a=0;a<f;a++)
    {
      y += T2aa(indexD(i,n,nels),indexD(a,f,virt))*T2aa(indexD(i,n,nels),indexD(a,f,virt));
    }
    for(int a=f+1;a<virt;a++)
    {
      y += T2aa(indexD(i,n,nels),indexD(f,a,virt))*T2aa(indexD(i,n,nels),indexD(f,a,virt));
    }
  }
  x -= y;
  y = 0.0;
  for(int i=n+1;i<nels;i++)
  {
    for(int a=0;a<f;a++)
    {
      y += T2aa(indexD(n,i,nels),indexD(a,f,virt))*T2aa(indexD(n,i,nels),indexD(a,f,virt));
    }
    for(int a=f+1;a<virt;a++)
    {
      y += T2aa(indexD(n,i,nels),indexD(f,a,virt))*T2aa(indexD(n,i,nels),indexD(f,a,virt));
    }
  }
  x -= y;
  y = 0.0;
  for(int i=0;i<m;i++)
  {
    for(int a=0;a<e;a++)
    {
      y += T2aa(indexD(i,m,nels),indexD(a,e,virt))*T2aa(indexD(i,m,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      y += T2aa(indexD(i,m,nels),indexD(e,a,virt))*T2aa(indexD(i,m,nels),indexD(e,a,virt));
    }
  }
  x -= y;
  y = 0.0;
  for(int i=m+1;i<nels;i++)
  {
    for(int a=0;a<e;a++)
    {
      y += T2aa(indexD(m,i,nels),indexD(a,e,virt))*T2aa(indexD(m,i,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      y += T2aa(indexD(m,i,nels),indexD(e,a,virt))*T2aa(indexD(m,i,nels),indexD(e,a,virt));
    }
  }
  x -= y;

  x += T1(m,e)*T1(m,e);
  x += T1(n,f)*T1(n,f);
  y=0.0;
  for(int i=0;i<nels;i++)
  {
    y += T1(i,e)*T1(i,e);
    y += T1(i,f)*T1(i,f);
  }
  x -= y;
  y = 0.0;
  for(int a=0;a<virt;a++)
  {
    y += T1(m,a)*T1(m,a);
    y += T1(n,a)*T1(n,a);
  }
  x -= y;

  return x;
}

double F2aa(int m,int n,int e,int f,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab)
{
  int nels=T1.n_rows;
  int virt=T1.n_cols;

  double x=1.0;
  double y=0.0;
 
  x -= 3.0*T2aa(indexD(m,n,nels),indexD(e,f,virt))*T2aa(indexD(m,n,nels),indexD(e,f,virt));
  y=0.0;
  for(int i=0;i<n;i++)
  {
    y += T2aa(indexD(i,n,nels),indexD(e,f,virt))*T2aa(indexD(i,n,nels),indexD(e,f,virt));
  }
  x += 2.0*y;
  y=0.0;
  for(int i=n+1;i<nels;i++)
  {
    y += T2aa(indexD(n,i,nels),indexD(e,f,virt))*T2aa(indexD(n,i,nels),indexD(e,f,virt));
  }
  x += 2.0*y;
  y=0.0;
  for(int i=0;i<m;i++)
  {
    y += T2aa(indexD(i,m,nels),indexD(e,f,virt))*T2aa(indexD(i,m,nels),indexD(e,f,virt));
  }
  x += 2.0*y;
  y=0.0;
  for(int i=m+1;i<nels;i++)
  {
    y += T2aa(indexD(m,i,nels),indexD(e,f,virt))*T2aa(indexD(m,i,nels),indexD(e,f,virt));
  }
  x += 2.0*y;
  y=0.0;
  for(int a=0;a<e;a++)
  {
    y += T2aa(indexD(m,n,nels),indexD(a,e,virt))*T2aa(indexD(m,n,nels),indexD(a,e,virt));
  }
  x += 2.0*y;
  y=0.0;
  for(int a=e+1;a<virt;a++)
  {
    y += T2aa(indexD(m,n,nels),indexD(e,a,virt))*T2aa(indexD(m,n,nels),indexD(e,a,virt));
  }
  x += 2.0*y;
  y=0.0;
  for(int a=0;a<f;a++)
  {
    y += T2aa(indexD(m,n,nels),indexD(a,f,virt))*T2aa(indexD(m,n,nels),indexD(a,f,virt));
  }
  x += 2.0*y;
  y=0.0;
  for(int a=f+1;a<virt;a++)
  {
    y += T2aa(indexD(m,n,nels),indexD(f,a,virt))*T2aa(indexD(m,n,nels),indexD(f,a,virt));
  }
  x += 2.0*y;

  y = 0.0;
  for(int i=0;i<nels;i++)
  {
    for(int j=i+1;j<nels;j++)
    {
      y += T2aa(indexD(i,j,nels),indexD(e,f,virt))*T2aa(indexD(i,j,nels),indexD(e,f,virt));
    }
  }
  x -= y;
  y=0.0;
  for(int a=0;a<virt;a++)
  {
    for(int b=a+1;b<virt;b++)
    {
      y += T2aa(indexD(m,n,nels),indexD(a,b,virt))*T2aa(indexD(m,n,nels),indexD(a,b,virt));
    }
  }
  x -= y;
  y = 0.0;
  for(int i=0;i<m;i++)
  {
    for(int a=0;a<e;a++)
    {
      y += T2aa(indexD(i,m,nels),indexD(a,e,virt))*T2aa(indexD(i,m,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      y += T2aa(indexD(i,m,nels),indexD(e,a,virt))*T2aa(indexD(i,m,nels),indexD(e,a,virt));
    }
    for(int a=0;a<f;a++)
    {
      y += T2aa(indexD(i,m,nels),indexD(a,f,virt))*T2aa(indexD(i,m,nels),indexD(a,f,virt));
    }
    for(int a=f+1;a<virt;a++)
    {
      y += T2aa(indexD(i,m,nels),indexD(f,a,virt))*T2aa(indexD(i,m,nels),indexD(f,a,virt));
    }
  }
  x -= y;
  y = 0.0;
  for(int i=m+1;i<nels;i++)
  {
    for(int a=0;a<e;a++)
    {
      y += T2aa(indexD(m,i,nels),indexD(a,e,virt))*T2aa(indexD(m,i,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      y += T2aa(indexD(m,i,nels),indexD(e,a,virt))*T2aa(indexD(m,i,nels),indexD(e,a,virt));
    }
    for(int a=0;a<f;a++)
    {
      y += T2aa(indexD(m,i,nels),indexD(a,f,virt))*T2aa(indexD(m,i,nels),indexD(a,f,virt));
    }
    for(int a=f+1;a<virt;a++)
    {
      y += T2aa(indexD(m,i,nels),indexD(f,a,virt))*T2aa(indexD(m,i,nels),indexD(f,a,virt));
    }
  }
  x -= y;
  y = 0.0;
  for(int i=0;i<n;i++)
  {
    for(int a=0;a<e;a++)
    {
      y += T2aa(indexD(i,n,nels),indexD(a,e,virt))*T2aa(indexD(i,n,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      y += T2aa(indexD(i,n,nels),indexD(e,a,virt))*T2aa(indexD(i,n,nels),indexD(e,a,virt));
    }
    for(int a=0;a<f;a++)
    {
      y += T2aa(indexD(i,n,nels),indexD(a,f,virt))*T2aa(indexD(i,n,nels),indexD(a,f,virt));
    }
    for(int a=f+1;a<virt;a++)
    {
      y += T2aa(indexD(i,n,nels),indexD(f,a,virt))*T2aa(indexD(i,n,nels),indexD(f,a,virt));
    }
  }
  x -= y;
  y = 0.0;
  for(int i=n+1;i<nels;i++)
  {
    for(int a=0;a<e;a++)
    {
      y += T2aa(indexD(n,i,nels),indexD(a,e,virt))*T2aa(indexD(n,i,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      y += T2aa(indexD(n,i,nels),indexD(e,a,virt))*T2aa(indexD(n,i,nels),indexD(e,a,virt));
    }
    for(int a=0;a<f;a++)
    {
      y += T2aa(indexD(n,i,nels),indexD(a,f,virt))*T2aa(indexD(n,i,nels),indexD(a,f,virt));
    }
    for(int a=f+1;a<virt;a++)
    {
      y += T2aa(indexD(n,i,nels),indexD(f,a,virt))*T2aa(indexD(n,i,nels),indexD(f,a,virt));
    }
  }
  x -= y;

  y = 0.0;
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      y += T2ab(m*nels+i,e*virt+a)*T2ab(m*nels+i,e*virt+a);
      y += T2ab(n*nels+i,e*virt+a)*T2ab(n*nels+i,e*virt+a);
      y += T2ab(m*nels+i,f*virt+a)*T2ab(m*nels+i,f*virt+a);
      y += T2ab(n*nels+i,f*virt+a)*T2ab(n*nels+i,f*virt+a);
    }
  }
  x -= y;

  x += T1(m,e)*T1(m,e);
  x += T1(m,f)*T1(m,f);
  x += T1(n,e)*T1(n,e);
  x += T1(n,f)*T1(n,f);
  y = 0.0;
  for(int i=0;i<nels;i++)
  {
    y += T1(i,e)*T1(i,e);
    y += T1(i,f)*T1(i,f);
  }
  x -= y;
  y = 0.0;
  for(int a=0;a<virt;a++)
  {
    y += T1(m,a)*T1(m,a);
    y += T1(n,a)*T1(n,a);
  }
  x -= y;

  return x;
}

void calcFci(arma::mat &F1m,arma::mat &F2aaM,arma::mat &F2abM,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,time_t &tim)
{
  int nels=T1.n_rows;
  int virt=T1.n_cols;

  double x = 0.0;
  for(int i=0;i<nels;i++)
  {
    for(int j=0;j<nels;j++)
    {
      for(int a=0;a<virt;a++)
      {
	for(int b=0;b<virt;b++)
	{
	  x += T2ab(i*nels+j,a*virt+b)*T2ab(i*nels+j,a*virt+b);
	}
      }
    }
  }
//  std::cout << '\t' << x << '\t' << T2ab.max() << '\t' << T2ab.min() << std::endl;
  for(int i=0;i<nels;i++)
  {
    for(int j=i+1;j<nels;j++)
    {
      for(int a=0;a<virt;a++)
      {
	for(int b=a+1;b<virt;b++)
	{
	  x += 2.0*T2aa(indexD(i,j,nels),indexD(a,b,virt))*T2aa(indexD(i,j,nels),indexD(a,b,virt));
	}
      }
    }
  }
//  std::cout << "\t\t" << x << std::endl;
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      x += 2.0*T1(i,a)*T1(i,a);
    }
  }
//  std::cout << "\t\t\t" << x << std::endl;
  //std::cout << '\t' << T1.max() << '\t' << T1.min() << std::endl;
  //double c0= 1.0;
  double c0= 1.0-x;

  if(c0 < 0)
  {
    c0=0.0;
  }
 
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      F1m(i,a)=c0;
    }
  }
  for(int i=0;i<nels;i++)
  {
    for(int j=i+1;j<nels;j++)
    {
      for(int a=0;a<virt;a++)
      {
	for(int b=a+1;b<virt;b++)
	{
	  F2aaM(indexD(i,j,nels),indexD(a,b,virt))=c0;
	}
      }
    }
  }
  for(int i=0;i<nels;i++)
  {
    for(int j=0;j<nels;j++)
    {
      for(int a=0;a<virt;a++)
      {
	for(int b=0;b<virt;b++)
	{
	  F2abM(i*nels+j,a*virt+b)=c0;
	}
      }
    }
  }

  return;
}


double dij(int m,int n,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab)
{
  int nels = T1.n_rows;
  int virt = T1.n_cols;
  int nact = nels+nact;
  double x=0.0;
  double y=0.0;

  y = 0.0;
  for(int a=0;a<virt;a++)
  {
    for(int b=0;b<virt;b++)
    {
      for(int j=0;j<nels;j++)
      {
	y += T2ab(m*nels+j,a*virt+b)*T2ab(n*nels+j,a*virt+b);
      }
    }
  }
  x += y;
  y = 0.0;
  for(int a=0;a<virt;a++)
  {
    for(int b=a+1;b<virt;b++)
    {
      for(int i=0;i<std::min(m,n);i++)
      {
	y += T2aa(indexD(i,m,nels),indexD(a,b,virt))*T2aa(indexD(i,n,nels),indexD(a,b,virt));
      }
      for(int i=m+1;i<n;i++)
      {
	y -= T2aa(indexD(m,i,nels),indexD(a,b,virt))*T2aa(indexD(i,n,nels),indexD(a,b,virt));
      }
      for(int i=n+1;i<m;i++)
      {
	y -= T2aa(indexD(i,m,nels),indexD(a,b,virt))*T2aa(indexD(n,i,nels),indexD(a,b,virt));
      }
      for(int i=std::max(m,n)+1;i<nels;i++)
      {
	y += T2aa(indexD(m,i,nels),indexD(a,b,virt))*T2aa(indexD(n,i,nels),indexD(a,b,virt));
      }
    }
  }
  x += y;
  y=0.0;
  for(int a=0;a<virt;a++)
  {
    y += T1(m,a)*T1(n,a);
  }
  x += y;

  return -x;
}

double dab(int e,int f,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab)
{
  int nels=T1.n_rows;
  int virt=T1.n_cols;
  double x=0.0;
  double y=0.0;

  for(int i=0;i<nels;i++)
  {
    for(int j=0;j<nels;j++)
    {
      for(int b=0;b<virt;b++)
      {
	y += T2ab(i*nels+j,e*virt+b)*T2ab(i*nels+j,f*virt+b);
      }
    }
  }
  x += y;

  y = 0.0;
  for(int i=0;i<nels;i++)
  {
    for(int j=i+1;j<nels;j++)
    {
      for(int a=0;a<e;a++)
      {
	if(a < f)
	{
	  y += T2aa(indexD(i,j,nels),indexD(a,e,virt))*T2aa(indexD(i,j,nels),indexD(a,f,virt));
	}
	else if(a > f)
	{
	  y -= T2aa(indexD(i,j,nels),indexD(a,e,virt))*T2aa(indexD(i,j,nels),indexD(f,a,virt));
	}
      }
      for(int a=e+1;a<virt;a++)
      {
	if(a < f)
	{
	  y -= T2aa(indexD(i,j,nels),indexD(e,a,virt))*T2aa(indexD(i,j,nels),indexD(a,f,virt));
	}
	else if (a > f)
	{
	  y += T2aa(indexD(i,j,nels),indexD(e,a,virt))*T2aa(indexD(i,j,nels),indexD(f,a,virt));
	}
      }
    }
  }
  x += y;
  y=0.0;
  for(int i=0;i<nels;i++)
  {
    y += T1(i,e)*T1(i,f);
  }
  x += y;

  return x;
}

double dia(int m,int e,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,const arma::mat &F1m)
{
  int nels=T1.n_rows;
  int virt=T1.n_cols;
  double x=0.0;
  double y=0.0;

  x = T1(m,e)*pow(F1m(m,e),0.5);
  y=0.0;
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      y += T1(i,a)*T2ab(i*nels+m,a*virt+e);
    }
  }  
  x += y;
  y = 0.0;
  for(int i=0;i<m;i++)
  {
    for(int a=0;a<e;a++)
    {
      y += T1(i,a)*T2aa(indexD(i,m,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      y -= T1(i,a)*T2aa(indexD(i,m,nels),indexD(e,a,virt));
    }
  }
  for(int i=m+1;i<nels;i++)
  {
    for(int a=0;a<e;a++)
    {
      y -= T1(i,a)*T2aa(indexD(m,i,nels),indexD(a,e,virt));
    }
    for(int a=e+1;a<virt;a++)
    {
      y += T1(i,a)*T2aa(indexD(m,i,nels),indexD(e,a,virt));
    }
  }
  x += y;

  return x;
}


void makeD2p(arma::mat &D1,arma::mat &D2aa,arma::mat &D2ab,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,arma::mat &F1m,arma::mat &F2aaM,arma::mat &F2abM,time_t &tim,void *ptr)
{
  intBundle *iPtr=static_cast<intBundle*>(ptr);
  time_t startTime,lastTime;
  startTime = time(NULL);
  int nact = D1.n_rows;
  int nels = T1.n_rows;
  int virt = T1.n_cols;
  nact = nels+virt;
  D1.zeros(); D2aa.zeros(); D2ab.zeros();
  double x,y;

  for(int i=0;i<nels;i++)
  {
    D1(i,i)=1.0;
    for(int j=0;j<nels;j++)
    {
      D1(i,j) += dij(i,j,T1,T2aa,T2ab);
    }
  }

  for(int a=0;a<virt;a++)
  {
    for(int b=0;b<virt;b++)
    {
      D1(a+nels,b+nels) += dab(a,b,T1,T2aa,T2ab);
    }
  }
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      x = dia(i,a,T1,T2aa,T2ab,F1m);
      D1(i,a+nels) += x;
      D1(a+nels,i) += x;
    }
  }

  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      D2ab(m*nact+n,m*nact+n) += 1.0;
      for(int o=0;o<nels;o++)
      {
	x=dij(n,o,T1,T2aa,T2ab);
	D2ab(m*nact+n,m*nact+o) += x;
	D2ab(n*nact+m,o*nact+m) += x;
	for(int p=0;p<nels;p++)
	{
	  x=0.0;
	  for(int a=0;a<virt;a++)
	  {
	    for(int b=0;b<virt;b++)
	    {
	      x += T2ab(m*nels+n,a*virt+b)*T2ab(o*nels+p,a*virt+b);
	    }
	  }
	  D2ab(m*nact+n,o*nact+p) += x;
	}
	
      }
    }
  }
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      D2aa(indexD(m,n,nact),indexD(m,n,nact)) += 1.0;
      for(int o=0;o<m;o++)
      {
	D2aa(indexD(m,n,nact),indexD(o,m,nact)) -= dij(n,o,T1,T2aa,T2ab);
      }
      for(int o=m+1;o<nels;o++)
      {
	D2aa(indexD(m,n,nact),indexD(m,o,nact)) += dij(n,o,T1,T2aa,T2ab);
      }
      for(int o=0;o<n;o++)
      {
	D2aa(indexD(m,n,nact),indexD(o,n,nact)) += dij(m,o,T1,T2aa,T2ab);
      }
      for(int o=n+1;o<nels;o++)
      {
	D2aa(indexD(m,n,nact),indexD(n,o,nact)) -= dij(m,o,T1,T2aa,T2ab);
      }
      for(int o=0;o<nels;o++)
      {
	for(int p=o+1;p<nels;p++)
	{
	  x = 0.0;
	  for(int a=0;a<virt;a++)
	  {
	    for(int b=a+1;b<virt;b++)
	    {
	      x += T2aa(indexD(o,p,nels),indexD(a,b,virt))*T2aa(indexD(m,n,nels),indexD(a,b,virt));
	    }
	  }
	  D2aa(indexD(m,n,nact),indexD(o,p,nact)) += x;
	}
      }
    }
  }

  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      for(int e=0;e<virt;e++)
      {
	x = dia(n,e,T1,T2aa,T2ab,F1m);
	D2ab(m*nact+n,m*nact+e+nels) += x;
	D2ab(m*nact+e+nels,m*nact+n) += x;
	D2ab(n*nact+m,(e+nels)*nact+m) += x;
	D2ab((e+nels)*nact+m,n*nact+m) += x;
	for(int o=0;o<nels;o++)
	{
	  y = 0.0;
	  for(int a=0;a<virt;a++)
	  {
	    y += T1(o,a)*T2ab(m*nels+n,a*virt+e);
	  }
	  D2ab(m*nact+n,o*nact+e+nels) -= y;
	  D2ab(o*nact+e+nels,m*nact+n) -= y;
	  D2ab(n*nact+m,(e+nels)*nact+o) -= y;
	  D2ab((e+nels)*nact+o,n*nact+m) -= y;
	}
      }
    }
  }

  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      for(int e=0;e<virt;e++)
      {
	x = dia(n,e,T1,T2aa,T2ab,F1m);
	D2aa(indexD(m,n,nact),indexD(m,e+nels,nact)) += x;
	D2aa(indexD(m,e+nels,nact),indexD(m,n,nact)) += x;
	x = dia(m,e,T1,T2aa,T2ab,F1m);
	D2aa(indexD(m,n,nact),indexD(n,e+nels,nact)) -= x;
	D2aa(indexD(n,e+nels,nact),indexD(m,n,nact)) -= x;
	for(int o=0;o<nels;o++)
	{
	  x = 0.0;
	  for(int a=0;a<e;a++)
	  {
	    x -= T1(o,a)*T2aa(indexD(m,n,nels),indexD(a,e,virt));
	  }
	  for(int a=e+1;a<virt;a++)
	  {
	    x += T1(o,a)*T2aa(indexD(m,n,nels),indexD(e,a,virt));
	  }
	  D2aa(indexD(m,n,nact),indexD(o,e+nels,nact)) += x;
	  D2aa(indexD(o,e+nels,nact),indexD(m,n,nact)) += x;
	}
      }
    }
  }


  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      for(int f=0;f<virt;f++)
      {
	x = 0.0;
	x = dab(e,f,T1,T2aa,T2ab);
	D2ab(m*nact+e+nels,m*nact+f+nels) += x;
	D2ab((e+nels)*nact+m,(f+nels)*nact+m) += x;
	for(int n=0;n<nels;n++)
	{
	  x = T1(n,e)*T1(m,f);
	  D2ab(m*nact+e+nels,(f+nels)*nact+n) += x;
	  D2ab((f+nels)*nact+n,m*nact+e+nels) += x;
	  x=0.0;
	  for(int a=0;a<virt;a++)
	  {
	    for(int j=0;j<nels;j++)
	    {
	      x += T2ab(n*nels+j,a*virt+e)*T2ab(m*nels+j,a*virt+f);
	    }
	  }
	  D2ab(m*nact+e+nels,n*nact+f+nels) -= x;
	  D2ab((e+nels)*nact+m,(f+nels)*nact+n) -= x;
	  x = 0.0;
	  for(int a=f+1;a<virt;a++)
	  {
	    for(int i=m+1;i<nels;i++)
	    {
	      x += T2aa(indexD(m,i,nels),indexD(f,a,virt))*T2ab(i*nels+n,a*virt+e);
	    }
	    for(int i=0;i<m;i++)
	    {
	      x -= T2aa(indexD(i,m,nels),indexD(f,a,virt))*T2ab(i*nels+n,a*virt+e);
	    }
	  }
	  for(int a=0;a<f;a++)
	  {
	    for(int i=m+1;i<nels;i++)
	    {
	      x -= T2aa(indexD(m,i,nels),indexD(a,f,virt))*T2ab(i*nels+n,a*virt+e);
	    }
	    for(int i=0;i<m;i++)
	    {
	      x += T2aa(indexD(i,m,nels),indexD(a,f,virt))*T2ab(i*nels+n,a*virt+e);
	    }
	  }
	  D2ab(m*nact+e+nels,(f+nels)*nact+n) += x;
	  D2ab((e+nels)*nact+m,n*nact+(f+nels)) += x;
	  x = 0.0;
	  for(int a=0;a<e;a++)
	  {
	    for(int i=0;i<n;i++)
	    {
	      x += T2ab(m*nels+i,f*virt+a)*T2aa(indexD(i,n,nels),indexD(a,e,virt));
	    }
	    for(int i=n+1;i<nels;i++)
	    {
	      x -= T2ab(m*nels+i,f*virt+a)*T2aa(indexD(n,i,nels),indexD(a,e,virt));
	    }
	  }
	  for(int a=e+1;a<virt;a++)
	  {
	    for(int i=0;i<n;i++)
	    {
	      x -= T2ab(m*nels+i,f*virt+a)*T2aa(indexD(i,n,nels),indexD(e,a,virt));
	    }
	    for(int i=n+1;i<nels;i++)
	    {
	      x += T2ab(m*nels+i,f*virt+a)*T2aa(indexD(n,i,nels),indexD(e,a,virt));
	    }
	  }
	  D2ab(m*nact+e+nels,(f+nels)*nact+n) += x;
	  D2ab((e+nels)*nact+m,n*nact+(f+nels)) += x;
	}
      }
    }
  }


  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      for(int f=0;f<virt;f++)
      {
	D2aa(indexD(m,e+nels,nact),indexD(m,f+nels,nact)) += dab(e,f,T1,T2aa,T2ab);
	for(int n=0;n<nels;n++)
	{
	  D2aa(indexD(m,e+nels,nact),indexD(n,f+nels,nact)) -= T1(n,e)*T1(m,f);
	  x=0.0;
	  for(int j=0;j<nels;j++)
	  {
	    for(int b=0;b<virt;b++)
	    {
	      x += T2ab(n*nels+j,e*virt+b)*T2ab(m*nels+j,f*virt+b);
	    }
	  }
	  D2aa(indexD(m,e+nels,nact),indexD(n,f+nels,nact)) -= x;
	  x = 0.0;
	  for(int i=0;i<std::min(m,n);i++)
	  {
	    for(int a=0;a<std::min(e,f);a++)
	    {
	      x += T2aa(indexD(i,n,nels),indexD(a,e,virt))*T2aa(indexD(i,m,nels),indexD(a,f,virt));
	    }
	    for(int a=e+1;a<f;a++)
	    {
	      x -= T2aa(indexD(i,n,nels),indexD(e,a,virt))*T2aa(indexD(i,m,nels),indexD(a,f,virt));
	    }
	    for(int a=f+1;a<e;a++)
	    {
	      x -= T2aa(indexD(i,n,nels),indexD(a,e,virt))*T2aa(indexD(i,m,nels),indexD(f,a,virt));
	    }
	    for(int a=std::max(e,f)+1;a<virt;a++)
	    {
	      x += T2aa(indexD(i,n,nels),indexD(e,a,virt))*T2aa(indexD(i,m,nels),indexD(f,a,virt));
	    }
	  }
	  D2aa(indexD(m,e+nels,nact),indexD(n,f+nels,nact)) -= x;
	  x=0.0;
	  for(int i=m+1;i<n;i++)
	  {
	    for(int a=0;a<std::min(e,f);a++)
	    {
	      x -= T2aa(indexD(i,n,nels),indexD(a,e,virt))*T2aa(indexD(m,i,nels),indexD(a,f,virt));
	    }
	    for(int a=e+1;a<f;a++)
	    {
	      x += T2aa(indexD(i,n,nels),indexD(e,a,virt))*T2aa(indexD(m,i,nels),indexD(a,f,virt));
	    }
	    for(int a=f+1;a<e;a++)
	    {
	      x += T2aa(indexD(i,n,nels),indexD(a,e,virt))*T2aa(indexD(m,i,nels),indexD(f,a,virt));
	    }
	    for(int a=std::max(e,f)+1;a<virt;a++)
	    {
	      x -= T2aa(indexD(i,n,nels),indexD(e,a,virt))*T2aa(indexD(m,i,nels),indexD(f,a,virt));
	    }
	  }
	  D2aa(indexD(m,e+nels,nact),indexD(n,f+nels,nact)) -= x;
	  x=0.0;
	  for(int i=n+1;i<m;i++)
	  {
	    for(int a=0;a<std::min(e,f);a++)
	    {
	      x -= T2aa(indexD(n,i,nels),indexD(a,e,virt))*T2aa(indexD(i,m,nels),indexD(a,f,virt));
	    }
	    for(int a=e+1;a<f;a++)
	    {
	      x += T2aa(indexD(n,i,nels),indexD(e,a,virt))*T2aa(indexD(i,m,nels),indexD(a,f,virt));
	    }
	    for(int a=f+1;a<e;a++)
	    {
	      x += T2aa(indexD(n,i,nels),indexD(a,e,virt))*T2aa(indexD(i,m,nels),indexD(f,a,virt));
	    }
	    for(int a=std::max(e,f)+1;a<virt;a++)
	    {
	      x -= T2aa(indexD(n,i,nels),indexD(e,a,virt))*T2aa(indexD(i,m,nels),indexD(f,a,virt));
	    }
	  }
	  D2aa(indexD(m,e+nels,nact),indexD(n,f+nels,nact)) -= x;
	  x=0.0;
	  for(int i=std::max(m,n)+1;i<nels;i++)
	  {
	    for(int a=0;a<std::min(e,f);a++)
	    {
	      x += T2aa(indexD(n,i,nels),indexD(a,e,virt))*T2aa(indexD(m,i,nels),indexD(a,f,virt));
	    }
	    for(int a=e+1;a<f;a++)
	    {
	      x -= T2aa(indexD(n,i,nels),indexD(e,a,virt))*T2aa(indexD(m,i,nels),indexD(a,f,virt));
	    }
	    for(int a=f+1;a<e;a++)
	    {
	      x -= T2aa(indexD(n,i,nels),indexD(a,e,virt))*T2aa(indexD(m,i,nels),indexD(f,a,virt));
	    }
	    for(int a=std::max(e,f)+1;a<virt;a++)
	    {
	      x += T2aa(indexD(n,i,nels),indexD(e,a,virt))*T2aa(indexD(m,i,nels),indexD(f,a,virt));
	    }
	  }
	  D2aa(indexD(m,e+nels,nact),indexD(n,f+nels,nact)) -= x;
	}
      }
    }
  }


  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      for(int f=0;f<virt;f++)
      {
	for(int g=0;g<virt;g++)
	{
	  x = 0.0;
	  for(int i=0;i<nels;i++)
	  {
	    x += T1(i,e)*T2ab(m*nels+i,f*virt+g);
	  }
	  D2ab(m*nact+e+nels,(f+nels)*nact+g+nels) += x;
	  D2ab((f+nels)*nact+g+nels,m*nact+e+nels) += x;
	  D2ab((e+nels)*nact+m,(g+nels)*nact+f+nels) += x;
	  D2ab((g+nels)*nact+f+nels,(e+nels)*nact+m) += x;
	}
      }
    }
  }

  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      for(int f=0;f<virt;f++)
      {
	for(int g=f+1;g<virt;g++)
	{
	  x = 0.0;
	  for(int i=0;i<m;i++)
	  {
	    x -= T1(i,e)*T2aa(indexD(i,m,nels),indexD(f,g,virt));
	  }
	  for(int i=m+1;i<nels;i++)
	  {
	    x += T1(i,e)*T2aa(indexD(m,i,nels),indexD(f,g,virt));
	  }
	  D2aa(indexD(m,e+nels,nact),indexD(f+nels,g+nels,nact)) += x;
	  D2aa(indexD(f+nels,g+nels,nact),indexD(m,e+nels,nact)) += x;
	}
      }
    }
  }


  for(int e=0;e<virt;e++)
  {
    for(int f=0;f<virt;f++)
    {
      for(int g=0;g<virt;g++)
      {
	for(int h=0;h<virt;h++)
	{
	  x = 0.0;
	  for(int i=0;i<nels;i++)
	  {
	    for(int j=0;j<nels;j++)
	    {
	      x += T2ab(i*nels+j,e*virt+f)*T2ab(i*nels+j,g*virt+h);
	    }
	  }
	  D2ab((e+nels)*nact+f+nels,(g+nels)*nact+h+nels) += x;
	}
      }
    }
  }

  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      for(int g=0;g<virt;g++)
      {
	for(int h=g+1;h<virt;h++)
	{
	  x=0.0;
	  for(int i=0;i<nels;i++)
	  {
	    for(int j=i+1;j<nels;j++)
	    {
	      x += T2aa(indexD(i,j,nels),indexD(e,f,virt))*T2aa(indexD(i,j,nels),indexD(g,h,virt));
	    }
	  }
	  D2aa(indexD(e+nels,f+nels,nact),indexD(g+nels,h+nels,nact)) += x;
	}
      }
    }
  }

  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      for(int e=0;e<virt;e++)
      {
	for(int f=0;f<virt;f++)
	{
	  x = T2ab(m*nels+n,e*virt+f)*pow(F2abM(m*nels+n,e*virt+f),0.5);
	  D2ab((e+nels)*nact+f+nels,m*nact+n) += x;
	  D2ab(m*nact+n,(e+nels)*nact+f+nels) += x;
	}
      }
    }
  }
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      for(int e=0;e<virt;e++)
      {
	for(int f=e+1;f<virt;f++)
	{
	  x=T2aa(indexD(m,n,nels),indexD(e,f,virt))*pow(F2aaM(indexD(m,n,nels),indexD(e,f,virt)),0.5);
	  D2aa(indexD(e+nels,f+nels,nact),indexD(m,n,nact)) += x;
	  D2aa(indexD(m,n,nact),indexD(e+nels,f+nels,nact)) += x;
	}
      }
    }
  }

  lastTime = time(NULL);

  tim += lastTime-startTime;
  return;
}


void printIts(const real_1d_array &x,double E,void *ptr)
{
  intBundle *iPtr=static_cast<intBundle*>(ptr);
  if((*iPtr).print)
  {
    std::cout << (*iPtr).numIt << '\t' << (*iPtr).Enuc+(*iPtr).E << std::endl;
  }
  (*iPtr).numIt++;

  return;
}


void genInts(double &Enuc,arma::mat &S,arma::mat &K1ao,arma::mat &V2aaAO,arma::mat &V2abAO,std::vector<libint2::Atom> atoms,std::string basis)
{
  double r;
  Enuc=0.0;
  for(int i=0;i<atoms.size();i++)
  {
    for(int j=i+1;j<atoms.size();j++)
    {
      r=(atoms[i].x-atoms[j].x)*(atoms[i].x-atoms[j].x);
      r+=(atoms[i].y-atoms[j].y)*(atoms[i].y-atoms[j].y);
      r+=(atoms[i].z-atoms[j].z)*(atoms[i].z-atoms[j].z);
      Enuc += (double) atoms[i].atomic_number*atoms[j].atomic_number/sqrt(r);
    }
  }

  int nAO=K1ao.n_rows;
  libint2::BasisSet obs(basis,atoms);

  libint2::init();

  libint2::OneBodyEngine engineS(libint2::OneBodyEngine::overlap,obs.max_nprim(),obs.max_l(),0);

  auto shell2bf = obs.shell2bf();

  for(int s1=0;s1<obs.size();s1++)
  {
    for(int s2=0;s2<=s1;s2++)
    {
      const auto* ints_shellset = engineS.compute(obs[s1],obs[s2]);

      int bf1 = shell2bf[s1];
      int n1 = obs[s1].size();
      int bf2 = shell2bf[s2];
      int n2 = obs[s2].size();

      for(int f1=0;f1<n1;f1++)
      {
	for(int f2=0;f2<n2;f2++)
	{
	  S(bf1+f1,bf2+f2)=ints_shellset[f1*n2+f2];
	  S(bf2+f2,bf1+f1)=S(bf1+f1,bf2+f2);
	}
      }
    }
  }

  libint2::OneBodyEngine engineK(libint2::OneBodyEngine::kinetic,obs.max_nprim(),obs.max_l(),0);

  for(int s1=0;s1<obs.size();s1++)
  {
    for(int s2=0;s2<=s1;s2++)
    {
      const auto* ints_shellset = engineK.compute(obs[s1],obs[s2]);

      int bf1 = shell2bf[s1];
      int n1 = obs[s1].size();
      int bf2 = shell2bf[s2];
      int n2 = obs[s2].size();

      for(int f1=0;f1<n1;f1++)
      {
	for(int f2=0;f2<n2;f2++)
	{
	  K1ao(bf1+f1,bf2+f2)=ints_shellset[f1*n2+f2];
	}
      }
    }
  }

  libint2::OneBodyEngine engineN(libint2::OneBodyEngine::nuclear,obs.max_nprim(),obs.max_l(),0);
  std::vector<std::pair<double,std::array<double,3>>> q;
  for(int a=0;a<atoms.size();a++) 
  {
    q.push_back( {static_cast<double>(atoms[a].atomic_number), {{atoms[a].x, atoms[a].y, atoms[a].z}}} );
  }
  engineN.set_params(q);

 
  for(int s1=0;s1<obs.size();s1++)
  {
    for(int s2=0;s2<=s1;s2++)
    {
      const auto* ints_shellset = engineN.compute(obs[s1],obs[s2]);

      int bf1 = shell2bf[s1];
      int n1 = obs[s1].size();
      int bf2 = shell2bf[s2];
      int n2 = obs[s2].size();

      for(int f1=0;f1<n1;f1++)
      {
	for(int f2=0;f2<n2;f2++)
	{
	  K1ao(bf1+f1,bf2+f2)+=ints_shellset[f1*n2+f2];
	  K1ao(bf2+f2,bf1+f1)=K1ao(bf1+f1,bf2+f2);
	}
      }
    }
  }

  libint2::TwoBodyEngine<libint2::Coulomb> engineV(obs.max_nprim(),obs.max_l(), 0);

  // loop over shell pairs of the Fock matrix, {s1,s2}
  // Fock matrix is symmetric, but skipping it here for simplicity (see compute_2body_fock)
  int num;
  for(int s1=0; s1<obs.size(); s1++)
   {

    int bf1 = shell2bf[s1]; // first basis function in this shell
    int n1 = obs[s1].size();

    for(int s2=0; s2<obs.size(); s2++)
    {

      int bf2 = shell2bf[s2];
      int n2 = obs[s2].size();

      // loop over shell pairs of the density matrix, {s3,s4}
      // again symmetry is not used for simplicity
      for(int s3=0; s3<obs.size(); s3++)
      {

	int bf3 = shell2bf[s3];
	int n3 = obs[s3].size();

	for(int s4=0; s4<obs.size(); s4++)
	{

	  int bf4 = shell2bf[s4];
	  int n4 = obs[s4].size();

	  const auto* ints_shellset = engineV.compute(obs[s1],obs[s2],obs[s3],obs[s4]);

	  num=0;
	  for(int f1=0;f1<n1;f1++)
	  {
	    for(int f2=0;f2<n2;f2++)
	    {
	      for(int f3=0;f3<n3;f3++)
	      {
		for(int f4=0;f4<n4;f4++)
		{
		  V2abAO((bf1+f1)*nAO+bf3+f3,(bf2+f2)*nAO+bf4+f4)=ints_shellset[num];
		  num++;
		}
	      }
	    }
	  }
        }
      }
    }
  }
  for(int i=0;i<nAO;i++)
  {
    for(int j=i+1;j<nAO;j++)
    {
      for(int k=0;k<nAO;k++)
      {
	for(int l=k+1;l<nAO;l++)
	{
	  V2aaAO(indexD(i,j,nAO),indexD(k,l,nAO))=2.0*(V2abAO(i*nAO+j,k*nAO+l)-V2abAO(i*nAO+j,l*nAO+k));
	}
      }
    }
  }

  return;
}

void evalCI(const real_1d_array &T,double &E,real_1d_array &grad,void *ptr)
{
  time_t startTime,lastTime,tim;
  double x,Ep,Em;

  intBundle *iPtr=static_cast<intBundle*>(ptr);

  int nels=(*iPtr).nels;  
  int virt=(*iPtr).virt;

  arma::mat T1(nels,virt);
  arma::mat T2aa(nels*(nels-1)/2,virt*(virt-1)/2);
  arma::mat T2ab(nels*nels,virt*virt);  

  arma::mat F1m(nels,virt,arma::fill::zeros);
  arma::mat F2aaM(nels*(nels-1)/2,virt*(virt-1)/2,arma::fill::zeros);
  arma::mat F2abM(nels*nels,virt*virt,arma::fill::zeros);  

  int nact=nels+virt;

  arma::mat D1(nact,nact,arma::fill::zeros);
  arma::mat D2aa(nact*(nact-1)/2,nact*(nact-1)/2,arma::fill::zeros);
  arma::mat D2ab(nact*nact,nact*nact,arma::fill::zeros);  

  int n1=nels*virt;
  int n2aa=T2aa.n_rows*T2aa.n_cols;

  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      T1(i,a)=T[i*virt+a];
      for(int j=i+1;j<nels;j++)
      {
	for(int b=a+1;b<virt;b++)
	{
	  T2aa(indexD(i,j,nels),indexD(a,b,virt))=T[n1+indexD(i,j,nels)*T2aa.n_cols+indexD(a,b,virt)];
	}
      }
      for(int j=0;j<nels;j++)
      {
	for(int b=0;b<virt;b++)
	{
	  T2ab(i*nels+j,a*virt+b)=T[n1+n2aa+(i*nels+j)*T2ab.n_cols+(a*virt+b)];
	}
      }	
    }
  }

  arma::mat K1=(*iPtr).K1;
  arma::mat V2aa=(*iPtr).V2aa;
  arma::mat V2ab=(*iPtr).V2ab;

  calcFci(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
  makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr);
  E = 2.0*trace( (*iPtr).K1*D1)+trace( (*iPtr).V2aa*D2aa)+trace( (*iPtr).V2ab*D2ab); 
  (*iPtr).E=E;
  std::cout << "\tE:\t" << E << std::endl;

  double dx=0.00001;
  double max=0.0;
    for(int i=0;i<nels;i++)
    {
      for(int a=0;a<virt;a++)
      {
	T2ab(i*nels+i,a*virt+a) += dx;
	calcFci(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr);
	Ep=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	T2ab(i*nels+i,a*virt+a) -= 2.0*dx;
	calcFci(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr);
	Em=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	x=(Ep-Em)/2.0/dx;
        grad[n1+n2aa+(i*nels+i)*T2ab.n_cols+(a*virt+a)]=x;
  //      grad[n1+n2aa+(i*nels+i)*T2ab.n_cols+(a*virt+a)]=0.5*x;
	if(fabs(x) > max)
	{
	  max = x;
	}
	T2ab(i*nels+i,a*virt+a) += dx;
	for(int j=0;j<nels;j++)
	{
	  for(int b=0;b<virt && j*nels+b < i*nels+a;b++)
	  {
	    T2ab(i*nels+j,a*virt+b) += dx;
	    T2ab(j*nels+i,b*virt+a) += dx;
	    calcFci(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	    makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr);
	    Ep=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	    T2ab(i*nels+j,a*virt+b) -= 2.0*dx;
	    T2ab(j*nels+i,b*virt+a) -= 2.0*dx;
	    calcFci(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	    makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr);
	    Em=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	    x=(Ep-Em)/2.0/dx;
//	    grad[n1+n2aa+(i*nels+j)*T2ab.n_cols+(a*virt+b)]=x;
//	    grad[n1+n2aa+(j*nels+i)*T2ab.n_cols+(b*virt+a)]=x;
	    grad[n1+n2aa+(i*nels+j)*T2ab.n_cols+(a*virt+b)]=0.5*x;
	    grad[n1+n2aa+(j*nels+i)*T2ab.n_cols+(b*virt+a)]=0.5*x;
	    T2ab(i*nels+j,a*virt+b) += dx;
	    T2ab(j*nels+i,b*virt+a) += dx;
	if(fabs(x) > max)
	{
	  max = x;
	}
	  }
	}
      }
    }
    std::cout << "\tmax dT2ab:\t" << max << std::endl; max=0.0;
    for(int i=0;i<nels;i++)
    {
      for(int j=i+1;j<nels;j++)
      {
	for(int a=0;a<virt;a++)
	{
	  for(int b=a+1;b<virt;b++)
	  {
	    T2aa(indexD(i,j,nels),indexD(a,b,virt)) += dx;
	    calcFci(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	    makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr);
	    Ep=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	    T2aa(indexD(i,j,nels),indexD(a,b,virt)) -= 2.0*dx;
	    calcFci(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	    makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr);
	    Em=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	    x=(Ep-Em)/2.0/dx;
	    grad[n1+indexD(i,j,nels)*T2aa.n_cols+indexD(a,b,virt)]=x;
//	    grad[n1+indexD(i,j,nels)*T2aa.n_cols+indexD(a,b,virt)]=0.5*x;
	    T2aa(indexD(i,j,nels),indexD(a,b,virt)) += dx;
	if(fabs(x) > max)
	{
	  max = x;
	}
	  }
	}
      }
    }
    std::cout << "\tmax dT2aa:\t" << max << std::endl; max=0.0;
    for(int i=0;i<nels;i++)
    {
      for(int a=0;a<virt;a++)
      {
	T1(i,a) += dx;
	calcFci(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr);
	Ep=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	T1(i,a) -= 2.0*dx;
	calcFci(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr);
	Em=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	x=(Ep-Em)/2.0/dx;
//        grad[i*virt+a]=0.5*x;
        grad[i*virt+a]=x;
	T1(i,a) += dx;
	if(fabs(x) > max)
	{
	  max = x;
	}
      }
    }
    std::cout << "\tmax dT1ia:\t" << max << std::endl << std::endl;

  return;
}

void evalNum(const real_1d_array &T,double &E,real_1d_array &grad,void *ptr)
{
  time_t startTime,lastTime,tim;
  double x,Ep,Em;

  intBundle *iPtr=static_cast<intBundle*>(ptr);

  int nels=(*iPtr).nels;  
  int virt=(*iPtr).virt;

  arma::mat T1(nels,virt);
  arma::mat T2aa(nels*(nels-1)/2,virt*(virt-1)/2);
  arma::mat T2ab(nels*nels,virt*virt);  

  arma::mat F1m(nels,virt,arma::fill::zeros);
  arma::mat F2aaM(nels*(nels-1)/2,virt*(virt-1)/2,arma::fill::zeros);
  arma::mat F2abM(nels*nels,virt*virt,arma::fill::zeros);  

  int nact=nels+virt;

  arma::mat D1(nact,nact,arma::fill::zeros);
  arma::mat D2aa(nact*(nact-1)/2,nact*(nact-1)/2,arma::fill::zeros);
  arma::mat D2ab(nact*nact,nact*nact,arma::fill::zeros);  

  int n1=nels*virt;
  int n2aa=T2aa.n_rows*T2aa.n_cols;

  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      T1(i,a)=T[i*virt+a];
      for(int j=i+1;j<nels;j++)
      {
	for(int b=a+1;b<virt;b++)
	{
	  T2aa(indexD(i,j,nels),indexD(a,b,virt))=T[n1+indexD(i,j,nels)*T2aa.n_cols+indexD(a,b,virt)];
	}
      }
      for(int j=0;j<nels;j++)
      {
	for(int b=0;b<virt;b++)
	{
	  T2ab(i*nels+j,a*virt+b)=T[n1+n2aa+(i*nels+j)*T2ab.n_cols+(a*virt+b)];
	}
      }	
    }
  }

  arma::mat K1=(*iPtr).K1;
  arma::mat V2aa=(*iPtr).V2aa;
  arma::mat V2ab=(*iPtr).V2ab;

  //calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
  makeDnew(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr,grad);
  E = 2.0*trace( (*iPtr).K1*D1)+trace( (*iPtr).V2aa*D2aa)+trace( (*iPtr).V2ab*D2ab); 
  (*iPtr).E=E;
  //std::cout << "\tE:\t" << E << std::endl;

  double dx=0.00001;
  double max=0.0;
    for(int i=0;i<nels;i++)
    {
      for(int a=0;a<virt;a++)
      {
	T2ab(i*nels+i,a*virt+a) += dx;
	//calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	makeDnew(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr,grad);
	Ep=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	T2ab(i*nels+i,a*virt+a) -= 2.0*dx;
	//calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	makeDnew(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr,grad);
	Em=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	x=(Ep-Em)/2.0/dx;
        grad[n1+n2aa+(i*nels+i)*T2ab.n_cols+(a*virt+a)]=x;
  //      grad[n1+n2aa+(i*nels+i)*T2ab.n_cols+(a*virt+a)]=0.5*x;
	if(fabs(x) > max)
	{
	  max = x;
	}
	T2ab(i*nels+i,a*virt+a) += dx;
	for(int j=0;j<nels;j++)
	{
	  for(int b=0;b<virt && j*nels+b < i*nels+a;b++)
	  {
	    T2ab(i*nels+j,a*virt+b) += dx;
	    T2ab(j*nels+i,b*virt+a) += dx;
	    //calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	    makeDnew(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr,grad);
	    Ep=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	    T2ab(i*nels+j,a*virt+b) -= 2.0*dx;
	    T2ab(j*nels+i,b*virt+a) -= 2.0*dx;
	    //calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	    makeDnew(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr,grad);
	    Em=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	    x=(Ep-Em)/2.0/dx;
//	    grad[n1+n2aa+(i*nels+j)*T2ab.n_cols+(a*virt+b)]=x;
//	    grad[n1+n2aa+(j*nels+i)*T2ab.n_cols+(b*virt+a)]=x;
	    grad[n1+n2aa+(i*nels+j)*T2ab.n_cols+(a*virt+b)]=0.5*x;
	    grad[n1+n2aa+(j*nels+i)*T2ab.n_cols+(b*virt+a)]=0.5*x;
	    T2ab(i*nels+j,a*virt+b) += dx;
	    T2ab(j*nels+i,b*virt+a) += dx;
	if(fabs(x) > max)
	{
	  max = x;
	}
	  }
	}
      }
    }
    //std::cout << "\tmax dT2ab:\t" << max << std::endl; max=0.0;
    for(int i=0;i<nels;i++)
    {
      for(int j=i+1;j<nels;j++)
      {
	for(int a=0;a<virt;a++)
	{
	  for(int b=a+1;b<virt;b++)
	  {
	    T2aa(indexD(i,j,nels),indexD(a,b,virt)) += dx;
	    //calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	    makeDnew(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr,grad);
	    Ep=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	    T2aa(indexD(i,j,nels),indexD(a,b,virt)) -= 2.0*dx;
	    //calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	    makeDnew(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr,grad);
	    Em=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	    x=(Ep-Em)/2.0/dx;
	    grad[n1+indexD(i,j,nels)*T2aa.n_cols+indexD(a,b,virt)]=x;
//	    grad[n1+indexD(i,j,nels)*T2aa.n_cols+indexD(a,b,virt)]=0.5*x;
	    T2aa(indexD(i,j,nels),indexD(a,b,virt)) += dx;
	if(fabs(x) > max)
	{
	  max = x;
	}
	  }
	}
      }
    }
    //std::cout << "\tmax dT2aa:\t" << max << std::endl; max=0.0;
    for(int i=0;i<nels;i++)
    {
      for(int a=0;a<virt;a++)
      {
	T1(i,a) += dx;
	//calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	makeDnew(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr,grad);
	Ep=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	T1(i,a) -= 2.0*dx;
	//calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
	makeDnew(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr,grad);
	Em=2.0*trace(K1*D1)+trace(V2aa*D2aa)+trace(V2ab*D2ab);
	x=(Ep-Em)/2.0/dx;
//        grad[i*virt+a]=0.5*x;
        grad[i*virt+a]=x;
	T1(i,a) += dx;
	if(fabs(x) > max)
	{
	  max = x;
	}
      }
    }
   // std::cout << "\tmax dT1ia:\t" << max << std::endl << std::endl;

  return;
}

void calcF(arma::mat &F1m,arma::mat &F2aaM,arma::mat &F2abM,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,time_t &tim)
{
  int nels=T1.n_rows;
  int virt=T1.n_cols;

  time_t startTime,lastTime;
  startTime=time(NULL);

  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      F1m(i,a)=F1(i,a,T1,T2aa,T2ab);
      for(int j=0;j<nels;j++)
      {
	for(int b=0;b<virt;b++)
	{
	  F2abM(i*nels+j,a*virt+b)=F2ab(i,j,a,b,T1,T2aa,T2ab);
	}
      }
      for(int j=i+1;j<nels;j++)
      {
	for(int b=a+1;b<virt;b++)
	{
	  F2aaM(indexD(i,j,nels),indexD(a,b,virt))=F2aa(i,j,a,b,T1,T2aa,T2ab);
	}
      }
    }
  }

  lastTime=time(NULL);
  tim += lastTime-startTime;
  return;
}

void gradEval(const real_1d_array &T,double &E,real_1d_array &grad,void *ptr)
{
  time_t startTime,lastTime;

  intBundle *iPtr=static_cast<intBundle*>(ptr);

  int nels=(*iPtr).nels;  
  int virt=(*iPtr).virt;

  arma::mat T1(nels,virt,arma::fill::zeros);
  arma::mat T2aa(nels*(nels-1)/2,virt*(virt-1)/2,arma::fill::zeros);
  arma::mat T2ab(nels*nels,virt*virt,arma::fill::zeros);  

  arma::mat F1m(nels,virt,arma::fill::zeros);
  arma::mat F2aaM(nels*(nels-1)/2,virt*(virt-1)/2,arma::fill::zeros);
  arma::mat F2abM(nels*nels,virt*virt,arma::fill::zeros);  

  int nact=nels+virt;

  arma::mat D1(nact,nact,arma::fill::zeros);
  arma::mat D2aa(nact*(nact-1)/2,nact*(nact-1)/2,arma::fill::zeros);
  arma::mat D2ab(nact*nact,nact*nact,arma::fill::zeros);  

  int n1=nels*virt;
  int n2aa=T2aa.n_rows*T2aa.n_cols;

  for(int a=0;a<virt;a++)
  {
    for(int i=0;i<nels;i++)
    {
      T1(i,a)=T[a*nels+i];
    }
  }
  for(int a=0;a<virt;a++)
  {
    for(int b=a+1;b<virt;b++)
    {
      for(int i=0;i<nels;i++)
      {
	for(int j=i+1;j<nels;j++)
	{
	  T2aa(indexD(i,j,nels),indexD(a,b,virt))=T[n1+indexD(a,b,virt)*T2aa.n_rows+indexD(i,j,nels)];
	}
      }
    }
  }
  for(int a=0;a<virt;a++)
  {
    for(int b=0;b<virt;b++)
    {
      for(int i=0;i<nels;i++)
      {
	for(int j=0;j<nels;j++)
	{
	  T2ab(i*nels+j,a*virt+b)=T[n1+n2aa+(a*virt+b)*T2ab.n_rows+(i*nels+j)];
	}
      }
    }
  }

  for(int i=0;i<nels*virt+nels*(nels-1)/2*virt*(virt-1)/2+nels*nels*virt*virt;i++)
  {
    grad[i]=0.0;
  }

  /*startTime = time(NULL);
  calcF(F1m,F2aaM,F2abM,T1,T2aa,T2ab,(*iPtr).timF);
  lastTime=time(NULL);
  (*iPtr).timF += lastTime-startTime;

  startTime = time(NULL);
  makeD2p(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr);
  E = 2.0*trace( (*iPtr).K1*D1)+trace( (*iPtr).V2aa*D2aa)+trace( (*iPtr).V2ab*D2ab); 
  (*iPtr).E=E;
  lastTime=time(NULL);
  (*iPtr).timE += lastTime-startTime;*/
  makeDnew(D1,D2aa,D2ab,T1,T2aa,T2ab,F1m,F2aaM,F2abM,(*iPtr).timE,ptr,grad);
  E = 2.0*trace( (*iPtr).K1*D1)+trace( (*iPtr).V2aa*D2aa)+trace( (*iPtr).V2ab*D2ab); 
  (*iPtr).E=E;

/*  startTime = time(NULL);
  double x;
  double maxT1=0.0;
  double maxT2aa=0.0;
  double maxT2ab=0.0;
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      x=dE1dT1(i,a,(*iPtr).K1,T1,T2aa,T2ab,F1m);
      x+=dE2ab_dT1(i,a,(*iPtr).V2ab,T1,T2aa,T2ab,F1m,F2abM);
      x+=dE2aa_dT1(i,a,(*iPtr).V2aa,T1,T2aa,T2ab,F1m,F2aaM);
      grad[i*virt+a]=0.0;
      grad[i*virt+a]+=x;
	if(fabs(x) > maxT1)
	{
	  maxT1 = x;
	}

      x=dE1dT2ab(i,i,a,a,(*iPtr).K1,T1,T2aa,T2ab,F1m);
      x+=dE2ab_dT2ab(i,i,a,a,(*iPtr).V2ab,T1,T2aa,T2ab,F1m,F2abM);
      x+=dE2aa_dT2ab(i,i,a,a,(*iPtr).V2aa,T1,T2aa,T2ab,F1m,F2aaM);
      grad[n1+n2aa+(i*nels+i)*T2ab.n_cols+(a*virt+a)]+=0.0;
      grad[n1+n2aa+(i*nels+i)*T2ab.n_cols+(a*virt+a)]+=x;
	if(fabs(x) > maxT2ab)
	{
	  maxT2ab = x;
	}
      for(int j=i+1;j<nels;j++)
      {
	for(int b=a+1;b<virt;b++)
	{
	  x=dE1dT2aa(i,j,a,b,(*iPtr).K1,T1,T2aa,T2ab,F1m);
	  x+=dE2ab_dT2aa(i,j,a,b,(*iPtr).V2ab,T1,T2aa,T2ab,F1m,F2abM);
	  x+=dE2aa_dT2aa(i,j,a,b,(*iPtr).V2aa,T1,T2aa,T2ab,F1m,F2aaM);
	  grad[n1+indexD(i,j,nels)*T2aa.n_cols+indexD(a,b,virt)]=0.0;
	  grad[n1+indexD(i,j,nels)*T2aa.n_cols+indexD(a,b,virt)]+=x;
	if(fabs(x) > maxT2aa)
	{
	  maxT2aa = x;
	}
	}
      }
      for(int j=0;j<nels;j++)
      {
	for(int b=0;b<virt && (j*virt+b)<i*virt+a;b++)
	{
	  x=dE1dT2ab(i,j,a,b,(*iPtr).K1,T1,T2aa,T2ab,F1m);
	  x+=dE1dT2ab(j,i,b,a,(*iPtr).K1,T1,T2aa,T2ab,F1m);
	  x+=dE2ab_dT2ab(i,j,a,b,(*iPtr).V2ab,T1,T2aa,T2ab,F1m,F2abM);
	  x+=dE2ab_dT2ab(j,i,b,a,(*iPtr).V2ab,T1,T2aa,T2ab,F1m,F2abM);
	  x+=dE2aa_dT2ab(i,j,a,b,(*iPtr).V2aa,T1,T2aa,T2ab,F1m,F2aaM);
	  x+=dE2aa_dT2ab(j,i,b,a,(*iPtr).V2aa,T1,T2aa,T2ab,F1m,F2aaM);
	  grad[n1+n2aa+(i*nels+j)*T2ab.n_cols+(a*virt+b)]=0.0;
	  grad[n1+n2aa+(j*nels+i)*T2ab.n_cols+(b*virt+a)]=0.0;
	  grad[n1+n2aa+(i*nels+j)*T2ab.n_cols+(a*virt+b)]+=x;
	  grad[n1+n2aa+(j*nels+i)*T2ab.n_cols+(b*virt+a)]+=x;
	if(fabs(x) > maxT2ab)
	{
	  maxT2ab = x;
	}
	}
      }
    }
  }
  std::cout << "\tE:\t" << E;
  std::cout << "\tmax dT2ab:\t" << maxT2ab;
  std::cout << "\tmax dT2aa:\t" << maxT2aa;
  std::cout << "\tmax dT1:\t" << maxT1 << std::endl << std::endl;

  lastTime=time(NULL);
  (*iPtr).timG += lastTime-startTime;
*/
  return;
}

void makeDnew(arma::mat &D1,arma::mat &D2aa,arma::mat &D2ab,const arma::mat &T1,const arma::mat &T2aa,const arma::mat &T2ab,arma::mat &F1m,arma::mat &F2aaM,arma::mat &F2abM,time_t &tim,void *ptr,real_1d_array &grad)
{
  intBundle *iPtr=static_cast<intBundle*>(ptr);
  time_t startTime,lastTime;
  startTime = time(NULL);
  int nact = D1.n_rows;
  int nels = T1.n_rows;
  int virt = T1.n_cols;
  nact = nels+virt;
  D1.zeros(); D2aa.zeros(); D2ab.zeros();
  double x,y;
  arma::mat U,V,W,Y;
  int n1,n2,n3,n4,n5,n6,n7,n8;
  int n1a=nels*virt;
  int n2aa=T2aa.n_rows*T2aa.n_cols;

  for(int b=0;b<virt;b++)
  {
    for(int j=0;j<nels;j++)
    {
      F1m(j,b)=1.0+T1(j,b)*T1(j,b);
    }
  }
  n1=0;
  for(int a=0;a<virt;a++)
  {
    for(int b=0;b<virt;b++)
    {
      n1=a*virt+b;
      n2=0;
      for(int i=0;i<nels;i++)
      {
	for(int j=0;j<nels;j++)
	{
	  F2abM(n2,n1) = 1.0-3.0*T2ab(n2,n1)*T2ab(n2,n1);
	  F2abM(n2,n1) += T1(i,a)*T1(i,a) + T1(j,b)*T1(j,b);
//	  F2abM(i*nels+j,a*virt+b) = 1.0-3.0*T2ab(i*nels+j,a*virt+b)*T2ab(i*nels+j,a*virt+b);
//	  F2abM(i*nels+j,a*virt+b) += T1(i,a)*T1(i,a) + T1(j,b)*T1(j,b);
	  n2++;
	}
      }
      n1++;
    }
  }
  n1=0;
  for(int a=0;a<virt;a++)
  {
    for(int b=a+1;b<virt;b++)
    {
      n2=0;
      for(int i=0;i<nels;i++)
      {
	for(int j=i+1;j<nels;j++)
	{
	  F2aaM(n2,n1) = 1.0-3.0*T2aa(n2,n1)*T2aa(n2,n1);
	  F2aaM(n2,n1) += T1(i,a)*T1(i,a) + T1(i,b)*T1(i,b) + T1(j,a)*T1(j,a) + T1(j,b)*T1(j,b);
	  //F2aaM(indexD(i,j,nels),indexD(a,b,virt)) = 1.0-3.0*T2aa(indexD(i,j,nels),indexD(a,b,virt))*T2aa(indexD(i,j,nels),indexD(a,b,virt));
	  //F2aaM(indexD(i,j,nels),indexD(a,b,virt)) += T1(i,a)*T1(i,a) + T1(i,b)*T1(i,b) + T1(j,a)*T1(j,a) + T1(j,b)*T1(j,b);
	  n2++;
	}
      }
      n1++;
    }
  }

  for(int m=0;m<nels;m++)
  {
    n1=indexD1(m,nact);
    for(int n=m+1;n<nels;n++)
    {
      n2=n1+n;
      D2aa(n2,n2) = 1.0;
    }
  }
  n1=0;
  for(int m=0;m<nels;m++)
  {
    n1=m*nact;
    for(int n=0;n<nels;n++)
    {
      //D2ab(m*nact+n,m*nact+n) = 1.0;
      n2=n1+n;
      D2ab(n2,n2) = 1.0;
    }
  }

  //RED
  
  W=T2ab*T2ab.t();
  V=-T1*T1.t();
  for(int m=0;m<nels;m++)
  {
    x = V(m,m);
    for(int a=0;a<virt;a++)
    {
      F1m(m,a) += x;
      for(int n=0;n<nels;n++)
      {
	n3=m*nels+n;
	n5=n*nels+m;
	for(int b=0;b<virt;b++)
	{
	  n4=a*virt+b;
	  F2abM(n3,n4) += x;
	  F2abM(n5,n4) += x;
	  //F2abM(m*nels+n,a*virt+b) += x;
	  //F2abM(n*nels+m,a*virt+b) += x;
	}
      }
      for(int b=a+1;b<virt;b++)
      {
	n2 = indexD(a,b,virt);
	for(int n=0;n<m;n++)
	{
	  n1 = indexD(n,m,nels);
	  F2aaM(n1,n2) += x;
	  //F2aaM(indexD(n,m,nels),indexD(a,b,virt)) += x;
	}
	for(int n=m+1;n<nels;n++)
	{
	  n1= indexD(m,n,nels);
	  F2aaM(n1,n2) += x;
	  //F2aaM(indexD(m,n,nels),indexD(a,b,virt)) += x;
	}
      }
    }
  }
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      n1 = m*nels+n;
      x = W(n1,n1);
      //x = W(m*nels+n,m*nels+n);
      for(int e=0;e<virt;e++)
      {
	for(int f=0;f<virt;f++)
	{
	  n2 = e*virt+f;
	  F2abM(n1,n2) -= x;
	  //F2abM(m*nels+n,e*virt+f) -= x;
	}
      }
    }
  }
  for(int i=0;i<nels;i++)
  {
    for(int j=0;j<nels;j++)
    {
      n1 = i*nact+j;
      n5 = i*nels+j;
      for(int k=0;k<nels;k++)
      {
	n2 = i*nels+k;
	n3 = j*nels+k;
	V(i,j) -= W(n2,n3);
	//V(i,j) -= W(i*nels+k,j*nels+k);
	for(int l=0;l<nels;l++)
	{
	  n4 = k*nact+l;
	  n6 = k*nels+l;
	  D2ab(n1,n4)+=W(n5,n6);
	  //D2ab(i*nact+j,k*nact+l)+=W(i*nels+j,k*nels+l);
	}
      }
    }
  }
  W=T2aa*T2aa.t();
  for(int i=0;i<nels;i++)
  {
    for(int j=0;j<nels;j++)
    {
      for(int k=0;k<std::min(i,j);k++)
      {
	n1 = indexD(k,i,nels);
	n2 = indexD(k,j,nels);
	V(i,j) -= W(n1,n2);
	//V(i,j) -= W(indexD(k,i,nels),indexD(k,j,nels));
      }
      for(int k=i+1;k<j;k++)
      {
	n1 = indexD(i,k,nels);
	n2 = indexD(k,j,nels);
	V(i,j) += W(n1,n2);
	//V(i,j) += W(indexD(i,k,nels),indexD(k,j,nels));
      }
      for(int k=j+1;k<i;k++)
      {
	n1 = indexD(k,i,nels);
	n2 = indexD(j,k,nels);
	V(i,j) += W(n1,n2);
	//V(i,j) += W(indexD(k,i,nels),indexD(j,k,nels));
      }
      for(int k=std::max(i,j)+1;k<nels;k++)
      {
	n1 = indexD(i,k,nels);
	n2 = indexD(j,k,nels);
	V(i,j) -= W(n1,n2);
	//V(i,j) -= W(indexD(i,k,nels),indexD(j,k,nels));
      }
      for(int k=i+1;k<nels;k++)
      {
	n3=indexD(i,k,nels);
	n1 = indexD(i,k,nact);
	for(int l=j+1;l<nels;l++)
	{
	  n4=indexD(j,l,nels);
	  n2=indexD(j,l,nact);
	  D2aa(n1,n2)+=W(n3,n4);
	  //D2aa(n1,n2)+=W(indexD(i,k,nels),indexD(j,l,nels));
	}
      }
    }
  }
  D1.submat(0,0,nels-1,nels-1)=V;
  for(int i=0;i<nels;i++)
  {
    D1(i,i)+=1.0;
    for(int j=0;j<nels;j++)
    {
      n1 = i*nact+j;
      n2 = j*nact+i;
      for(int k=0;k<nels;k++)
      {
	n3 = i*nact+k;
	n4 = k*nact+i;
	D2ab(n1,n3) += V(j,k);
	D2ab(n2,n4) += V(j,k);
	//D2ab(i*nact+j,i*nact+k) += V(j,k);
	//D2ab(j*nact+i,k*nact+i) += V(j,k);
      }
    }
    for(int j=i+1;j<nels;j++)
    {
      n1 = indexD(i,j,nact);
      for(int k=0;k<i;k++)
      {
	n2 = indexD(k,i,nact);
	D2aa(n1,n2)-=V(j,k);
      }
      for(int k=i+1;k<nels;k++)
      {
	n2 = indexD(i,k,nact);
	D2aa(n1,n2)+=V(j,k);
      }
      for(int k=0;k<j;k++)
      {
	n2 = indexD(k,j,nact);
	D2aa(n1,n2)+=V(i,k);
      }
      for(int k=j+1;k<nels;k++)
      {
	n2 = indexD(j,k,nact);
	D2aa(n1,n2)-=V(i,k);
      }
    }
  }
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      n1 = indexD(m,n,nels);
      x = W(n1,n1);
      //x = W(indexD(m,n,nels),indexD(m,n,nels));
      for(int e=0;e<virt;e++)
      {
	for(int f=e+1;f<virt;f++)
	{
	  n2=indexD(e,f,virt);
	  F2aaM(n1,n2) -= x;
	  //F2aaM(indexD(m,n,nels),indexD(e,f,virt)) -= x;
	}
      }
    }
  }

  W=T2ab.t()*T2ab;
  V=T1.t()*T1;
  for(int a=0;a<virt;a++)
  {
    x = V(a,a);
    for(int m=0;m<nels;m++)
    {
      F1m(m,a) -= x;
      for(int n=0;n<nels;n++)
      {
	n1=m*nels+n;
	for(int b=0;b<virt;b++)
	{
	  n2=a*virt+b;
	  n3=b*virt+a;
	  F2abM(n1,n2) -= x;
	  F2abM(n1,n3) -= x;
	  //F2abM(m*nels+n,a*virt+b) -= x;
	  //F2abM(m*nels+n,b*virt+a) -= x;
	}
      }
      for(int n=m+1;n<nels;n++)
      {
	n1 = indexD(m,n,nels);
	for(int b=0;b<a;b++)
	{
	  n2=indexD(b,a,virt);
	  F2aaM(n1,n2) -= x;
	  //F2aaM(indexD(m,n,nels),indexD(b,a,virt)) -= x;
	}
	for(int b=a+1;b<virt;b++)
	{
	  n2=indexD(a,b,virt);
	  F2aaM(n1,n2) -= x;
	  //F2aaM(indexD(m,n,nels),indexD(a,b,virt)) -= x;
	}
      }
    }
  }
  for(int e=0;e<virt;e++)
  {
    for(int f=0;f<virt;f++)
    {	
      n1=(e+nels)*nact+f+nels;
      n2=e*virt+f;
      for(int g=0;g<virt;g++)
      {
	n3=e*virt+g;
	n4=f*virt+g;
	V(e,f) += W(n3,n4);
	//V(e,f) += W(e*virt+g,f*virt+g);
	for(int h=0;h<virt;h++)
	{
	  n5=(g+nels)*nact+h+nels;
	  n6=g*virt+h;
	  D2ab(n1,n5) += W(n2,n6);
	  //D2ab((e+nels)*nact+f+nels,(g+nels)*nact+h+nels) += W(e*virt+f,g*virt+h);
	}
      }
      x = W(n2,n2);
      //x = W(e*virt+f,e*virt+f);
      for(int m=0;m<nels;m++)
      {
	for(int n=0;n<nels;n++)
	{
	  n7=m*nels+n;
	  F2abM(n7,n2) -= x;
	  //F2abM(m*nels+n,e*virt+f) -= x;
	}
      }
    }
  }
  W=T2aa.t()*T2aa;
  for(int e=0;e<virt;e++)
  {
    for(int f=0;f<virt;f++)
    {
      for(int g=0;g<std::min(e,f);g++)
      {
	V(e,f) += W(indexD(g,e,virt),indexD(g,f,virt));
      }
      for(int g=e+1;g<f;g++)
      {
	V(e,f) -= W(indexD(e,g,virt),indexD(g,f,virt));
      }
      for(int g=f+1;g<e;g++)
      {
	V(e,f) -= W(indexD(g,e,virt),indexD(f,g,virt));
      }
      for(int g=std::max(e,f)+1;g<virt;g++)
      {
	V(e,f) += W(indexD(e,g,virt),indexD(f,g,virt));
      }
      for(int g=e+1;g<virt;g++)
      {
	n1 = indexD(e+nels,g+nels,nact);
	n3 = indexD(e,g,virt);
	for(int h=f+1;h<virt;h++)
	{
	  n2 = indexD(f+nels,h+nels,nact);
	  n4 = indexD(f,h,virt);
	  D2aa(n1,n2) += W(n3,n4);
	  //D2aa(n1,n2) += W(indexD(e,g,virt),indexD(f,h,virt));
	}
      }
    }
  }
  D1.submat(nels,nels,nact-1,nact-1)=V;
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      n3=m*nact+e+nels;;
      n4=(e+nels)*nact+m;
      n1 = indexD(m,e+nels,nact);
      for(int f=0;f<virt;f++)
      {
	n5=m*nact+f+nels;;
	n6=(f+nels)*nact+m;
	D2ab(n3,n5) += V(e,f);
	D2ab(n4,n6) += V(e,f);
	//D2ab(m*nact+e+nels,m*nact+f+nels) += V(e,f);
	//D2ab((e+nels)*nact+m,(f+nels)*nact+m) += V(e,f);
	D2aa(n1,indexD(m,f+nels,nact)) += V(e,f);
      }
    }
  }
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      n1 = indexD(e,f,virt);;
      x = W(n1,n1);
      //x = W(indexD(e,f,virt),indexD(e,f,virt));
      for(int m=0;m<nels;m++)
      {
	for(int n=m+1;n<nels;n++)
	{
	  F2aaM(indexD(m,n,nels),n1) -= x;
	  //F2aaM(indexD(m,n,nels),indexD(e,f,virt)) -= x;
	}
      }
    }
  }

  W.set_size(nels*virt,nels*virt);
//W_bj_ai = T2ab_ji_ab
  for(int a=0;a<virt;a++)
  {
    for(int b=0;b<virt;b++)
    {
      n3=a*virt+b;
      for(int i=0;i<nels;i++)
      {
	n1=a*nels+i;
	for(int j=0;j<nels;j++)
	{
	  W(b*nels+j,n1) = T2ab(j*nels+i,n3);
	  //W(j*virt+b,i*virt+a) = T2ab(j*nels+i,a*virt+b);
	}
      }
    }
  }
  U=(*iPtr).V2abia_jb2*W;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      for(int f=0;f<virt;f++)
      {
	for(int n=0;n<nels;n++)
	{
	  grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+(m*nels+n)] -= 2.0*(U(f*nels+m,e*nels+n)+U(e*nels+n,f*nels+m)); 
	}
      }
    }
  }
  U = W*W.t();
  for(int f=0;f<virt;f++)
  {
    for(int m=0;m<nels;m++)
    {
      n1=f*nels+m;
      for(int n=0;n<nels;n++)
      {
	n2=(f+nels)*nact+n;
	n3=n*nact+f+nels;
	for(int e=0;e<virt;e++)
	{
	  D2ab(m*nact+e+nels,n3) -= U(e*nels+n,n1);
	  D2ab((e+nels)*nact+m,n2) -= U(e*nels+n,n1);
	  //D2ab(m*nact+e+nels,n*nact+f+nels)-=U(n*virt+e,m*virt+f);
	  //D2ab((e+nels)*nact+m,(f+nels)*nact+n)-=U(n*virt+e,m*virt+f);
	}
      }
    }
  }
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      n1=e*nels+m;
      x = U(n1,n1);
      //x = U(m*virt+e,m*virt+e);
      for(int f=0;f<virt;f++)
      {
	n2=e*virt+f;
	n3=f*virt+e;
	for(int n=0;n<nels;n++)
	{
	  F2abM(m*nels+n,n3) -= x;
	  F2abM(n*nels+m,n2) -= x;
	  //F2abM(m*nels+n,f*virt+e) -= x;
	  //F2abM(n*nels+m,e*virt+f) -= x;
	}
      }
    }
  }

//W_bj_ai = T2ab_ij_ab
  for(int a=0;a<virt;a++)
  {
    for(int i=0;i<nels;i++)
    {
      n1=a*nels+i;
      for(int b=0;b<virt;b++)
      {
	n2=a*virt+b;
	for(int j=0;j<nels;j++)
	{
	  W(b*nels+j,n1) = T2ab(i*nels+j,n2);
	  //W(j*virt+b,i*virt+a) = T2ab(i*nels+j,a*virt+b);
	}
      }
    }
  }
  V.set_size(nels*virt,nels*virt);
//V_ai_fm = T2aa_mi_fa
  for(int a=0;a<virt;a++)
  {
    for(int i=0;i<nels;i++)
    {
      n3=a*nels+i;
      for(int m=0;m<nels;m++)
      {
	V(n3,a*nels+m)=0.0;
	//V(i*virt+a,i*virt+b)=0.0;
      }
      for(int f=0;f<a;f++)
      {
	n1=indexD(f,a,virt);
	V(n3,f*nels+i)=0.0;
	for(int m=0;m<i;m++)
	{
	  V(n3,f*nels+m) = T2aa(indexD(m,i,nels),n1);
	  //V(i*virt+a,m*virt+f) = T2aa(indexD(m,i,nels),indexD(f,a,virt));
	}
	for(int m=i+1;m<nels;m++)
	{
	  V(n3,f*nels+m) = -T2aa(indexD(i,m,nels),n1);
	}
      }
      for(int f=a+1;f<virt;f++)
      {
	n1=indexD(a,f,virt);
	V(n3,f*nels+i)=0.0;
	for(int m=0;m<i;m++)
	{
	  V(n3,f*nels+m) = -T2aa(indexD(m,i,nels),n1);
	}
	for(int m=i+1;m<nels;m++)
	{
	  V(n3,f*nels+m) = T2aa(indexD(i,m,nels),n1);
	}
      }
    }
  }
  U=(*iPtr).V2abia_jb*V-(*iPtr).V2aaia_jb*W;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[n1a+n2aa+(e*virt+e)*T2ab.n_rows+m*nels+m] += 4.0*U(e*nels+m,e*nels+m);
      for(int f=0;f<virt;f++)
      {
	for(int n=0;n<nels && f*nels+n<e*nels+m;n++)
	{
	  x = 2.0*(U(f*nels+n,e*nels+m)+U(e*nels+m,f*nels+n));
	  grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+m*nels+n] += x;
	  grad[n1a+n2aa+(f*virt+e)*T2ab.n_rows+n*nels+m] += x;
	}
      }
    }
  }
  U = W*V;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      n1=m*nact+e+nels;
      n2=(e+nels)*nact+m;
      for(int f=0;f<virt;f++)
      {
	n3=f*nels+m;
	for(int n=0;n<nels;n++)
	{
	  x = T1(n,e)*T1(m,f) + U(e*nels+n,n3) + U(n3,e*nels+n);
	  //x = T1(n,e)*T1(m,f) + U(n*virt+e,m*virt+f) + U(m*virt+f,n*virt+e);
	  D2ab(n1,(f+nels)*nact+n) += x;
	  D2ab(n2,n*nact+f+nels) += x;
	  //D2ab(m*nact+e+nels,(f+nels)*nact+n) += x;
	  //D2ab((e+nels)*nact+m,n*nact+f+nels) += x;
	}
      }
    }
  }
  U=(*iPtr).K1ia*(W+V);
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[e*nels+m]+=4.0*U(0,e*nels+m);
      //grad[e*nels+m]+=4.0*U(0,m*virt+e);
    }
  }
  U=(*iPtr).V1ia*(W+V);
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[e*nels+m]+=4.0*U(0,e*nels+m);
    }
  }
  U=(*iPtr).V1aia*(W+V);
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[e*nels+m]+=4.0*U(0,e*nels+m);
    }
  }
  U=(*iPtr).V2abia_jb*W-(*iPtr).V2aaia_jb*V;
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      n1=indexD(e,f,virt);
      for(int m=0;m<nels;m++)
      {
	for(int n=m+1;n<nels;n++)
	{
	  x=4.0*(U(e*nels+m,f*nels+n)-U(f*nels+m,e*nels+n)-U(e*nels+n,f*nels+m)+U(f*nels+n,e*nels+m));
	  grad[n1a+n1*T2aa.n_rows+indexD(m,n,nels)] += x;
	}
      }
    }
  }
  U=(*iPtr).V2aaia_jb*W;
  for(int e=0;e<virt;e++)
  {
    for(int f=0;f<virt;f++)
    {
      for(int m=0;m<nels;m++)
      {
	for(int n=0;n<nels;n++)
	{
	  //grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+m*nels+n] -= 2.0*(U(e*nels+m,f*nels+n)+U(f*nels+n,e*nels+m));
	}
      }
    }
  }

  U=W*W.t();
  for(int f=0;f<virt;f++)
  {
    for(int m=0;m<nels;m++)
    {
      n3=f*nels+m;
      F1m(m,f) += U(n3,n3);
      //F1m(m,f) += U(m*virt+f,m*virt+f);
      for(int e=0;e<virt;e++)
      {
	n1 = indexD(m,e+nels,nact);
	for(int n=0;n<nels;n++)
	{
	  n2 = indexD(n,f+nels,nact);
	  D2aa(n2,n1) -= T1(n,e)*T1(m,f);
	  D2aa(n2,n1) -= U(e*nels+n,n3);
	  //D2aa(n1,n2) -= U(n*virt+e,m*virt+f);
	}
      }
    }
  }
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      n1=e*nels+m;
      x=U(n1,n1);
      //x = U(m*virt+e,m*virt+e);
      for(int f=0;f<virt;f++)
      {
	n2=e*virt+f;
	n3=f*virt+e;
	for(int n=0;n<nels;n++)
	{
	  F2abM(n*nels+m,n3) -= x;
	  F2abM(m*nels+n,n2) -= x;
	  //F2abM(n*nels+m,f*virt+e) -= x;
	  //F2abM(m*nels+n,e*virt+f) -= x;
	}
      }
      for(int f=0;f<e;f++)
      {
	n2=indexD(f,e,virt);
	for(int n=0;n<m;n++)
	{
	  F2aaM(indexD(n,m,nels),n2) -= x;
	  //F2aaM(indexD(n,m,nels),indexD(f,e,virt)) -= x;
	}
	for(int n=m+1;n<nels;n++)
	{
	  F2aaM(indexD(m,n,nels),n2) -= x;
	}
      }
      for(int f=e+1;f<virt;f++)
      {
	n2=indexD(e,f,virt);
	for(int n=0;n<m;n++)
	{
	  F2aaM(indexD(n,m,nels),n2) -= x;
	  //F2aaM(indexD(n,m,nels),indexD(f,e,virt)) -= x;
	}
	for(int n=m+1;n<nels;n++)
	{
	  F2aaM(indexD(m,n,nels),n2) -= x;
	}
      }
    }
  }
  U=V.t()*V;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      n3=e*nels+m;
      n1=indexD(m,e+nels,nact);
      F1m(m,e) += U(n3,n3);
      //F1m(m,e) += U(m*virt+e,m*virt+e);
      for(int n=0;n<nels;n++)
      {
	n4=e*nels+n;
	for(int f=0;f<virt;f++)
	{
	  D2aa(indexD(n,f+nels,nact),n1) -= U(f*nels+m,n4);
	  //D2aa(indexD(m,e+nels,nact),indexD(n,f+nels,nact)) -= U(n4,m*virt+f);
	}
      }
    }
  }
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      n3=e*nels+m;
      x=U(n3,n3);
      //x = U(m*virt+e,m*virt+e);
      for(int f=0;f<virt;f++)
      {
	n4=e*virt+f;
	n5=f*virt+e;
	for(int n=0;n<nels;n++)
	{
	  F2abM(m*nels+n,n4) -= x;
	  F2abM(n*nels+m,n5) -= x;
	  //F2abM(m*nels+n,e*virt+f) -= x;
	  //F2abM(n*nels+m,f*virt+e) -= x;
	}
      }
      for(int f=0;f<e;f++)
      {
	n1 = indexD(f,e,virt);
	for(int n=0;n<m;n++)
	{
	  F2aaM(indexD(n,m,nels),n1) -= x;
	  //F2aaM(indexD(n,m,nels),indexD(f,e,virt)) -= x;
	}
	for(int n=m+1;n<nels;n++)
	{
	  F2aaM(indexD(m,n,nels),n1) -= x;
	  //F2aaM(indexD(n,m,nels),indexD(f,e,virt)) -= x;
	}
      }
      for(int f=e+1;f<virt;f++)
      {
	n1 = indexD(e,f,virt);
	for(int n=0;n<m;n++)
	{
	  F2aaM(indexD(n,m,nels),n1) -= x;
	  //F2aaM(indexD(n,m,nels),indexD(f,e,virt)) -= x;
	}
	for(int n=m+1;n<nels;n++)
	{
	  F2aaM(indexD(m,n,nels),n1) -= x;
	  //F2aaM(indexD(n,m,nels),indexD(f,e,virt)) -= x;
	}
      }
    }
  }

//W_i_fgm = T2ab_mi_fg
  W.set_size(nels,nels*virt*virt);
  for(int f=0;f<virt;f++)
  {
    for(int g=0;g<virt;g++)
    {
      n1=f*virt+g;
      for(int m=0;m<nels;m++)
      {
	n2=n1*nels+m;
	for(int i=0;i<nels;i++)
	{
	  W(i,n2) = T2ab(m*nels+i,n1);
	  //W(i,(f*virt+g)*nels + m) = T2ab(m*nels+i,f*virt+g);
	}
      }
    }
  }
  U=(*iPtr).V2abh_fgn*W.t();
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[e*nels+m] += 4.0*U(e,m);
    }
  }
  U=((*iPtr).K1i_j+(*iPtr).V1i_j+(*iPtr).V1ai_j)*W;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[n1a+n2aa+(e*virt+e)*T2ab.n_rows+(m*nels+m)]-=4.0*U(m,(e*virt+e)*nels+m);
      for(int f=0;f<virt;f++)
      {
	n1=e*virt+f;
	n2=n1*nels+m;
	n3=f*virt+e;
	for(int n=0;n<nels && f*nels+n<e*nels+m;n++)
	{
	  x = U(n,n2) + U(m,n3*nels+n);
	  grad[n1a+n2aa+n1*T2ab.n_rows+m*nels+n] -= 2.0*x;
	  grad[n1a+n2aa+n3*T2ab.n_rows+n*nels+m] -= 2.0*x;
	}
      }
    }
  }

  for(int f=0;f<virt;f++)
  {
    for(int g=0;g<virt;g++)
    {
      n3=f*virt+g;
      n4=g*virt+f;
      for(int m=0;m<nels;m++)
      {
	n1=n3*nels+m;
	Y = W.col(n1).t()*W.col(n1);
	//Y = W.col((f*virt+g)*nels+m).t()*W.col((f*virt+g)*nels+m);
	for(int n=0;n<nels;n++)
	{
	  F2abM(m*nels+n,n3) += 2.0*Y(0);
	  F2abM(n*nels+m,n4) += 2.0*Y(0);
	  //F2abM(m*nels+n,f*virt+g) += 2.0*Y(0);
	  //F2abM(n*nels+m,g*virt+f) += 2.0*Y(0);
	}
      }
    }
  }
  for(int m=0;m<nels;m++)
  {
    Y = W.row(m)*W.row(m).t();
    for(int e=0;e<virt;e++)
    {
      F1m(m,e) -= Y(0);
    }
  }
  U = T1.t()*W;
  for(int f=0;f<virt;f++)
  {
    for(int g=0;g<virt;g++)
    {
      n2=(f+nels)*nact+g+nels;
      n3=(g+nels)*nact+f+nels;
      n6=(f*virt+g)*nels;
      for(int m=0;m<nels;m++)
      {
	n1=n6+m;
	for(int e=0;e<virt;e++)
	{
	  n4=m*nact+e+nels;
	  n5=(e+nels)*nact+m;
	  x = U(e,n1);
	  D2ab(n4,n2) += x;
	  D2ab(n2,n4) += x;
	  D2ab(n5,n3) += x;
	  D2ab(n3,n5) += x;
	  //x = U(e,(f*virt+g)*nels+m);
	  //D2ab(m*nact+e+nels,(f+nels)*nact+g+nels) += x;
	  //D2ab((f+nels)*nact+g+nels,m*nact+e+nels) += x;
	  //D2ab((e+nels)*nact+m,(g+nels)*nact+f+nels) += x;
	  //D2ab((g+nels)*nact+f+nels,(e+nels)*nact+m) += x;
	}
      }
    }
  }

  W.set_size(nels,nels*virt*(virt-1)/2);
//W_i_fgm = -T2aa_im_fg
  for(int f=0;f<virt;f++)
  {
    for(int g=f+1;g<virt;g++)
    {
      n2=indexD(f,g,virt);
      for(int m=0;m<nels;m++)
      {
	n3=n2*nels+m;
	W(m,n3)=0.0;;
	//W(m,indexD(f,g,virt)*nels+m) = 0.0;
	for(int i=0;i<m;i++)
	{
	  W(i,n3) = -T2aa(indexD(i,m,nels),n2);
	  //W(i,indexD(f,g,virt)*nels+m) = -T2aa(indexD(i,m,nels),indexD(f,g,virt));
	}
	for(int i=m+1;i<nels;i++)
	{
	  W(i,n3) = T2aa(indexD(m,i,nels),n2);
	  //W(i,indexD(f,g,virt)*nels+m) = T2aa(indexD(m,i,nels),indexD(f,g,virt));
	}
      }
    }
  }
  U=(*iPtr).V2aah_fgn*W.t();
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[e*nels+m] += 2.0*U(e,m);
    }
  }
  U=((*iPtr).K1i_j+(*iPtr).V1i_j+(*iPtr).V1ai_j)*W;
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      n1=indexD(e,f,virt);
      for(int m=0;m<nels;m++)
      {
	for(int n=m+1;n<nels;n++)
	{
	  n2=indexD(m,n,nels);
	  grad[n1a+n1*T2aa.n_rows+n2] += 4.0*(U(m,n1*nels+n)-U(n,n1*nels+m));
	}
      }
    }
  }
  for(int m=0;m<nels;m++)
  {
    Y = W.row(m)*W.row(m).t();
    for(int e=0;e<virt;e++)
    {
      F1m(m,e) -= Y(0);
    }
  }
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      n2 = indexD(e,f,virt);
      for(int m=0;m<nels;m++)
      {
	n3=n2*nels+m;
	Y = W.col(n3).t()*W.col(n3);
	//Y = W.col(n2*nels+m).t()*W.col(n2*nels+m);
	//Y = W.col(indexD(e,f,virt)*nels+m).t()*W.col(indexD(e,f,virt)*nels+m);
	for(int n=0;n<m;n++)
	{
	  F2aaM(indexD(n,m,nels),n2) += 2.0*Y(0);
	  //F2aaM(indexD(n,m,nels),indexD(e,f,virt)) += 2.0*Y(0);
	}
	for(int n=m+1;n<nels;n++)
	{
	  F2aaM(indexD(m,n,nels),n2) += 2.0*Y(0);
	  //F2aaM(indexD(m,n,nels),indexD(e,f,virt)) += 2.0*Y(0);
	}
      }
    }
  }

  U = T1.t()*W;
  for(int f=0;f<virt;f++)
  {
    for(int g=f+1;g<virt;g++)
    {
      n3 = indexD(f,g,virt)*nels;
      n1=indexD(f+nels,g+nels,nact);
      for(int m=0;m<nels;m++)
      {
	n4=n3+m;
	for(int e=0;e<virt;e++)
	{
	  x = U(e,n4);
	  //x = U(e,(indexD(f,g,virt))*nels+m);
      //n1=indexD(f+nels,g+nels,nact);
	  n2=indexD(m,e+nels,nact);
	  D2aa(n1,n2) += x;
	  D2aa(n2,n1) += x;
//	  D2aa(indexD(f+nels,g+nels,nact),indexD(m,e+nels,nact)) += x;
//	  D2aa(indexD(m,e+nels,nact),indexD(f+nels,g+nels,nact)) += x;
	}
      }
    }
  }
  W.set_size(nels*nels*virt,virt);
//W_ija_b = T2ab_ij_ba
  for(int i=0;i<nels;i++)
  {
    for(int j=0;j<nels;j++)
    {
      n2=i*nels+j;
      for(int a=0;a<virt;a++)
      {
	n1=n2*virt+a;
	for(int b=0;b<virt;b++)
	{
	  W(n1,b)=T2ab(n2,b*virt+a);
	  //W((i*nels+j)*virt+b,a)=T2ab(i*nels+j,a*virt+b);
	}
      }
    }
  }
  U=(*iPtr).V2abi_jka*W;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[e*nels+m] -= 4.0*U(m,e);
    }
  }
  U=W*((*iPtr).K1a_b+(*iPtr).V1a_b+(*iPtr).V1aa_b);
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[n1a+n2aa+(e*virt+e)*T2ab.n_rows+m*nels+m] += 4.0*U((m*nels+m)*virt+e,e);
      for(int f=0;f<virt;f++)
      {
	for(int n=0;n<nels && f*nels+n<e*nels+m;n++)
	{
	  x=2.0*( U((m*nels+n)*virt+f,e)+U((n*nels+m)*virt+e,f));
	  grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+m*nels+n] += x;
	  grad[n1a+n2aa+(f*virt+e)*T2ab.n_rows+n*nels+m] += x;
	}
      }
    }
  }
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      n2=m*nels+n;
      n3=n*nels+m;
      for(int e=0;e<virt;e++)
      {
	n1=n2*virt+e;
	Y = W.row(n1)*W.row(n1).t();
	//Y = W.row((m*nels+n)*virt+e)*W.row((m*nels+n)*virt+e).t();
	for(int f=0;f<virt;f++)
	{
	  F2abM(n2,f*virt+e) += 2.0*Y(0);
	  F2abM(n3,e*virt+f) += 2.0*Y(0);
	  //F2abM(m*nels+n,f*virt+e) += 2.0*Y(0);
	  //F2abM(n*nels+m,e*virt+f) += 2.0*Y(0);
	}
      }
    }
  }
  for(int a=0;a<virt;a++)
  {
    Y = W.col(a).t()*W.col(a);
    for(int m=0;m<nels;m++)
    {
      F1m(m,a) -= Y(0);
    }
  }
  V=W*T1.t();
  U.set_size(nels,virt);
  U.zeros();
  for(int i=0;i<nels;i++)
  {
    for(int j=0;j<nels;j++)
    {
      n1=i*nact+j;
      n2=j*nact+i;
      n3=j*nels+i;
      n5=i*nels+j;
      for(int a=0;a<virt;a++)
      {
	n4=n3*virt+a;
	n6=n5*virt+a;
	U(i,a) += V(n4,j);
	//U(i,a) += V((j*nels+i)*virt+a,j);
	for(int k=0;k<nels;k++)
	{
	  x=V(n6,k);
	  n7=k*nact+a+nels;
	  n8=(a+nels)*nact+k;
	  D2ab(n1,n7)-=x;
	  D2ab(n7,n1)-=x;
	  D2ab(n2,n8)-=x;
	  D2ab(n8,n2)-=x;
	  //x=V((i*nels+j)*virt+a,k);
	  //D2ab(i*nact+j,k*nact+a+nels)-=x;
	  //D2ab(k*nact+a+nels,i*nact+j)-=x;
	  //D2ab(j*nact+i,(a+nels)*nact+k)-=x;
	  //D2ab((a+nels)*nact+k,j*nact+i)-=x;
	}
      }
    }
  }

  W.set_size(nels*(nels-1)/2*virt,virt); 
//W_jka_b = T2aa_jk_ab
  for(int j=0;j<nels;j++)
  {
    for(int k=j+1;k<nels;k++)
    {
      n1 = indexD(j,k,nels);
      for(int a=0;a<virt;a++)
      {
	n3=n1*virt+a;
	W(n3,a)=0.0;
	//W(indexD(j,k,nels)*virt+a,a)=0.0;
	for(int b=a+1;b<virt;b++)
	{
	  x=T2aa(n1,indexD(a,b,virt));
	  W(n3,b)=-x;
	  W(n1*virt+b,a)=x;
	  //W(indexD(j,k,nels)*virt+a,b)=-T2aa(indexD(j,k,nels),indexD(a,b,virt));
	  //W(indexD(j,k,nels)*virt+b,a)=T2aa(indexD(j,k,nels),indexD(a,b,virt));
	}
      }
    }
  }
  //U=((*iPtr).K1a_b+(*iPtr).V1a_b)*W.t();
  U=((*iPtr).K1a_b+(*iPtr).V1a_b+(*iPtr).V1aa_b)*W.t();
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      n1=indexD(m,n,nels);
      for(int e=0;e<virt;e++)
      {
	n2=n1*virt+e;
	for(int f=e+1;f<virt;f++)
	{
	  grad[n1a+indexD(e,f,virt)*T2aa.n_rows+n1] += 4.0*U(e,n1*virt+f);
	  grad[n1a+indexD(e,f,virt)*T2aa.n_rows+n1] -= 4.0*U(f,n2);
	}
      }
    }
  }
  U=(*iPtr).V2aai_jka*W;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[e*nels+m] -= 2.0*U(m,e);
    }
  }
  for(int i=0;i<nels;i++)
  {
    for(int j=i+1;j<nels;j++)
    {
      n1 = indexD(i,j,nels);
      for(int a=0;a<virt;a++)
      {
	n2=n1*virt+a;
	Y = W.row(n2)*W.row(n2).t();
	//Y = W.row(indexD(i,j,nels)*virt+a)*W.row(indexD(i,j,nels)*virt+a).t();
	for(int b=0;b<a;b++)
	{
	  F2aaM(n1,indexD(b,a,virt)) += 2.0*Y(0);
	  //F2aaM(indexD(i,j,nels),indexD(b,a,virt)) += 2.0*Y(0);
	}
	for(int b=a+1;b<virt;b++)
	{
	  F2aaM(n1,indexD(a,b,virt)) += 2.0*Y(0);
	  //F2aaM(indexD(i,j,nels),indexD(a,b,virt)) += 2.0*Y(0);
	}
      }
    }
  }
  for(int e=0;e<virt;e++)
  {
    Y = W.col(e).t()*W.col(e);
    for(int m=0;m<nels;m++)
    {
      F1m(m,e) -= Y(0);
    }
  }
  V=W*T1.t();
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      n1=indexD(i,a+nels,nact);
      for(int j=0;j<i;j++)
      {
	U(i,a)+=V(indexD(j,i,nels)*virt+a,j);
      }
      for(int j=i+1;j<nels;j++)
      {
	U(i,a)-=V(indexD(i,j,nels)*virt+a,j);
      }
      for(int j=0;j<nels;j++)
      {
	for(int k=j+1;k<nels;k++)
	{
	  x=V(indexD(j,k,nels)*virt+a,i);
	  D2aa(indexD(j,k,nact),n1)-=x;
	  D2aa(n1,indexD(j,k,nact))-=x;
	  //D2aa(indexD(j,k,nact),indexD(i,a+nels,nact))-=x;
	  //D2aa(indexD(i,a+nels,nact),indexD(j,k,nact))-=x;
	}
      }
    }
  }
  for(int i=0;i<nels;i++)
  {
    for(int a=0;a<virt;a++)
    {
      U(i,a) += T1(i,a)*sqrt(F1m(i,a));
    }
  }
  D1.submat(0,nels,nels-1,nact-1)=U;
  D1.submat(nels,0,nact-1,nels-1)=U.t();
  for(int m=0;m<nels;m++)
  {
    for(int e=0;e<virt;e++)
    {
      n1=m*nact+e+nels;
      n2=(e+nels)*nact+m;
      n3=indexD(m,e+nels,nact);
      for(int n=0;n<nels;n++)
      {
	D2ab(n1,m*nact+n)+=U(n,e);
	D2ab(m*nact+n,n1)+=U(n,e);
	D2ab(n2,n*nact+m)+=U(n,e);
	D2ab(n*nact+m,n2)+=U(n,e);
      }
      for(int n=m+1;n<nels;n++)
      {
	D2aa(indexD(m,n,nact),n3)+=U(n,e);
	D2aa(n3,indexD(m,n,nact))+=U(n,e);
	D2aa(indexD(m,n,nact),indexD(n,e+nels,nact))-=U(m,e);
	D2aa(indexD(n,e+nels,nact),indexD(m,n,nact))-=U(m,e);
	//D2aa(indexD(m,n,nact),indexD(m,e+nels,nact))+=U(n,e);
	//D2aa(indexD(m,e+nels,nact),indexD(m,n,nact))+=U(n,e);
	//D2aa(indexD(m,n,nact),indexD(n,e+nels,nact))-=U(m,e);
	//D2aa(indexD(n,e+nels,nact),indexD(m,n,nact))-=U(m,e);
      }
    }
  }

  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      n1=m*nels+n;
      n2=m*nact+n;
      for(int e=0;e<virt;e++)
      {
	for(int f=0;f<virt;f++)
	{
	  n3=(e+nels)*nact+f+nels;
	  n4=e*virt+f;
	  x = T2ab(n1,n4)*sqrt(F2abM(n1,n4));
	  D2ab(n3,n2) += x;
	  D2ab(n2,n3) += x;
	  //x = T2ab(m*nels+n,e*virt+f)*pow(F2abM(m*nels+n,e*virt+f),0.5);
	  //D2ab((e+nels)*nact+f+nels,m*nact+n) += x;
	  //D2ab(m*nact+n,(e+nels)*nact+f+nels) += x;
	}
      }
    }
  }
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      n3=indexD(m,n,nels);
      n2=indexD(m,n,nact);
      for(int e=0;e<virt;e++)
      {
	for(int f=e+1;f<virt;f++)
	{
	  n1=indexD(e,f,virt);
	  n4=indexD(e+nels,f+nels,nact);
	  x=T2aa(n3,n1)*sqrt(F2aaM(n3,n1));
	  D2aa(n4,n2) += x;
	  D2aa(n2,n4) += x;
	  //D2aa(indexD(e+nels,f+nels,nact),indexD(m,n,nact)) += x;
	  //D2aa(indexD(m,n,nact),indexD(e+nels,f+nels,nact)) += x;
	}
      }
    }
  }

  U=( (*iPtr).K1i_j+(*iPtr).V1i_j+(*iPtr).V1ai_j)*T1;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[e*nels+m]-=4.0*U(m,e);
    }
  }
  U=((*iPtr).K1a_b+(*iPtr).V1a_b+(*iPtr).V1aa_b)*T1.t();
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[e*nels+m]+=4.0*U(e,m);
    }
  }
  U=((*iPtr).V2abia_jb-(*iPtr).V2aaia_jb)*arma::vectorise(T1);
//Wai = T1i_a
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[e*nels+m] += 4.0*U(e*nels+m);
    }
  }

  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      x=4.0*((*iPtr).K1(m,e+nels)+(*iPtr).V1ia(e*nels+m)+(*iPtr).V1aia(e*nels+m))*T1(m,e)/sqrt(F1m(m,e));
      //y=4.0*(*iPtr).V1ia(e*nels+m)*T1(m,e)/sqrt(F1m(m,e));
      for(int i=0;i<nels;i++)
      {
	grad[e*nels+i] -= x*T1(i,e);
      }
      for(int f=0;f<virt;f++)
      {
	grad[f*nels+m] -= x*T1(m,f);
      }
    }
  }

  U=T1*(*iPtr).V2aah_fgn;
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      n1=indexD(e,f,virt);
      for(int m=0;m<nels;m++)
      {
	n2=n1*nels+m;
	for(int n=m+1;n<nels;n++)
	{
	  grad[n1a+n1*T2aa.n_rows+indexD(m,n,nels)] += 2.0*(U(n,n2)-U(m,n1*nels+n));
	}
      }
    }
  }
  U = T2aa*(*iPtr).V2aaab_cd;
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      n1=indexD(e,f,virt);
      for(int m=0;m<nels;m++)
      {
	for(int n=m+1;n<nels;n++)
	{
	  n2=indexD(m,n,nels);
	  grad[n1a+n1*T2aa.n_rows+n2] += 2.0*U(n2,n1);
	}
      }
    }
  }

  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      x=0.0;
      for(int f=0;f<virt;f++)
      {
        n1=e*virt+f;
        n2=(e+nels)*nact+f+nels;
	for(int n=0;n<nels;n++)
	{
	  x += (*iPtr).V2ab(m*nact+n,n2)*T2ab(m*nels+n,n1)/sqrt(F2abM(m*nels+n,n1));
	  
	}
      }
      grad[e*nels+m] += 4.0*T1(m,e)*x;
      for(int n=0;n<nels;n++)
      {
	grad[e*nels+n] -= 4.0*T1(n,e)*x;
      }
      for(int f=0;f<virt;f++)
      {
	grad[f*nels+m] -= 4.0*T1(m,f)*x;
      }
      for(int f=0;f<e;f++)
      {
	n1=indexD(f,e,virt);
	for(int n=0;n<m;n++)
	{
	  grad[n1a+n1*T2aa.n_rows+indexD(n,m,nels)] -= 4.0*x*T2aa(indexD(n,m,nels),n1);
	}
	for(int n=m+1;n<nels;n++)
	{
	  grad[n1a+n1*T2aa.n_rows+indexD(m,n,nels)] -= 4.0*x*T2aa(indexD(m,n,nels),n1);
	}
      }
      for(int f=e+1;f<virt;f++)
      {
	n1=indexD(e,f,virt);
	for(int n=0;n<m;n++)
	{
	  grad[n1a+n1*T2aa.n_rows+indexD(n,m,nels)] -= 4.0*x*T2aa(indexD(n,m,nels),n1);
	}
	for(int n=m+1;n<nels;n++)
	{
	  grad[n1a+n1*T2aa.n_rows+indexD(m,n,nels)] -= 4.0*x*T2aa(indexD(m,n,nels),n1);
	}
      }
    }
  }

  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      x=0.0;
      for(int f=0;f<e;f++)
      {
	n1=indexD(f,e,virt);
	n2=indexD(f+nels,e+nels,nact);
	for(int n=0;n<m;n++)
	{
	  n3=indexD(n,m,nact);
	  n4=indexD(n,m,nels);
	  x += (*iPtr).V2aa(n3,n2)*T2aa(n4,n1)/sqrt(F2aaM(n4,n1));
	}
	for(int n=m+1;n<nels;n++)
	{
	  n3=indexD(m,n,nact);
	  n4=indexD(m,n,nels);
	  x += (*iPtr).V2aa(n3,n2)*T2aa(n4,n1)/sqrt(F2aaM(n4,n1));
	}
      }
      for(int f=e+1;f<virt;f++)
      {
	n1=indexD(e,f,virt);
	n2=indexD(e+nels,f+nels,nact);
	for(int n=0;n<m;n++)
	{
	  n3=indexD(n,m,nact);
	  n4=indexD(n,m,nels);
	  x += (*iPtr).V2aa(n3,n2)*T2aa(n4,n1)/sqrt(F2aaM(n4,n1));
	}
	for(int n=m+1;n<nels;n++)
	{
	  n3=indexD(m,n,nact);
	  n4=indexD(m,n,nels);
	  x += (*iPtr).V2aa(n3,n2)*T2aa(n4,n1)/sqrt(F2aaM(n4,n1));
	}
      }
      grad[e*nels+m] += 2.0*x*T1(m,e);
      for(int f=0;f<virt;f++)
      {
	grad[f*nels+m] -= x*T1(m,f);
      }
      for(int n=0;n<nels;n++)
      {
	grad[e*nels+n] -= x*T1(n,e);
      }
    }
  }
  U=(*iPtr).V2aaij_kl*T2aa;
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      n1=indexD(e,f,virt);
      for(int m=0;m<nels;m++)
      {
	for(int n=m+1;n<nels;n++)
	{
	  grad[n1a+n1*T2aa.n_rows+indexD(m,n,nels)] += 2.0*U(indexD(m,n,nels),n1);
	}
      }
    }
  }
  U=T1.t()*(*iPtr).V2aai_jka;
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      n1=indexD(m,n,nels);
      for(int e=0;e<virt;e++)
      {
	n2=n1*virt+e;
	for(int f=e+1;f<virt;f++)
	{
	  n3=indexD(e,f,virt);
	  grad[n1a+n3*T2aa.n_rows+n1] += 2.0*(U(f,n2)-U(e,n1*virt+f));
	}
      }
    }
  }
  U=(*iPtr).V2abij_kl*T2ab;
  for(int e=0;e<virt;e++)
  {
    for(int k=0;k<nels;k++)
    {
      grad[n1a+n2aa+(e*virt+e)*T2ab.n_rows+(k*nels+k)] += 2.0*U(k*nels+k,e*virt+e);
      for(int f=0;f<virt;f++)
      {
	for(int l=0;l<nels && f*nels+l<e*nels+k;l++)
	{
	  x=2.0*U(k*nels+l,e*virt+f);
	  grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+(k*nels+l)] += x;
	  grad[n1a+n2aa+(f*virt+e)*T2ab.n_rows+(l*nels+k)] += x;
	}
      }
    }
  }


  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
//      x = ((*iPtr).K1(m,e+nels)+(*iPtr).V1ia(e*nels+m))*T1(m,e)/sqrt(F1m(m,e));
      x = ((*iPtr).K1(m,e+nels)+(*iPtr).V1ia(e*nels+m)+(*iPtr).V1aia(e*nels+m))*T1(m,e)/sqrt(F1m(m,e));
      for(int f=0;f<e;f++)
      {
	n1=indexD(f,e,virt);
	for(int n=0;n<m;n++)
	{
	  grad[n1a+n1*T2aa.n_rows+indexD(n,m,nels)] += 4.0*x*T2aa(indexD(n,m,nels),n1);
	}
	for(int n=m+1;n<nels;n++)
	{
	  grad[n1a+n1*T2aa.n_rows+indexD(m,n,nels)] += 4.0*x*T2aa(indexD(m,n,nels),n1);
	}
	for(int i=0;i<nels;i++)
	{
	  for(int n=i+1;n<nels;n++)
	  {
	  grad[n1a+n1*T2aa.n_rows+indexD(i,n,nels)] -= 4.0*x*T2aa(indexD(i,n,nels),n1);
	  }
	}
      }
      for(int f=e+1;f<virt;f++)
      {
	n1=indexD(e,f,virt);
	for(int n=0;n<m;n++)
	{
	  grad[n1a+n1*T2aa.n_rows+indexD(n,m,nels)] += 4.0*x*T2aa(indexD(n,m,nels),n1);
	}
	for(int n=m+1;n<nels;n++)
	{
	  grad[n1a+n1*T2aa.n_rows+indexD(m,n,nels)] += 4.0*x*T2aa(indexD(m,n,nels),n1);
	}
	for(int i=0;i<nels;i++)
	{
	  for(int n=i+1;n<nels;n++)
	  {
	  grad[n1a+n1*T2aa.n_rows+indexD(i,n,nels)] -= 4.0*x*T2aa(indexD(i,n,nels),n1);
	  }
	}
      }
      for(int f=0;f<virt;f++)
      {
	for(int g=f+1;g<virt;g++)
	{
	  n1=indexD(f,g,virt);
	  for(int n=0;n<m;n++)
	  {
	    grad[n1a+n1*T2aa.n_rows+indexD(n,m,nels)] -= 4.0*x*T2aa(indexD(n,m,nels),n1);
	  }
	  for(int n=m+1;n<nels;n++)
	  {
	    grad[n1a+n1*T2aa.n_rows+indexD(m,n,nels)] -= 4.0*x*T2aa(indexD(m,n,nels),n1);
	  }
	}
      }
    }
  }
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      n1=indexD(e,f,virt);
      n2=indexD(e+nels,f+nels,nact);
      for(int m=0;m<nels;m++)
      {
	for(int n=m+1;n<nels;n++)
	{
	  n3=indexD(m,n,nels);
	  n4=indexD(m,n,nact);
	  x = (*iPtr).V2aa(n4,n2)*T2aa(n3,n1)/sqrt(F2aaM(n3,n1));
	  grad[n1a+n1*T2aa.n_rows+n3] -= 6.0*x*T2aa(n3,n1);
	  for(int i=0;i<m;i++)
	  {
	    grad[n1a+n1*T2aa.n_rows+indexD(i,m,nels)] += 4.0*x*T2aa(indexD(i,m,nels),n1);
	  }
	  for(int i=m+1;i<nels;i++)
	  {
	    grad[n1a+n1*T2aa.n_rows+indexD(m,i,nels)] += 4.0*x*T2aa(indexD(m,i,nels),n1);
	  }
	  for(int i=0;i<n;i++)
	  {
	    grad[n1a+n1*T2aa.n_rows+indexD(i,n,nels)] += 4.0*x*T2aa(indexD(i,n,nels),n1);
	  }
	  for(int i=n+1;i<nels;i++)
	  {
	    grad[n1a+n1*T2aa.n_rows+indexD(n,i,nels)] += 4.0*x*T2aa(indexD(n,i,nels),n1);
	  }
	  for(int a=0;a<e;a++)
	  {
	    n5=indexD(a,e,virt);
	    grad[n1a+n5*T2aa.n_rows+n3] += 4.0*x*T2aa(n3,n5);
	    for(int i=0;i<m;i++)
	    {
	      n6=indexD(i,m,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=m+1;i<nels;i++)
	    {
	      n6=indexD(m,i,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=0;i<n;i++)
	    {
	      n6=indexD(i,n,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=n+1;i<nels;i++)
	    {
	      n6=indexD(n,i,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	  }
	  for(int a=e+1;a<virt;a++)
	  {
	    n5=indexD(e,a,virt);
	    grad[n1a+n5*T2aa.n_rows+n3] += 4.0*x*T2aa(n3,n5);
	    for(int i=0;i<m;i++)
	    {
	      n6=indexD(i,m,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=m+1;i<nels;i++)
	    {
	      n6=indexD(m,i,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=0;i<n;i++)
	    {
	      n6=indexD(i,n,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=n+1;i<nels;i++)
	    {
	      n6=indexD(n,i,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	  }
	  for(int a=0;a<f;a++)
	  {
	    n5=indexD(a,f,virt);
	    grad[n1a+n5*T2aa.n_rows+n3] += 4.0*x*T2aa(n3,n5);
	    for(int i=0;i<m;i++)
	    {
	      n6=indexD(i,m,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=m+1;i<nels;i++)
	    {
	      n6=indexD(m,i,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=0;i<n;i++)
	    {
	      n6=indexD(i,n,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=n+1;i<nels;i++)
	    {
	      n6=indexD(n,i,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	  }
	  for(int a=f+1;a<virt;a++)
	  {
	    n5=indexD(f,a,virt);
	    grad[n1a+n5*T2aa.n_rows+n3] += 4.0*x*T2aa(n3,n5);
	    for(int i=0;i<m;i++)
	    {
	      n6=indexD(i,m,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=m+1;i<nels;i++)
	    {
	      n6=indexD(m,i,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=0;i<n;i++)
	    {
	      n6=indexD(i,n,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	    for(int i=n+1;i<nels;i++)
	    {
	      n6=indexD(n,i,nels);
	      grad[n1a+n5*T2aa.n_rows+n6] -= 2.0*x*T2aa(n6,n5);
	    }
	  }
	  for(int i=0;i<nels;i++)
	  {
	    for(int j=i+1;j<nels;j++)
	    {
	      grad[n1a+n1*T2aa.n_rows+indexD(i,j,nels)] -= 2.0*x*T2aa(indexD(i,j,nels),n1);
	    }
	  }
	  for(int a=0;a<virt;a++)
	  {
	    for(int b=a+1;b<virt;b++)
	    {
	      grad[n1a+indexD(a,b,virt)*T2aa.n_rows+n3] -= 2.0*x*T2aa(n3,indexD(a,b,virt));
	    }
	  }
	}
      }
    }
  }

  U=T1.t()*(*iPtr).V2abi_jka;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[n1a+n2aa+(e*virt+e)*T2ab.n_rows+m*nels+m] -= 4.0*U(e,(m*nels+m)*virt+e);
      for(int f=0;f<virt;f++)
      {
	for(int n=0;n<nels && f*nels+n<e*nels+m;n++)
	{
	  x = 2.0*(U(f,(n*nels+m)*virt+e)+U(e,(m*nels+n)*virt+f));
	  grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+m*nels+n] -= x;
	  grad[n1a+n2aa+(f*virt+e)*T2ab.n_rows+n*nels+m] -= x; 
	}
      }
    }
  }
  U=T1*(*iPtr).V2abh_fgn;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[n1a+n2aa+(e*virt+e)*T2ab.n_rows+m*nels+m] += 4.0*U(m,(e*virt+e)*nels+m);
      for(int f=0;f<virt;f++)
      {
	for(int n=0;n<nels && f*nels+n<e*nels+m;n++)
	{
	  x = 2.0*(U(n,(e*virt+f)*nels+m)+U(m,(f*virt+e)*nels+n));
	  grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+m*nels+n] += x; 
	  grad[n1a+n2aa+(f*virt+e)*T2ab.n_rows+n*nels+m] += x; 
	}
      }
    }
  }

  U=T2ab*(*iPtr).V2abab_cd;
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      grad[n1a+n2aa+(e*virt+e)*T2ab.n_rows+m*nels+m] += 2.0*U(m*nels+m,e*virt+e); 
      for(int f=0;f<virt;f++)
      {
	for(int n=0;n<nels && f*nels+n<e*nels+m;n++)
	{
	  x=2.0*U(m*nels+n,e*virt+f);
	  grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+m*nels+n] += x;
	  grad[n1a+n2aa+(f*virt+e)*T2ab.n_rows+n*nels+m] += x; 
	}
      }
    }
  }

  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      x=2.0*((*iPtr).K1(m,e+nels)+(*iPtr).V1ia(e*nels+m)+(*iPtr).V1aia(e*nels+m))*T1(m,e)/sqrt(F1m(m,e));
      for(int f=0;f<virt;f++)
      {
	n3=e*virt+f;
	n4=f*virt+e;
	for(int n=0;n<nels;n++)
	{
	  n1=m*nels+n;
	  n2=n*nels+m;
	  y=x*T2ab(m*nels+n,e*virt+f);
	  grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+m*nels+n] += x*T2ab(m*nels+n,e*virt+f);
	  grad[n1a+n2aa+(f*virt+e)*T2ab.n_rows+n*nels+m] += x*T2ab(m*nels+n,e*virt+f);
	  for(int i=0;i<nels;i++)
	  {
	    grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+i*nels+n] -= x*T2ab(i*nels+n,e*virt+f);
	    grad[n1a+n2aa+(f*virt+e)*T2ab.n_rows+n*nels+i] -= x*T2ab(i*nels+n,e*virt+f);
	  }
	  for(int a=0;a<virt;a++)
	  {
	    grad[n1a+n2aa+(a*virt+f)*T2ab.n_rows+m*nels+n] -= x*T2ab(m*nels+n,a*virt+f);
	    grad[n1a+n2aa+(f*virt+a)*T2ab.n_rows+n*nels+m] -= x*T2ab(m*nels+n,a*virt+f);
	  }
	}
      }
    }
  }
  for(int e=0;e<virt;e++)
  {
    for(int f=0;f<virt;f++)
    {
      for(int m=0;m<nels;m++)
      {
	for(int n=0;n<nels;n++)
	{
	  x = (*iPtr).V2ab(m*nact+n,(e+nels)*nact+f+nels)*T2ab(m*nels+n,e*virt+f)/sqrt(F2abM(m*nels+n,e*virt+f));
	  grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+m*nels+n] -= 6.0*x*T2ab(m*nels+n,e*virt+f);
	  for(int i=0;i<nels;i++)
	  {
	    grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+m*nels+i] += 4.0*x*T2ab(m*nels+i,e*virt+f);
	    grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+i*nels+n] += 4.0*x*T2ab(i*nels+n,e*virt+f);
	    for(int j=0;j<nels;j++)
	    {
	      grad[n1a+n2aa+(e*virt+f)*T2ab.n_rows+i*nels+j] -= 2.0*x*T2ab(i*nels+j,e*virt+f);
	    }
	  }
	  for(int a=0;a<virt;a++)
	  {
	    grad[n1a+n2aa+(e*virt+a)*T2ab.n_rows+m*nels+n] += 4.0*x*T2ab(m*nels+n,e*virt+a);
	    grad[n1a+n2aa+(a*virt+f)*T2ab.n_rows+m*nels+n] += 4.0*x*T2ab(m*nels+n,a*virt+f);
	    for(int i=0;i<nels;i++)
	    {
	      grad[n1a+n2aa+(a*virt+f)*T2ab.n_rows+i*nels+n] -= 2.0*x*T2ab(i*nels+n,a*virt+f);
	      grad[n1a+n2aa+(e*virt+a)*T2ab.n_rows+i*nels+n] -= 2.0*x*T2ab(i*nels+n,e*virt+a);
	      grad[n1a+n2aa+(a*virt+f)*T2ab.n_rows+m*nels+i] -= 2.0*x*T2ab(m*nels+i,a*virt+f);
	      grad[n1a+n2aa+(e*virt+a)*T2ab.n_rows+m*nels+i] -= 2.0*x*T2ab(m*nels+i,e*virt+a);
	    }
	    for(int b=0;b<virt;b++)
	    {
	      grad[n1a+n2aa+(a*virt+b)*T2ab.n_rows+m*nels+n] -= 2.0*x*T2ab(m*nels+n,a*virt+b);
	    }
	  }
	}
      }
    }
  }

  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      n1=indexD(e,f,virt);
      n2=indexD(e+nels,f+nels,nact);
      for(int m=0;m<nels;m++)
      {
	for(int n=m+1;n<nels;n++)
	{
	  n3=indexD(m,n,nels);
	  n4=indexD(m,n,nact);
	  x = -(*iPtr).V2aa(n4,n2)*T2aa(n3,n1)/sqrt(F2aaM(n3,n1));
	  for(int a=0;a<virt;a++)
	  {
	    for(int i=0;i<nels;i++)
	    {
	      grad[n1a+n2aa+(a*virt+e)*T2ab.n_rows+i*nels+m] += x*T2ab(i*nels+m,a*virt+e);
	      grad[n1a+n2aa+(a*virt+f)*T2ab.n_rows+i*nels+m] += x*T2ab(i*nels+m,a*virt+f);
	      grad[n1a+n2aa+(a*virt+e)*T2ab.n_rows+i*nels+n] += x*T2ab(i*nels+n,a*virt+e);
	      grad[n1a+n2aa+(a*virt+f)*T2ab.n_rows+i*nels+n] += x*T2ab(i*nels+n,a*virt+f);
	      
	      grad[n1a+n2aa+(e*virt+a)*T2ab.n_rows+m*nels+i] += x*T2ab(m*nels+i,e*virt+a);
	      grad[n1a+n2aa+(f*virt+a)*T2ab.n_rows+m*nels+i] += x*T2ab(m*nels+i,f*virt+a);
	      grad[n1a+n2aa+(e*virt+a)*T2ab.n_rows+n*nels+i] += x*T2ab(n*nels+i,e*virt+a);
	      grad[n1a+n2aa+(f*virt+a)*T2ab.n_rows+n*nels+i] += x*T2ab(n*nels+i,f*virt+a);
	    }
	  }
	}
      }
    }
  }

//VOICI
  double maxT1=0.0;
  double maxT2aa=0.0;
  double maxT2ab=0.0;

  for(int a=0;a<virt;a++)
  {
    for(int i=0;i<nels;i++)
    {
      x=4.0*((*iPtr).K1(i,a+nels)+(*iPtr).V1ia(a*nels+i))*sqrt(F1m(i,a));
      x+=4.0*(*iPtr).V1aia(a*nels+i)*sqrt(F1m(i,a));
      x+=4.0*((*iPtr).K1(i,a+nels)+(*iPtr).V1ia(a*nels+i))*T1(i,a)*T1(i,a)/sqrt(F1m(i,a));
      x+=4.0*(*iPtr).V1aia(a*nels+i)*T1(i,a)*T1(i,a)/sqrt(F1m(i,a));
      //x=dE1dT1(i,a,(*iPtr).K1,T1,T2aa,T2ab,F1m);
      //x+=dE2ab_dT1(i,a,(*iPtr).V2ab,T1,T2aa,T2ab,F1m,F2abM);
      //x+=dE2aa_dT1(i,a,(*iPtr).V2aa,T1,T2aa,T2ab,F1m,F2aaM);
      grad[a*nels+i]+=x;
      if(fabs(grad[a*nels+i]) > maxT1)
      {
	maxT1 = fabs(grad[a*nels+i]);
      }
    }
  }
  for(int a=0;a<virt;a++)
  {
    for(int b=a+1;b<virt;b++)
    {
      for(int i=0;i<nels;i++)
      {
        for(int j=i+1;j<nels;j++)
	{
	  x=4.0*((*iPtr).K1(i,a+nels)*T1(j,b)-(*iPtr).K1(j,a+nels)*T1(i,b)-(*iPtr).K1(i,b+nels)*T1(j,a)+(*iPtr).K1(j,b+nels)*T1(i,a));
	  x+=4.0*((*iPtr).V1ia(a*nels+i)*T1(j,b)-(*iPtr).V1ia(a*nels+j)*T1(i,b)-(*iPtr).V1ia(b*nels+i)*T1(j,a)+(*iPtr).V1ia(b*nels+j)*T1(i,a));
	  x+=4.0*((*iPtr).V1aia(a*nels+i)*T1(j,b)-(*iPtr).V1aia(a*nels+j)*T1(i,b)-(*iPtr).V1aia(b*nels+i)*T1(j,a)+(*iPtr).V1aia(b*nels+j)*T1(i,a));
	  x+= 2.0*(*iPtr).V2aa(indexD(i,j,nact),indexD(a+nels,b+nels,nact))*sqrt(F2aaM(indexD(i,j,nels),indexD(a,b,virt)));

//	  x+=dE1dT2aa(i,j,a,b,(*iPtr).K1,T1,T2aa,T2ab,F1m);
//	  x+=dE2ab_dT2aa(i,j,a,b,(*iPtr).V2ab,T1,T2aa,T2ab,F1m,F2abM);
//	  x+=dE2aa_dT2aa(i,j,a,b,(*iPtr).V2aa,T1,T2aa,T2ab,F1m,F2aaM);
	  grad[n1a+indexD(a,b,virt)*T2aa.n_rows+indexD(i,j,nels)]+=x;
	  if(fabs(grad[n1a+indexD(a,b,virt)*T2aa.n_rows+indexD(i,j,nels)]) > maxT2aa)
	  {
	    maxT2aa = fabs(grad[n1a+indexD(a,b,virt)*T2aa.n_rows+indexD(i,j,nels)]  );
 	  }
	}
      }
    }
  }
  for(int a=0;a<virt;a++)
  {
    for(int i=0;i<nels;i++)
    {
      y=4.0*((*iPtr).K1(i,a+nels)+(*iPtr).V1ia(a*nels+i)+(*iPtr).V1aia(a*nels+i))*T1(i,a);
      y += 2.0*(*iPtr).V2ab(i*nact+i,(a+nels)*nact+a+nels)*sqrt(F2abM(i*nels+i,a*virt+a));
//      x+=dE1dT2ab(i,i,a,a,(*iPtr).K1,T1,T2aa,T2ab,F1m);
      x=dE2ab_dT2ab(i,i,a,a,(*iPtr).V2ab,T1,T2aa,T2ab,F1m,F2abM);
      x+=dE2aa_dT2ab(i,i,a,a,(*iPtr).V2aa,T1,T2aa,T2ab,F1m,F2aaM);
      grad[n1a+n2aa+(a*virt+a)*T2ab.n_rows+(i*nels+i)]+=x+y;
      if(fabs(grad[n1a+n2aa+(a*virt+a)*T2ab.n_rows+i*nels+i]  ) > maxT2ab)
      {
	maxT2ab = fabs(grad[n1a+n2aa+(a*virt+a)*T2ab.n_rows+i*nels+i]  );
      }
    }
  }
  for(int a=0;a<virt;a++)
  {
    for(int b=0;b<virt;b++)
    {
      for(int i=0;i<nels;i++)
      {
	for(int j=0;j<nels && (b*nels+j)<a*nels+i;j++)
	{
	  y=2.0*((*iPtr).K1(i,a+nels)+(*iPtr).V1ia(a*nels+i)+(*iPtr).V1aia(a*nels+i))*T1(j,b);
	  y += 2.0*((*iPtr).K1(j,b+nels)+(*iPtr).V1ia(b*nels+j)+(*iPtr).V1aia(b*nels+j))*T1(i,a);
          y += 2.0*(*iPtr).V2ab(i*nact+j,(a+nels)*nact+b+nels)*sqrt(F2abM(i*nels+j,a*virt+b));
	  x=0.0;
//	  x=dE1dT2ab(i,j,a,b,(*iPtr).K1,T1,T2aa,T2ab,F1m);
//	  x+=dE1dT2ab(j,i,b,a,(*iPtr).K1,T1,T2aa,T2ab,F1m);
	  x+=dE2ab_dT2ab(i,j,a,b,(*iPtr).V2ab,T1,T2aa,T2ab,F1m,F2abM);
	  x+=dE2ab_dT2ab(j,i,b,a,(*iPtr).V2ab,T1,T2aa,T2ab,F1m,F2abM);
	  x+=dE2aa_dT2ab(i,j,a,b,(*iPtr).V2aa,T1,T2aa,T2ab,F1m,F2aaM);
	  x+=dE2aa_dT2ab(j,i,b,a,(*iPtr).V2aa,T1,T2aa,T2ab,F1m,F2aaM);
	  grad[n1a+n2aa+(a*virt+b)*T2ab.n_rows+(i*nels+j)]+=0.5*x+y;
	  grad[n1a+n2aa+(b*virt+a)*T2ab.n_rows+(j*nels+i)]+=0.5*x+y;
	  if(fabs(grad[n1a+n2aa+(a*virt+b)*T2ab.n_rows+i*nels+j]  ) > maxT2ab)
	  {
	    maxT2ab = fabs(grad[n1a+n2aa+(a*virt+b)*T2ab.n_rows+i*nels+j]  );
	  }
	}
      }
    }
  }
  double E = 2.0*trace( (*iPtr).K1*D1)+trace( (*iPtr).V2aa*D2aa)+trace( (*iPtr).V2ab*D2ab); 
  /*std::cout << "\tE:\t" << E;
  std::cout << "\tmax dT2ab:\t" << maxT2ab;
  std::cout << "\tmax dT2aa:\t" << maxT2aa;
  std::cout << "\tmax dT1:\t" << maxT1 << std::endl << std::endl;
*/
  lastTime = time(NULL);

  tim += lastTime-startTime;

  return;
}

void setMats(intBundle &i)
{
  int nels=i.nels;
  int virt=i.virt;
  int nact=i.nels+i.virt;
  double x;
  int n1,n2,n3;

  i.K1i_j=i.K1.submat(0,0,nels-1,nels-1);
  i.K1a_b=i.K1.submat(nels,nels,nact-1,nact-1);
  i.K1ia.resize(1,nels*virt);
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      i.K1ia(0,e*nels+m)=i.K1(m,e+nels);
    }
  }
  i.V1i_j.resize(nels,nels);
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      x=0.0;
      for(int k=0;k<nels;k++)
      {
	x+=i.V2ab(n*nact+k,m*nact+k); 
      }
      i.V1i_j(n,m)=x;
    }
  }
  i.V1ai_j.resize(nels,nels);
  for(int m=0;m<nels;m++)
  {
    for(int n=0;n<nels;n++)
    {
      x=0.0;
      for(int k=0;k<std::min(m,n);k++)
      {
	x += i.V2aa(indexD(k,m,nact),indexD(k,n,nact));
      }
      for(int k=m+1;k<n;k++)
      {
	x -= i.V2aa(indexD(m,k,nact),indexD(k,n,nact));
      }
      for(int k=n+1;k<m;k++)
      {
	x -= i.V2aa(indexD(k,m,nact),indexD(n,k,nact));
      }
      for(int k=std::max(m,n)+1;k<nels;k++)
      {
	x += i.V2aa(indexD(m,k,nact),indexD(n,k,nact));
      }
      //i.V1i_j(m,n)+=0.5*x;
      i.V1ai_j(m,n)+=0.5*x;
    }
  }
  i.V1ia.resize(1,nels*virt);
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      x=0.0;
      for(int n=0;n<nels;n++)
      {
	x+=i.V2ab(m*nact+n,(e+nels)*nact+n);
      }
      i.V1ia(0,e*nels+m)=x;
    }
  }
  /*i.V1i_a.resize(nels,virt);
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      x=0.0;
      for(int n=0;n<nels;n++)
      {
	x += i.V2ab(m*nact+n,(e+nels)*nact+n);
      }
      i.V1i_a(m,e) = x;
    }
  }*/
  i.V1aia.resize(1,nels*virt);
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      x=0.0;
      for(int n=0;n<m;n++)
      {
	x += i.V2aa(indexD(n,m,nact),indexD(n,e+nels,nact));
      }
      for(int n=m+1;n<nels;n++)
      {
	x -= i.V2aa(indexD(m,n,nact),indexD(n,e+nels,nact));
      }
      i.V1aia(0,e*nels+m) = 0.5*x;
    }
  }
  i.V2abi_jka.resize(nels,nels*nels*virt);
  for(int j=0;j<nels;j++)
  {
    for(int k=0;k<nels;k++)
    {
      for(int e=0;e<virt;e++)
      {
	for(int l=0;l<nels;l++)
	{
	  i.V2abi_jka(l,(j*nels+k)*virt+e) = i.V2ab(l*nact+e+nels,j*nact+k);
	}
      }
    }
  }
  i.V2aai_jka.resize(nels,nels*(nels-1)/2*virt);
  for(int m=0;m<nels;m++)
  {
    for(int n=m+1;n<nels;n++)
    {
      n1=indexD(m,n,nact);
      n3=indexD(m,n,nels);
      for(int e=0;e<virt;e++)
      {
	n2=n3*virt+e;
	for(int k=0;k<nels;k++)
	{
	  i.V2aai_jka(k,n2) = i.V2aa(indexD(k,e+nels,nact),n1);
	}
      }
    }
  }
  i.V1a_b.resize(virt,virt);
  for(int e=0;e<virt;e++)
  {
    for(int f=0;f<virt;f++)
    {
      x=0.0;
      for(int m=0;m<nels;m++)
      {
	x += i.V2ab(m*nact+f+nels,m*nact+e+nels);
      }
      i.V1a_b(f,e) = x;
    }
  }
  i.V1aa_b.resize(virt,virt);
  for(int e=0;e<virt;e++)
  {
    for(int f=0;f<virt;f++)
    {
      x=0.0;
      for(int m=0;m<nels;m++)
      {
	x += i.V2aa(indexD(m,e+nels,nact),indexD(m,f+nels,nact));
      }
      i.V1aa_b(f,e) = 0.5*x;
    }
  }
  i.V2abia_jb.resize(nels*virt,nels*virt);
  for(int b=0;b<virt;b++)
  {
    for(int j=0;j<nels;j++)
    {
      for(int a=0;a<virt;a++)
      {
	for(int k=0;k<nels;k++)
	{
	  i.V2abia_jb(a*nels+k,b*nels+j) = i.V2ab((a+nels)*nact+j,k*nact+b+nels);
	}
      }
    }
  }
  i.V2abia_jb2.resize(nels*virt,nels*virt);
  for(int e=0;e<virt;e++)
  {
    for(int m=0;m<nels;m++)
    {
      for(int f=0;f<virt;f++)
      {
	for(int n=0;n<nels;n++)
	{
	  i.V2abia_jb2(f*nels+n,e*nels+m) = i.V2ab(m*nact+f+nels,n*nact+e+nels);
	      //x += (*iPtr).V2ab(i*nact+f+nels,m*nact+a+nels)*T2ab(i*nels+n,e*virt+a);
	}
      }
    }
  }
  i.V2aaia_jb.resize(nels*virt,nels*virt);
  for(int b=0;b<virt;b++)
  {
    for(int j=0;j<nels;j++)
    {
      for(int a=0;a<virt;a++)
      {
	for(int k=0;k<nels;k++)
	{
	  i.V2aaia_jb(b*nels+j,a*nels+k) = 0.5*i.V2aa(indexD(j,a+nels,nact),indexD(k,b+nels,nact));	  
	}
      }
    }
  }
  i.V2abh_fgn.resize(virt,virt*virt*nels);
  for(int h=0;h<virt;h++)
  {
    for(int n=0;n<nels;n++)
    {
      for(int f=0;f<virt;f++)
      {
	for(int g=0;g<virt;g++)
	{
	  i.V2abh_fgn(h,(f*virt+g)*nels+n) = i.V2ab((f+nels)*nact+g+nels,n*nact+h+nels);
	}
      }
    }
  }
  i.V2aah_fgn.resize(virt,virt*(virt-1)/2*nels);
  for(int f=0;f<virt;f++)
  {
    for(int g=f+1;g<virt;g++)
    {
      n1=indexD(f,g,virt);
      n3=indexD(f+nels,g+nels,nact);
      for(int m=0;m<nels;m++)
      {
	n2=n1*nels+m;
	for(int h=0;h<virt;h++)
	{
	  i.V2aah_fgn(h,n2) = i.V2aa(indexD(m,h+nels,nact),n3);
	}
      }
    }
  }
  i.V2aaij_kl.resize(nels*(nels-1)/2,nels*(nels-1)/2);
  for(int j=0;j<nels;j++)
  {
    for(int k=j+1;k<nels;k++)
    {
      for(int l=0;l<nels;l++)
      {
	for(int m=l+1;m<nels;m++)
	{
	  i.V2aaij_kl(indexD(l,m,nels),indexD(j,k,nels)) = i.V2aa(indexD(l,m,nact),indexD(j,k,nact));
	}
      }
    }
  }
  i.V2aaab_cd.resize(virt*(virt-1)/2,virt*(virt-1)/2);
  for(int e=0;e<virt;e++)
  {
    for(int f=e+1;f<virt;f++)
    {
      n1=indexD(e,f,virt);
      n2=indexD(e+nels,f+nels,nact);
      for(int g=0;g<virt;g++)
      {
	for(int h=g+1;h<virt;h++)
	{
	  i.V2aaab_cd(indexD(g,h,virt),n1) = i.V2aa(indexD(g+nels,h+nels,nact),n2);
	}
      }
    }
  }
  i.V2abij_kl.resize(nels*nels,nels*nels);
  for(int m=0;m<nels;m++)
  {
    for(int j=0;j<nels;j++)
    {
      for(int k=0;k<nels;k++)
      {
	for(int l=0;l<nels;l++)
	{
	  i.V2abij_kl(k*nels+l,m*nels+j) = i.V2ab(k*nact+l,m*nact+j);
	}
      }
    }
  }
  i.V2abab_cd.resize(virt*virt,virt*virt);
  for(int e=0;e<virt;e++)
  {
    for(int f=0;f<virt;f++)
    {
      for(int g=0;g<virt;g++)
      {
	for(int h=0;h<virt;h++)
	{
	  i.V2abab_cd(g*virt+h,e*virt+f) = i.V2ab((g+nels)*nact+h+nels,(e+nels)*nact+f+nels);
	}
      }
    }
  }

  return;
}
